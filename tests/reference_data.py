# -*- coding: utf-8 -*-
"""Access to Kauri ID reference data set."""

# Created: 2019-03-14 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

import json
import os


_REFERENCE_FILE = 'reference_data.json'
_REFERENCE_FILE_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                                    _REFERENCE_FILE))
REFERENCE_DATA = json.load(open(_REFERENCE_FILE_PATH, 'rt'))

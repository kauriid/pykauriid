# -*- coding: utf-8 -*-
"""Tests data for PyKauriID."""

# Created: 2018-07-26 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.


__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

JWK_HARRY_FULL_ED25519 = (
    '{"kty":"OKP"'
    ',"crv":"Ed25519"'
    ',"d":"nWGxne_9WmC6hEr0kuwsxERJxWl7MmkZcDusAxyuf2A"'
    ',"x":"11qYAYKxCrfVS_7TyWQHOg7hcvPapiMlrwIaaPcHURo"'
    ',"kid":"Harry James Potter (student)"}')
JWK_HARRY_PUB_ED25519 = (
    '{"kty":"OKP"'
    ',"crv":"Ed25519"'
    ',"x":"11qYAYKxCrfVS_7TyWQHOg7hcvPapiMlrwIaaPcHURo"'
    ',"kid":"Harry James Potter (student)"}')
JWK_HARRY_FULL_ED25519_DICT = {
    'kty': 'OKP',
    'crv': 'Ed25519',
    'd': b'\x9da\xb1\x9d\xef\xfdZ`\xba\x84J\xf4\x92\xec,\xc4DI\xc5i{2i'
         b'\x19p;\xac\x03\x1c\xae\x7f`',
    'x': b'\xd7Z\x98\x01\x82\xb1\n\xb7\xd5K\xfe\xd3\xc9d\x07:\x0e\xe1r'
         b'\xf3\xda\xa6#%\xaf\x02\x1ah\xf7\x07Q\x1a',
    'kid': 'Harry James Potter (student)'
}
JWK_ALBUS_PUB_ED25519 = (
    '{"kty":"OKP"'
    ',"crv":"Ed25519"'
    ',"x":"8TakswfV80LFiX_Ii1dJL9u4vqWlkhApKbo-v2i1uUc"'
    ',"kid":"Albus Percival Wulfric Brian Dumbledore (Headmaster)"}')
JWK_ALBUS_FULL_ED25519 = (
    '{"kty":"OKP"'
    ',"crv":"Ed25519"'
    ',"d":"Y-pua--r4vGiRcZieji_sdH5feNsRJgTXBEiOlEmfWc"'
    ',"x":"8TakswfV80LFiX_Ii1dJL9u4vqWlkhApKbo-v2i1uUc"'
    ',"kid":"Albus Percival Wulfric Brian Dumbledore (Headmaster)"}')
JWK_ALBUS_FULL_ED25519_DICT = {
    'kty': 'OKP',
    'crv': 'Ed25519',
    'd': b'c\xeank\xef\xab\xe2\xf1\xa2E\xc6bz8\xbf\xb1\xd1\xf9}\xe3lD\x98'
         b'\x13\\\x11":Q&}g',
    'x': b'\xf16\xa4\xb3\x07\xd5\xf3B\xc5\x89\x7f\xc8\x8bWI/\xdb\xb8\xbe'
         b'\xa5\xa5\x92\x10))\xba>\xbfh\xb5\xb9G',
    'kid': 'Albus Percival Wulfric Brian Dumbledore (Headmaster)'
}
JWK_HARRY_FULL_X25519 = (
    '{"kty":"OKP"'
    ',"crv":"X25519"'
    ',"d":"U7ZBkzSFDI7uUV0RfDmFaeHxUCnFaxaib9sypeLrJ-M"'
    ',"x":"U1GCoOv8N0lePEpZVL0eWEOxriqW_ikAnZdDq4nAn24"'
    ',"kid":"Harry James Potter (student)"}')
JWK_HARRY_PUB_X25519 = (
    '{"kty":"OKP"'
    ',"crv":"X25519"'
    ',"x":"U1GCoOv8N0lePEpZVL0eWEOxriqW_ikAnZdDq4nAn24"'
    ',"kid":"Harry James Potter (student)"}')
JWK_HARRY_FULL_X25519_DICT = {
    'kty': 'OKP',
    'crv': 'X25519',
    'd': b"S\xb6A\x934\x85\x0c\x8e\xeeQ]\x11|9\x85i\xe1\xf1P)\xc5k\x16"
         b"\xa2o\xdb2\xa5\xe2\xeb'\xe3",
    'x': b'SQ\x82\xa0\xeb\xfc7I^<JYT\xbd\x1eXC\xb1\xae*\x96\xfe)\x00\x9d'
         b'\x97C\xab\x89\xc0\x9fn',
    'kid': 'Harry James Potter (student)'
}
JWK_ALBUS_FULL_X25519 = (
    '{"kty":"OKP"'
    ',"crv":"X25519"'
    ',"d":"vfoxXOfEJsZXwzgOVLEK7kmqgcaX0gXtYaEFD2tlwbA"'
    ',"x":"kVNmr6NqCs1mjtprt4sK01Is6J39jpprRz4Ky3Ig-1M"'
    ',"kid":"Albus Percival Wulfric Brian Dumbledore (Headmaster)"}')
JWK_ALBUS_PUB_X25519 = (
    '{"kty":"OKP"'
    ',"crv":"X25519"'
    ',"x":"kVNmr6NqCs1mjtprt4sK01Is6J39jpprRz4Ky3Ig-1M"'
    ',"kid":"Albus Percival Wulfric Brian Dumbledore (Headmaster)"}')

HARRY_DID = 'did:ssid:11qXXXharryXXXURo'
ALBUS_DID = 'did:ssid:8TaXXXalbusXXXuUc'

HARRY_JSON_LD = """{
    "@context": "http://schema.org",
    "@type": "Person",
    "familyName": "Potter",
    "givenName": "Harry James",
    "birthDate": "1980-07-31",
    "address": {
        "@type": "PostalAddress",
        "streetAddress": "4 Privet Drive",
        "addressLocality": "Little Whinging",
        "postalCode": "BS13 9RN",
        "addressRegion": "Surrey",
        "addressCountry": "United Kingdom",
        "telephone": "+44 555 325 1464"
    }
}"""

HARRY_JSON_LD_CLAIMS = [
    {
        '@context': 'http://schema.org',
        '@type': 'Person',
        'familyName': 'Potter'
    },
    {
        '@context': 'http://schema.org',
        '@type': 'Person',
        'givenName': 'Harry James'
    },
    {
        '@context': 'http://schema.org',
        '@type': 'Person',
        # Or 333849600 from calendar.timegm((1980, 7, 31, 0, 0, 0, 0, 0, 0)).
        'birthDate': '1980-07-31'
    },
    {
        '@context': 'http://schema.org',
        '@type': 'Person',
        'address': {
            '@type': 'PostalAddress',
            'streetAddress': '4 Privet Drive'
        }
    },
    {
        '@context': 'http://schema.org',
        '@type': 'Person',
        'address': {
            '@type': 'PostalAddress',
            'addressLocality': 'Little Whinging'
        }
    },
    {
        '@context': 'http://schema.org',
        '@type': 'Person',
        'address': {
            '@type': 'PostalAddress',
            'postalCode': 'BS13 9RN'
        }
    },
    {
        '@context': 'http://schema.org',
        '@type': 'Person',
        'address': {
            '@type': 'PostalAddress',
            'addressRegion': 'Surrey'
        }
    },
    {
        '@context': 'http://schema.org',
        '@type': 'Person',
        'address': {
            '@type': 'PostalAddress',
            # Alternative: two-letter ISO 3166-1 alpha-2 country code
            'addressCountry': 'United Kingdom'
        }
    },
    {
        '@context': 'http://schema.org',
        '@type': 'Person',
        'address': {
            '@type': 'PostalAddress',
            'telephone': '+44 555 325 1464'
        }
    }
]
DEVICE_CLAIMS = [
    {
        "@context": "http://schema.org",
        "@type": "Thing",
        "identifier": [
            {"@type": "PropertyValue",
             "serialNumber": "4711-0815"},
            {"@type": "PropertyValue",
             "name": "MAC address",
             "value": "9c:de:ad:be:ef:42"}
        ]
    }
]

CLAIM_SET_DICT = {
    'claims': [
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAACn.Um-dkZ-yboq'
         'CVR85LLil9Cjv6I0f3_Ay1jt3ay5OL5AKyb-66S9JG21LYdk8xLWWmAqMVrRmXTZF'
         'vYQ71vnq739dYYCAFNw.6AD25ss-l6A95SZqoc7xeQ'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAACc.BKCVZxoby6G'
         'Qry0Cs-xk3nzx7wzxCqmAJd9xQ6P5KjPTu4SOZBT1VlUBZsyNvf62McVxP-XOqKsm'
         'wjolbSuXxIx-220UFfDnFuPX.ExoHLA3EeOT_F_U0MRHyMQ'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAACR.rgam9pMCRd5'
         'YXC8trc0XM5NoIt3OXDaSwSzJ_XGZoQp75Zz85sqLnPGwU8IctXr2_8M8Spe2nBk5'
         'HGCzV9VV2helAiIIVNFceKQ.-z55Js9LTrGv5aMwPBNwKA'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAACG.ZrDjN5G_s9J'
         'KAJltCFBmejya-edH38lYoJXon4NrOBZV8b3ya9eVVvyoNtsiKO-ABLAOL0eWMU9X'
         '6c-1321pVa4ixPs_2dxmc6JlkZllHn3EWALS7q9oLOFLPveBg01dbzhJ3rEDtVUaZ'
         '6Cc-2i9tmx-PTiiEw.VB7LTKIA4nRpgnhw4BOG5w'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAAB7.-cdL78mFwqM'
         'LbCe1urN-P2tosPYAjbUxMXZwNC6E2H329tGYomEHs5hOL_u4YqqsvSR14RfHfmeq'
         'fAcGnRSLKJ2K1iuAGicuKYkrBVzjsDUdKC0DTr4sksfD6enOn2C1jQ8wu4HJ3-TCC'
         'E79R-FO_YATVxApmAj3XA.SgYm1epvW2IfTCzTqzhB-g'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAABw.9br06pFZeOp'
         '0XspJQxVFGf3_Wategvsq3tgQ3ZfnMXoTVz83sZ-J5Ci7DENwzGkzkKRbly0Dsw_b'
         'Uvjv38W7jDmW4xuGwuFw0s0LA_4Mf4PeXEFOlQDnwe9yS06KtSsVxRuxkYz8OYBFA'
         'Rh02Q.1qwbVaHi3QX3TuQs2yNEQA'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAABl.qkpjcC3Iraz'
         'rYXK37wz6odisek-ZDmwgMmneRVjG0ge4aabhGMF26fDgPjZiJqDgl-hOLeJ34lZB'
         'fSfEzR3mWJi4uZPtdluCBmRwpJwnuTtH9QR8xU0HzU62x_zE1hmQzcu09qKzFW5-0'
         'JCuXuA.HCYK39pFVNNwis3rsZIG3w'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAABa.5JaB27g7rjJ'
         'JYvjGO6R2_6Cy9TCn-Zw1wNMqt2RLA8UgRC2_N6sUQ_bLS3_sRs0lfBnoPODwXpsL'
         'VjHJRnn6VxCa7_dARGmQj3-Mh5PZooxNOaekplblENz3rbzxUVJlsOgEyVg9KG6wZ'
         'nPw3WGegP5Zp1wGTdI.Tqbyt3NeSrvr0DomFpc15g'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAABP.szHU1b5iLkA'
         '3MvzKtqBdDMW1wjhwpnewKo35xU9IO5HExzY0NZW1I3AGDeZbOv_FDzeetCqw3xQk'
         'nEgwoBu-q1XlvNdlLA3R3N5u1Sl4RdeQSb7zym1lRmp96ltlSLtKsT2s9mcT63SBo'
         '3hVil1PfiCcHWE.LzpxTkCddKMNs_HN2Vy3sQ')
    ],
    'commitment':
        ('eyJhbGciOiJFZDI1NTE5In0.eyJjb21taXRtZW50IjoiQUFBQUFBQUFBQUFBQUFBQ'
         'UFBQUFWdy5BR1llLVMtMXc2Q1RCdXF3TzhXY0dmNEg3T3RheHZ1aS04eWl3dFBUek'
         '9VLkFBQUFBQUFBQUFBQUFBQUFBQUFBSUEuUDZENVVpRFMxckp1WnRua01HWDA5YWx'
         'ZXzlpcU41aUNPNnZrVzg4Zi1ETS5BQUFBQUFBQUFBQUFBQUFBQUFBQVFRLm5fY2Zl'
         'ZmR5cEo3Y0ZWb0lHZnNJTUk5a3k1ZzZoZkhMNThFcEk4WXkwek0uQUFBQUFBQUFBQ'
         'UFBQUFBQUFBQUFZZy5YdXo3dlBwNjBnc25DMXo4SU5Dc1Q4aHRhWlJZQkJkOVpwVX'
         'F2b21fQTJzLkFBQUFBQUFBQUFBQUFBQUFBQUFBS3cuSFlFSFljbl9maGlsckc0TS1'
         '1aHlqaFVNSnlDRzFHUUw3YVdqQzg5Zk1ycy5BQUFBQUFBQUFBQUFBQUFBQUFBQVRB'
         'LkEyemEwR0NXYXdLYW8zOTFORk51OUozQk8wZ2ZGSlhjQVdTbVNCOFN1VWMuQUFBQ'
         'UFBQUFBQUFBQUFBQUFBQUFiUS5zS0RqRTA4SjMxdHRrRzhJVjV2T01LaEZXajJqcF'
         'B3dGYya2Z6OURXZzZVLkFBQUFBQUFBQUFBQUFBQUFBQUFBRlEucDJOWlVWSmFBU0Y'
         '3dk5ZQ0xEenFZdGpqZHlkcWNpTXhPYWNPSTZSaUFOTS5BQUFBQUFBQUFBQUFBQUFB'
         'QUFBQU5nLkZ2MHh0VHRQSGpXNmpDU0RjT2dRN05vSWFqd1EwbmRiREExQV9lSkNzd'
         'FkiLCJzdWIiOiJkaWQ6c3NpZDoxMXFYWFhoYXJyeVhYWFVSbyIsImlzcyI6ImRpZD'
         'pzc2lkOjExcVhYWGhhcnJ5WFhYVVJvIiwicm9sZSI6InN1YmplY3QifQ.BNSikqT6'
         'W-d2PYbUT1Inp3QPcJBC5H2MxwpTyy8U_6fqTnWrlyistn_glMO3_HHIemnmOT-2i'
         'LaqlRqQspoQBg'),
    'iat': 1537086642,
    'exp': 1568622642,
    'sub': HARRY_DID
}
DEVICE_CLAIM_SET_DICT = {
    'claims': [
        ('eyJhbGciOiJ1bnNlY3VyZWQifQ...eyJAY29udGV4dCI6Imh0dHA6Ly9zY2hlbWEu'
         'b3JnIiwiQHR5cGUiOiJUaGluZyIsImlkZW50aWZpZXIiOlt7IkB0eXBlIjoiUHJvc'
         'GVydHlWYWx1ZSIsInNlcmlhbE51bWJlciI6IjQ3MTEtMDgxNSJ9LHsiQHR5cGUiOi'
         'JQcm9wZXJ0eVZhbHVlIiwibmFtZSI6Ik1BQyBhZGRyZXNzIiwidmFsdWUiOiI5Yzp'
         'kZTphZDpiZTplZjo0MiJ9XX0.')
    ],
    'commitment':
        ('eyJhbGciOiJFZDI1NTE5In0.eyJjb21taXRtZW50IjoiQUFBQUFBQUFBQUFBQUFBQ'
         'UFBQUFBQS42d1M4a2c1cEVLQnhPSGcxckJtenp2QndJelRaVXR1UzI1U3h6b0Ezam'
         's4Iiwic3ViIjoiZGlkOnNzaWQ6MTFxWFhYaGFycnlYWFhVUm8iLCJpc3MiOiJkaWQ'
         '6c3NpZDoxMXFYWFhoYXJyeVhYWFVSbyIsInJvbGUiOiJzdWJqZWN0In0.XtCms5Xz'
         '84Sv1fAv3atpG3rMt9-9j30ZUkL_apzHU8Kb9L4UWfHbAZXlcBV3e3PGyxrGHswux'
         'uynUEmxaJCrBQ'),
    'iat': 1537086642,
    'exp': 1568622642,
    'sub': HARRY_DID
}
DEVICE_CLAIM_SET_STRIPPED_COMMITMENT = (
    'eyJhbGciOiJ1bnNlY3VyZWQifQ...eyJzdWIiOiJkaWQ6c3NpZDoxMXFYWFhoYXJyeVhYW'
    'FVSbyIsImlhdCI6MTUzNzA4NjY0MiwiZXhwIjoxNTY4NjIyNjQyLCJjbGFpbXMiOlsiZXl'
    'KaGJHY2lPaUoxYm5ObFkzVnlaV1FpZlEuLi5leUpBWTI5dWRHVjRkQ0k2SW1oMGRIQTZMe'
    'Tl6WTJobGJXRXViM0puSWl3aVFIUjVjR1VpT2lKVWFHbHVaeUlzSW1sa1pXNTBhV1pwWlh'
    'JaU9sdDdJa0IwZVhCbElqb2lVSEp2Y0dWeWRIbFdZV3gxWlNJc0luTmxjbWxoYkU1MWJXS'
    'mxjaUk2SWpRM01URXRNRGd4TlNKOUxIc2lRSFI1Y0dVaU9pSlFjbTl3WlhKMGVWWmhiSFZ'
    'sSWl3aWJtRnRaU0k2SWsxQlF5QmhaR1J5WlhOeklpd2lkbUZzZFdVaU9pSTVZenBrWlRwa'
    'FpEcGlaVHBsWmpvME1pSjlYWDAuIl19.')

CLAIM_SET = ''.join(
    """
    eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAADp
    .7myjSIjl30fFVfsJM02NaSY-qyGgubxlrsszw2Mccn9_m5oYEOEaE0
    -d_ryP_leX_4_a7GtUyYSe3s3UYHXS8KCjnNxNo7_x9xybhNMXA-P1DxxJ6OJFx2pEKjB4BiG
    OsN052XlxUqMi5-DSmHvPxn9nK41BEBZ1-OUJfcFb3dMf8M3JRg_bJY4JJSTU3w9KAfAtW8wN
    ywN52m2FFP_YygFmhbHLwdl2Dz34C70sF6c3YOn59ilXFJnZMpXPuVMwYEVRk6T59MJcfFe5i
    wn202KDDKcGebh116zkAda9BIqt1Yn5mE3whSK_WHIdrhVHbb7yjhAuK5dXrsC0WiHe9QQ3b6
    W1kw5pWbdShgVU3zA2P5ofZUPPa2PJ4UlgKlwWxh18OJJcnzgpoqKERBQzeYmx9OohMQKfBMP
    Z8RMP6NbUHTqtvuIyHDZjuOzbTQDJemLXV2gh236vdjMDxF0ox6XYtPjPLVJYSHM1Z4T9Dnqj
    Rh_b8xVyXlXUu_i6z2GaPKQu2SUHintU1ZWhI7DnCgl0bWlEkYJwJB96-HJZ3AoNUsTI_FUsu
    kYZ4dOAw4Ygb1G0deQZHHBOJfjTOjJtmUS8jiVVVVAKbi3a4nT1I8rnmTwkdg-
    mzhWMM8nLAEmUJY9qll9xTnfO3ue3lYdfwyE9G1Ulsoim0nhQYjXs-qWGBpeN8wuKWEG6HYdP
    B56tYLHuXCQIR79zq2O2717z0n9WKtt26N7DnS2wJJBuSjRNwoNA0tu4rwau9vPAaPW6zbNzJ
    fSPEUYdri4sVBLxw62n_L_Qi3alGvWE1P9DNLphjqUH5bHfStr3iNadLAFuBI-
    V1gJs0ZTOd29DyCd54r1S-gJKkKYNdOO0i-2YNvxvEqi9u1UK4di-sR37kpD70hAf92-KPxvY
    guXjwwrkelcqeK6LBq4EGVnTPy8qaki930Y9rQIjut_LBSuUrsdbIYe9FM3hVvQsLVFcjfWtx
    lRfMLXH9KcpgeaMTGepgbCOMdx0UYVW7irKdxx5CD9-M30_d7oN8fxwNFC98xnGpO-
    zf2fRd80bf5AptE1gGalqdJuD81Hgco7K3ijcSexbaR7Nu2
    -IhtH0OAMyT93ynJzVbylLT15CUYC2UW7z3riH1Z7S-
    4AkGugSgeMnJ97ZlcXtMyEVDGSxOGCImr83fPn7_gfM1ndxTA5QRp5-Vu3KNTAMp9INS08j9N
    Kv0quIrAetMa_Hk0Na0aFtQWJ4JAHKs_85ZHr59W4w4LCybnRx_1Y3q2b5DdQY7KmO9ApOeWy
    lzC23ny5y2ewCPCZqcs20khGmDH91hXpcDRllSTZPNZ5PXpMMcqZCfHugkCvypPT9araVOoQ-
    ENi0W_mX304hfjsMT-3iVikELzJmJGhsN39wA0HqiedtG4pMKfXd44Cpti8wMbOSbogNypemq
    2UcnF6exVx9zoIWlUYTjDIpRphX4Hc3N-jiuGmxvLPEHKy_zZ2t86t3YkUdcWFhdnn7eEIHw-
    QzwZ7g6FQZBtrEGYY2DH4uLv0zOoHl0pOxX7OjYjyFmH5X9VsUz-
    B69zjuIaLITvKp542wNiC4k_Fb-hI-AahmNml_LjwYEOAChHEbCa-
    5cx8JAch1YI_dQY12In34A-
    5tFvgHSfOAXb4243PA0lGErdvFv4IuaIP2cdykW7u7PSBOJiCV0kdszoQtYjov5fhPxpj_Pq-
    XOEfSsp0aH84zkIha4S4IA_9MgUywHM8e_8UbK_Cqrt5xNgRl2QPHTsQiJHHbaMbGpKeTcira
    h_dX0CxWjSHqFqpRPhipKyYp7rpihvCs4cKUNWo0UeZDj61ohOiztG8RCSkXkpbg-
    v84YooVY4wfoVcp2n_ar62WPNs-NeCzt6i7kZ53oVp-
    q5XcOK6PTG5yHccQ8qAXKdAIbYsPFKkUTFSZE-QTi90xauic9quKb1_97l7bDxYJ6krKsdltY
    TD168i17Nd05R7W8PFin6R6mez1Ji5Ei6RlVIMASJlWShDJ-ZvVjTSc3tpJiKbe-gzWOl_nrs
    nEwkoEuUQ4x8QxE95iHlAmwO32To94S9zzKzeAXlF2jsPL35qVo3Jg9TT5ERKe5NJsnMst7EV
    ewEZtdRKi961b4J-RJh0HAeaTCyn1cQLePunXxUwQU-MSLv3aVQl3a9dvsgd9z6gjV7kUdKjH
    8EXAhGKpAjMf3NLzN22n03XbnhtRtFdB5L9pdOaG05QaJb5
    -pxGbW_Yk5USXHgg9lWExKxZQWO4Z1g4cDV2Znn3UompgU-CL76pXxB-wzNIdDJ0Yj5bAHRyU
    hFc0mL5p7_EPhCd4jOlqRPBEUNnlIAxCAz4do71OOJscLWO1EaWmOF5WLSvWO1LNCCd_VztTq
    Owbiex77HlGk-Ednz_W4EUHgxIXc0k6I82yws7geyE4Dm5HvI-Obx0M7oqxLI2ufhMmILgtOm
    s7EuROAQ39iuiHsTVqXAk3AO5xLmof1L4y3Lw1OD5oK5KK0GOx_A3YjrcAe1RyEjnBvBnaFLG
    CoQJ3DVxfRIYYE5JgtZdRmHWuR6A9ox8N0ooM5mv4aGc2Etw4qJDV5DtBJs_s0GB-
    FvrywrghBKPh5xbJ-
    mwpixOS3_45lFwAraKZqewg870NLl9ENY1GdT9UXxGGhNRItZYhLj9xHIliXdd-wcBr62
    --J25jS11ynFZvls4Ex-
    wofPNU2YL9OkBVnGmexPLLHn6sqzkavUvqjtchtSd1ncuQwZ3aggGk-
    OgPNcoKBb3ZuVUYSrWkv7HUj-ERCi9AXtlV7714gVHm-5pqXvOtwTFjQHZ92bWVb4wrZdj-
    xZ7u5gmB-CJqWBKNnuUIzX7y72Rwtpicb7w75LgZELNP-
    I9uL0wSrVaqFo72I0tVKLWkuNUvEte3zKRiTpwNhfk3OqjUJAywmgMgWG2Rfajp2-S_W-
    BkjB-kXkkWmYdrtlsH1Bq-I-n-W4Qnt3GgBbQb_6lqT3lIcQ5Ym8gDWF5kvbqaZtLc2HHlCve
    qXPNMfjqiLLGs8iohbZr0zSfoBGu9D6ZkWhJcpU5dFkanvev7rNE2fD2IGbVnKVYzir7z25TL
    PtN1FFvaUQb2wrDx9rrohjHNUZorL8RfBO0sA4m_olXlUI-yulVz9P7Iy2XSdd8Ddibb-
    Bnoz4OReX2R-urFVZJbiZQbDj8daL0vHm31-HPZmOM56wJE2pqrmkLyJrw7_oBRRsCHIrPyK1
    ZipLQbYBcucHkxtcFmvzGUoS0secaxt0lQWT96iIbWMqfKRN0HYF5
    -WMxAAAz91njaWRO5FxRK4H0lcKemw53xY4JkXBHvYRQ7IUtViKDI32yka_X_FZjJ8_Gsy-tg
    O2n5dTj3ypOKTzNNPZpuFuxer0VBgZEc9yAeFeehwNkk0MZ8SBB1VfJCkZBnAedPpUtykHHYr
    rJN5EtKwnLJKzzuP2XtLDEO8Gc_Wu_QbJnqzDseFtLN03UmHiLYZ-qbWqAbm-HAsD7K8pQwN7
    mNxgXhtuR8CiXKqIfAIjyIVN3SBJxV_W5E84OEQudmFQlIzCUhSSTRL4AHmbKnMSOgvm7vxKq
    OREJqnzSxweS9AO7QUTlpoup1VXUzDbdb7pn_VlfjLWxwJKwzihAVR1x3Tm8zPpONKpKHVy-
    Hm7PKAaFjIc1DrPRsdTr5daZry7oSsg-ps2d3yFEnu49dh55sxGTLu3wJPStAGrkD7os_ZUB9
    HB9hrkIRiuztHySLmMdV2B5c0b_g4Mzt-6GyvIs4pGTRr3LKhiXCX0h6F7qzrvvAo6w1DNwzo
    Lk1vnjMxT52mIsZYuoHezlnSMp54MnrmSf2_kX6hqZMum9C0t267ocHcpbgpzpa0Pdiz_x3n_
    owss4QzSdjE5i0SFjsfAs5n-
    daqQtQ6E7ZnX6TLvObRKVyOYF4pq6KtbeJFPUreL8RqIornki-
    oX3dIKUmsRt4gs7AgTu9C8Inr-vy-t9HHwnkzai4xzL2dApasGE42ajYaX9lbyDc9S0NkB7K9
    PygYdwxMQbZEVaZLcbd52ScJ39IEkTjcyrJ5lSQHM3ZNKlKrl2HO6MTgCnGTrUV2t1fO2perR
    zOtOzRo5yp2BYAYshLvMOVCdBCc_Hldsc3e8rXAhWpdG9VHNUzmg.egN6hdrSoGUlWrr4v42D
    1w
    """.split())
CLAIM_SET_AES256GCM = ''.join(
    """
    eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2R0NNIn0..AAAAAAAAAAAAAADp.fS39LTtZ6kNCaZi
    Sti9afi2XRack4iEy6vDyiSrQ_KajZQ4-KBrwieHKSF8G4fGkMXK0PyJHvYOFkC9Ooe2uM-z7
    q7KPp50AL35yOkeccrtflrJynn5Vnr3HgoUaM6xG20N6gYrGzOYu3hbzW4lOAes_jn_UcJYel
    Xk-A2zeBzwM57jnrWIkbuyHqh3jR__IoJAvm3FA25t5uHQYsUv9QpDJXLlhBK06Kq1y3WQAA4
    90YPljOKhhOqIAgJNbz504W7L_B5ZUxcpWK8KiUIzKtldkwlA8WtdnnnCwkDsDhydB57P4FQD
    gT173zjz6d4PFRB81G4wZ5GiAr8TW3kUm7gd_VCbstlNcMefjFJ5DPM0G7JwDOh-ZpfWozyWY
    BFrcrSmgqnfeHZqeiezeSwtphTeB2XIgrYgPJU48wCYVyILcWTKlFmPy8_5ovM3UVMRBhgeiD
    NBmtLPoDhxT5dK2DmIayV8UrvBuolfq7JB432_EGnxKXhQvjaZXXk2vR7fvJeTiN11F30V_Z1
    g5SZH9CXlXQApyBAxTVFjIIkw2dEmVJUE7QoMNbX1Yr_7n1yFWhFAImA9Z6rThlaOQXXdkVhK
    NWC-PpgRCErkAVSX3nvhucvUAxRbuAKVEvStykjmtaaKEydTqeuOKX77ArRuARBE9H2_PrdUo
    nB2ZNN5SbSDAYRKwQGIvxNWTH2Tmyb9G2zzIBVA9EcnWdEod4R0o5EYZabxvOg9NiPw4mdabi
    ZDkHX2eNc9s35_UVT243Q_HXGgIJcrcWQVxaGtCdRV2XqDcH-dRWg6Yl_0qXURmpoAgRuWEAn
    ZGHtlRpVRyXyVeTh_mQLy005eA15i0SepgYB9_2AKScypXxIoRXnGDO1ycxkZPDmVda9JVjLW
    XhKlYFSEyucrsmfVFNOg6_hOcihD-
    cTlN8ilRcIXper5-zfB5LUVfakMD4T3xLMkNLwP2mnkEpRN27SV1vfRrjAIJjlj9ipWejiv69
    -Ohcc3XKZen4i0AD3jhFBdScCxog6YPmHCnYsod09Oz9YsbnVHdFNG7nANvtjOWDDIaqlgrbE
    ZLBYRD88QDxzzsVWuY_TSnnfX-aYDm5qk0Bw3YZuBjiO80JkUrKUzwYQ22jNODUMl12kV8j8O
    xFnqKq94ZSCyh6nXz5tvsr9ML6BJko9LSMadMwMwOVD5WwuWkj0UoSPIn8hamM8A_eRzMQ-ed
    5TRebARa_CyIyur6a1xMOXjtvxZLSLzuDcNsrwxtxksVZGKpTVn6Js_qGVS23C9dA4lwXLAmD
    DL31tO7JdKZTcYN-yGL66UybeXJ8VKDhpCxd853Am4VlrqaMXiMlY2UsRkKJJU-
    PXvftnKuoZWS-Jl0M-khl68Wz2y61vBAh9UfLqWz5JYTAjxqEqzA-
    brQvQUz05VCOfHWjD_vTcbhaJOv6-XLL42ZmbLn0eidCyphF6nlsttX9g_AR4cjuVqJLQT4hP
    AdqXR8TW0ePIcbHbjUR57HkMqaq30WELpZsY9RevChzXB3HU3WvQDW_q6NcyAdSjgauZyis2r
    -kCvTrQypejF5i-
    OT5psSbU_src2pLlG685fCo1VNlYeBYHmrlRlz3r9Z_GHznbNbiUcsYpumRkXFBIUbMRisAu-
    pKsqUD5tnDzxChyu6JSp1_mvSpWN3UzhBiGuezDaoKBy4j21P9IT8k7YndIZxQrHeW6H5nZN9
    g-Y4fmAijXLarAJwn8sJIa3js7NpbMDT_f5hypzbNrY98aDqyxtqjomNWv9Q_mLecS0pi5UdL
    Trg54Fy774CKMwWlAbgimOun-2VHGfm2Nx43Jfgk6-PfvHTg1I-m9AUDkFUubtK0t18-uUAq0
    TGhdmlF4rMOJIxa3kWRK3bleXMyxhqX4TS2PL6AIoMvbYHcGM5dJH58m3VN0rfJMRDZfzH8Wn
    yj9J0r92RA4RPhW49TH8Pez9x_cFD3wXmP03dfllc5cs0uQFWeeTw3
    -U8FasoJgogpZ3P1GzTtEpXhianegqcN9FrA7pD9JExt-tmYlNvpPxjtfOuQlcUE37s0EPLlI
    hS9YVOSqlbpW6Chocv33TqSq7JoEL8RqM7O7tGWTasrz6sJWi3eRx2GSEyoHfEzY1DUn152yV
    8N8bn-IF527O3EP6uRQ9I9D3VJHng0R2gyiglJVBBRDU6RQ_mS0cEP1GGSyf96mB8FZNTOUw6
    VAQR1tCvfzhoIFSOFHl6E2BgdbuVUXE1SSLL7iZCPKwPZc4jwtPqVS-uhxC-216i9HusZJyTH
    PDoQKq4vYQIx6-K13iv4rhd_JV8NoA2c0Krym2FPOdKiismzJAzuR2MXxDGmJE1OpQicMkqXu
    hhWpbFO5ORuVdlOTpc9-lg1rPv2j1V10MPZLxu3NTtusxlha_CRMROIRHgbZSln9nK4JwRUko
    K2p5AVpDI_5K5hx9TjSj72vyPsszRMHi9mJY47GYr1okFAC95v01ku0iM0DHYXHO1gcrxnreR
    Ygj_THXm5RW5rBj_WYegRwx6SesBP9c8pXM-
    4VIrLAkLkq6Nd_NjxAsyocaYoBZRsn7C6R9yzVV63jHMLHToVYuzkCOSBf0QP22B3O-
    HmCX1iGanDXN7fKBGQ_cn-LeQblgP9cjD0UnOHexSGJDW6-Mi4PJP82PuNCg7Nj6loSKuR0P9
    HoIg3veh0ZHaUvJ28o1SHbUDvY8kbhWiEfgyi786zGHVjuz-b57hixRBsl-3Pk1JD6vgGKh8K
    YlmKzabQxPBYSXYksS80Ezu2sUCdkvfrZfKVlldJsXVgJGMZ9iFY8dkW0DIBQB-
    JYIQrfD37JXF-kRSp6BTXPpgW7ZG-ZJkjQ1fRLT1xud7BxQ8yU_g96r7Pu7ZjnHq9WlaTlaOs
    DSs3o5AAJHoUOQKTXnBKIeD6EeHlb_cji_CKco6ez4a9xshk49kK-MZKviwByKqP545wrTr9U
    z0bCfEZDnWfZk8T9Rj9f0B7BOR8PbLqDJZHnQPZOq7w704mWGm6GaZhTaQxyqjb6faTslRtvP
    yArDy-Yuhq1By0zjGFUPLHNb5Ot6JDezkg9wyp6Qbg3tLjk754lausFk4lMHt_j-
    pHBU6h8Ige5BNy0u_En-YSn-
    eEqgMywZrfQn6MXXaZaTOdiBxOxCzYOJ_xpF8OyjGEwoJDWn7l-y1YfcZKuHwWY2_ELPPPX2g
    KcWMWmoKxRqjCa19vUYyHbYza09yWUMEHoygSZRmEMSMfgPkUd9fpjtntwbTATq0i7W1Qy6Cu
    NIGGlZFn7jBfqLIEiwZt5Z-AthFIXV9ITclw5Am55aZsRCnyBHgyxGAIbXsjj-
    9eDC8zRndlVpjNQWC3kgCz30QFkAfFO_it8HzetQ2du5akr6
    -fR9Q3CVdZj3b1RL9AVoACDSCAJHP-
    o86jwqSw0Jj37S6xYLPMBNZ44JnF7ipDu1HFHPSugbhP0XBqf5r_jwC-
    Vhc4GZwtzx7_rPfQvg3rkaDQKOhoux832EuZFAIaWt-
    A9GGG1teXSLXoFoVdcQZIpMEh4seR4qhs10uJwhrp-9SsFaTUgPE5fGAJQ34WCe8GxIa0jn-
    lagTQ50XTr6h5ovsmnuGTd6hPdP10p8yI4URtZBjyIfClYf2N9eCyaQxQ0Qf6hEK-F7y7NHCf
    JoicSztPtDmo8kKWtc6GG13BLUDCSFLDjxbllxY6s6lDb7V2J7qWjfvfKnzt9gLZCYWWyzUHn
    bqXsky40zRGV4VOC9aVazpcvthsAqe_k35f5I-BLdzzPQoOvM-
    NlG3sFvZP60JtGlhDUdGI_xD-aaauLymNUZcYsJm6w09px6bhQhhvMBCj0i-ZJ-
    0Wj0Slkm7UISBa6ZWwKP6eWqM7JFaMRSKou0u0vA-
    9Q_meAtETd6hLthSaf4fkkHbei3_PyfFYBhnNkG-f85x4U_kd8xZ3I1ee9MCHoWzzUd2yeGj-
    nvPzZ37CnUVHpvW49XMT_C5ov5Dn4EksE5HRUQHHwB2yPvSGOaXaDyXArJIAKAfqEDrg19oKv
    5c02hQKJg1KjW3H5S0NYG-BgfaroDaTwSNLDFKeBE-WvdMjKH-DYhtPnctm05DZYUAk6yHWBZ
    jr8dFoBs6YIVJ7hLLQGfbIt1PxhukstNmsi6eC550zx0jxy_sbPOB7ElGZon9IS4WBEi56uAh
    q1lB5V0CPg.l4_dBV2v_kdwwXYsfQeRTA
    """.split())
DEVICE_CLAIM_SET = ''.join(
    """
    eyJhbGciOiJ1bnNlY3VyZWQifQ...eyJzdWIiOiJkaWQ6c3NpZDoxMXFYWFhoYXJyeVhYWFVS
    byIsImlhdCI6MTUzNzA4NjY0MiwiZXhwIjoxNTY4NjIyNjQyLCJjbGFpbXMiOlsiZXlKaGJHY
    2lPaUoxYm5ObFkzVnlaV1FpZlEuLi5leUpBWTI5dWRHVjRkQ0k2SW1oMGRIQTZMeTl6WTJobG
    JXRXViM0puSWl3aVFIUjVjR1VpT2lKVWFHbHVaeUlzSW1sa1pXNTBhV1pwWlhJaU9sdDdJa0I
    wZVhCbElqb2lVSEp2Y0dWeWRIbFdZV3gxWlNJc0luTmxjbWxoYkU1MWJXSmxjaUk2SWpRM01U
    RXRNRGd4TlNKOUxIc2lRSFI1Y0dVaU9pSlFjbTl3WlhKMGVWWmhiSFZsSWl3aWJtRnRaU0k2S
    WsxQlF5QmhaR1J5WlhOeklpd2lkbUZzZFdVaU9pSTVZenBrWlRwaFpEcGlaVHBsWmpvME1pSj
    lYWDAuIl0sImNvbW1pdG1lbnQiOiJleUpoYkdjaU9pSkZaREkxTlRFNUluMC5leUpqYjIxdGF
    YUnRaVzUwSWpvaVFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRUzQyZDFNNGEyYzFjRVZM
    UW5oUFNHY3hja0p0ZW5wMlFuZEplbFJhVlhSMVV6STFVM2g2YjBFemFtczRJaXdpYzNWaUlqb
    2laR2xrT25OemFXUTZNVEZ4V0ZoWWFHRnljbmxZV0ZoVlVtOGlMQ0pwYzNNaU9pSmthV1E2Yz
    NOcFpEb3hNWEZZV0Zob1lYSnllVmhZV0ZWU2J5SXNJbkp2YkdVaU9pSnpkV0pxWldOMEluMC5
    YdENtczVYejg0U3YxZkF2M2F0cEczck10OS05ajMwWlVrTF9hcHpIVThLYjlMNFVXZkhiQVpY
    bGNCVjNlM1BHeXhyR0hzd3V4dXluVUVteGFKQ3JCUSJ9.
    """.split())
CLAIM_SET_STRIPPED_COMMITMENT = ''.join(
    """
    eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAAAA
    .AjAqymkSdCFyHQBlpOrK0cOaE_6_wHlewXvYRAm6bBqQVfTZFtHV5TRMeDz-
    HJlF6P8XflBik16Pt6Y9otussfxIxXWAxrMVuQMZqKAgQ_kKWoPJj-
    WmZfhn5tpMDG3H_JRrQYMtwtYb1mFxjZFueeK8X6fuhJWbiXVFw4W2ThO-
    2VauPiozSEq2lUNJr2joIDpzeqVSwMzpqCe-pJL0MfkRhBns8FdKRN08-tzN-
    bbP9N_KLC31YvskBaeIOfiv8qKqpl-
    oRrlUqt4S1ZsX8fUX5LlgeQPuaN4Vu4GxMYWxfpaRMSumv-2XraczUHb_WeOs19
    -vvasAyb4c0OnL2tD8V7gfIo-
    1O02xyvjCAFdfpA9MF9qefpnM4fMhKXqZozxHqRrgOEO_ZMeOfrsR43CTkLOn8s-
    FF9otB7xabwylZOnPOgO8X-iqfBnmUxlm7tO3aPUY87iGwfOJIUp6YoOPp1d6q4N3
    -AWDJ3_5np7F-Xz8zQrvvxxkOHbzVXBtCuLju2RYglsadlZWi0xG4mzZKubVmZQtnlzqHAjJq
    UjA4lK6V0AmfqBYIgqz0AV5_Gc8QtZhessBtLIxsS_9yhsWYZYFo8nhkauglO5nXuRDv70E60
    PKRVmjniXOf7nj1GIfG3XH-VwVspfdV8QWPHoI3wb21dVjLbKJKzbBaemGgE2eo-NQaCqHkjm
    zCcFGFnK9UL77sdmFATKroQQJuY1oqLefoIjfpH21EErv4DtGrOq4UssOYgVhiZGJjysBWPyw
    B9AeULd_XKf8cBFjI4duYdD2IccBmOa4EE7xmzFQ_d1QH-2AcnWB7nZFA8wLr7X-knbFaCl7L
    v5POtVPbQkm_G54l2uYjxfpv0mtSNWHPDb_tSzB8X5TreOZMWWadVpGHaPiedh_7e2VDqR2wm
    Xi-8sfBMplMs9GQe-PSJlXvWtFgUG_eIZB35y4T0aJHJVSHy-
    Ei_pOAX3ZCWQ5mBgrITUr_HRL9HNvFbfQS_bE6NpJKJhohoIceCg-
    DBmtfLSimK3PRL9KxQJ_X-bSFqng7M-LOvLX1r4bUvlh8cIiU3QdD7UY2xvzwPMPYO6W9c-
    fs7Gf-GxxlutM4auVH3lB3okr0w4ABfTUuOjdZjKm7N2-JkGCBN3ZLlh5x6jejbLebaghB0x4
    qIVrLL47v52LedEeOoRp0j6_T9FW8QmInOaYjRLS1dU1-TlNeru6bkt41q3eHBupkAbGuArYG
    KiMuUDXGEOCw9bgnFCGP7ZTMx1kiiM5Wae3Jq18ESQDBRqZbBkrHcBXDct5VhbSbN_PrL_Z2t
    yRRZq70OVeqUjyd-qtio5P8AyVHcWq0-5wTgTAHQDN72eQANu2zzh5zIBPqGv40Dnxxl1HNhz
    JdYrxRrz9LzmX1JRxpAYf3ma9U9crSlC8IVndqWr4iFjYasdJdjrersYHmmRygxouPjNlpZxQ
    JVAvJSgCtYDO3GL_4e3eYQjoditZAW5D5ywEerGdaLGKXCEuddi5GWutyIyxwe6KuX7-Kp8J3
    YMfA469zZp3c5m4keMXReQEVH5YPLT2VLGetxtfszFZixXsYx2LXfhLoq_fw4Dy5SBXCFrQrG
    ZwgzAmgJSVGCOcKELsg6NEWbncI9mJU0wvVPs5Ko5pohZR-0jNs1B7gtBVBwpIgDhcVdIb2iD
    5DQtOdT7JQ5qT1h95LckukcOGMwtzLhStWHDZ4xsEwxHRey6G7BWL9b8exqVKHgBjLhIvBU7g
    dinRUdpuwg1E5KJWJI0jgRfEcGZVDHIA_BBAK65uRH7qIy-9OA-D9pC8QmNSo2EU4Uae6brB5
    VDll7AvIZZX8FGXwibym2Cc9kMFGsVXbq9s26yCDVGBRwnh8RtOIMgZpgPi_eQTJrcfwlUGoq
    2NvmE89kocYiyaBQG7IzXtrrtpP-I_YIxcBKnDpjDQqtUiY4IETwvZpaaG60F-
    D3xL78ZsOyLQEescazragIoxPe1StX-lv_dlDqAbt4QwWmv0tV-DMQpMXxmIfhhegOidGky9a
    48vEN4KtjOyazYl0eStOlI2GZQokjLjPPOrglBMwy7_e2r3bQEbdCwgaxPRx8OKPs7Eq9lUi4
    HAx4iiqg93DP12LDNbFl9OHLDeWbN3kF3mXx44r7fOjHEH0VZtng9rfCMd8yZzz12TjzFMu91
    VxClnU8WYb-nYt4V3ENZjQIs4ISThclcv95ekUcBRubG7mlq_j9uceKp9jEItOl2WsD0PKoEb
    2T5Gr7fkwt_QNJGao5quyuW-lG6GIl6Pu3TIWXRj2rmRpz-
    0k2GJNmEr38G8FhXqW0tAHIw4bmcEa5yUephH82r1A7olrASJeE79FYs1hzDnsQYBpgrSg_-4
    1UkIdH6b5Dcr27vOmIHbryA_ThWqa07DzRYZ8xgJASoNlZwSRpt_SidfBI6pwdxrzVMu0sTZ8
    tkxGPcV_79mZKu_9AGCBEuQMS8kUbIegcORY7PXg9s3d3puNzgViABWp6Pp-I6V-
    2t98evg83ep497Y7Df7TTu4qUJryFuF-Kf_sVqY_kuaBmmTDK9EOD421BJxSyKauSXCP_lyrs
    Ws46PJZjAHMnlxh_9UY3yD5lIwWJUPHxhPxlzKHYCq-7fwVFjmvBG2PWJEBo8aPmIi-
    ug6zagkWcm4WdvbTLVKO0YCVqaIpGOiJMolyZnV-FGeB0UDAmil.u0B-
    PNfPJ92AeCQI7XgAFg
    """.split())
CLAIM_SET_KEYS = {
    'claim_keys': {
        '0': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAk'},
        '1': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANI'},
        '2': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPM'},
        '3': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQ'},
        '4': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAN0'},
        '5': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP4'},
        '6': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB8'},
        '7': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMc'},
        '8': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOg'}
    },
    'claim_set': CLAIM_SET,
    'claim_type_hints': {
        '0': 'Person.birthDate',
        '1': 'Person.address/PostalAddress.addressCountry',
        '2': 'Person.address/PostalAddress.addressLocality',
        '3': 'Person.givenName',
        '4': 'Person.address/PostalAddress.addressRegion',
        '5': 'Person.address/PostalAddress.streetAddress',
        '6': 'Person.familyName',
        '7': 'Person.address/PostalAddress.telephone',
        '8': 'Person.address/PostalAddress.postalCode'},
    'object_key': {
        'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
        'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJs'},
    'trace_key': None,
    'attestation_id':
        'kauriid:attestations/00000000-1111-2222-3333-444444444444'
}
CLAIM_SET_TRACE_KEY = {
    'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
    'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFA'
}

CLAIM_SET_KEY_DICT_AES256GCM = {
    'claim_keys': {
        '0': {
            'kty': 'oct', 'alg': 'A256GCM', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAk'},
        '1': {
            'kty': 'oct', 'alg': 'A256GCM', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANI'},
        '2': {
            'kty': 'oct', 'alg': 'A256GCM', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPM'},
        '3': {
            'kty': 'oct', 'alg': 'A256GCM', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQ'},
        '4': {
            'kty': 'oct', 'alg': 'A256GCM', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAN0'},
        '5': {
            'kty': 'oct', 'alg': 'A256GCM', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP4'},
        '6': {
            'kty': 'oct', 'alg': 'A256GCM', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB8'},
        '7': {
            'kty': 'oct', 'alg': 'A256GCM', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMc'},
        '8': {
            'kty': 'oct', 'alg': 'A256GCM', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOg'}
    },
    'claim_set': CLAIM_SET_AES256GCM,
    'claim_type_hints': {
        '0': 'Person.birthDate',
        '1': 'Person.address/PostalAddress.addressCountry',
        '2': 'Person.address/PostalAddress.addressLocality',
        '3': 'Person.givenName',
        '4': 'Person.address/PostalAddress.addressRegion',
        '5': 'Person.address/PostalAddress.streetAddress',
        '6': 'Person.familyName',
        '7': 'Person.address/PostalAddress.telephone',
        '8': 'Person.address/PostalAddress.postalCode'},
    'object_key': {
        'kty': 'oct', 'alg': 'A256GCM', 'use': 'enc',
        'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJs'},
    'trace_key': {
        'kty': 'oct', 'alg': 'A256GCM', 'use': 'enc',
        'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFA'}
}
CLAIM_SET_UNCOMMITTED = ''.join(
    """
    eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAADp.7myjSIjl30fFVfsJM02NaSY-qyGgubxlrsszw2Mccn9_m5oYEOEaE0-d_ryP_leX_4_a7GtUyYSe3s3UYHXS8KCjnNxNo7_x9xybhNMXA-P1DxxJ6OJFx2pEKjB4BiGOsN052XlxUqMi5-DSmHvPxn9nK41BEBZ1-OUJfcFb3dMf8M3JRg_bJY4JJSTU3w9KAfAtW8wNywN52m2FFP_YygFmhbHLwdl2Dz34C70sF6c3YOn59ilXFJnZMpXPuVMwYEVRk6T59MJcfFe5iwn202KDDKcGebh116zkAda9BIqt1Yn5mE3whSK_WHIdrhVHbb7yjhAuK5dXrsC0WiHe9QQ3b6W1kw5pWbdShgVU3zA2P5ofZUPPa2PJ4UlgKlwWxh18OJJcnzgpoqKERBQzeYmx9OohMQKfBMPZ8RMP6NbUHTqtvuIyHDZjuOzbTQDJemLXV2gh236vdjMDxF0ox6XYtPjPLVJYSHM1Z4T9DnqjRh_b8xVyXlXUu_i6z2GaPKQu2SUHintU1ZWhI7DnCgl0bWlEkYJwJB96-HJZ3AoNUsTI_FUsukYZ4dOAw4Ygb1G0deQZHHBOJfjTOjJtmUS8jiVVVVAKbi3a4nT1I8rnmTwkdg-mzhWMM8nLAEmUJY9qll9xTnfO3ue3lYdfwyE9G1Ulsoim0nhQYjXs-qWGBpeN8wuKWEG6HYdPB56tYLHuXCQIR79zq2O2717z0n9WKtt26N7DnS2wJJBuSjRNwoNA0tu4rwau9vPAaPW6zbNzJfSPEUYdri4sVBLxw62n_L_Qi3alGvWE1P9DNLphjqUH5bHfStr3iNadLAFuBI-V1gJs0ZTOd29DyCd54r1S-gJKkKYNdOO0i-2YNvxvEqi9u1UK4di-sR37kpD70hAf92-KPxvYguXjwwrkelcqeK6LBq4EGVnTPy8qaki930Y9rQIjut_LBSuUrsdbIYe9FM3hVvQsLVFcjfWtxlRfMLXH9KcpgeaMTGepgbCOMdx0UYVW7irKdxx5CD9-M30_d7oN8fxwNFC98xnGpO-zf2fRd80bf5AptE1gGalqdJuD81Hgco7K3ijcSexbaR7Nu2-IhtH0OAMyT93ynJzVbylLT15CUYC2UW7z3riH1Z7S-4AkGugSgeMnJ97ZlcXtMyEVDGSxOGCImr83fPn7_gfM1ndxTA5QRp5-Vu3KNTAMp9INS08j9NKv0quIrAetMa_Hk0Na0aFtQWJ4JAHKs_85ZHr59W4w4LCybnRx_1Y3q2b5DdQY7KmO9ApOeWylzC23ny5y2ewCPCZqcs20khGmDH91hXpcDRllSTZPNZ5PXpMMcqZCfHugkCvypPT9araVOoQ-ENi0W_mX304hfjsMT-3iVikELzJmJGhsN39wA0HqiedtG4pMKfXd44Cpti8wMbOSbogNypemq2UcnF6exVx9zoIWlUYTjDIpRphX4Hc3N-jiuGmxvLPEHKy_zZ2t86t3YkUdcWFhdnn7eEIHw-QzwZ7g6FQZBtrEGYY2DH4uLv0zOoHl0pOxX7OjYjyFmH5X9VsUz-B69zjuIaLITvKp542wNiC4k_Fb-hI-AahmNml_LjwYEOAChHEbCa-5cx8JAch1YI_dQY12In34A-5tFvgHSfOAXb4243PA0lGErdvFv4IuaIP2cdykW7u7PSBOJiCV0kdszoQtYjov5fhPxpj_Pq-XOEfSsp0aH84zkIha4S4IA_9MgUywHM8e_8UbK_Cqrt5xNgRl2QPHTsQiJHHbaMbGpKeTcirah_dX0CxWjSHqFqpRPhipKyYp7rpihvCs4cKUNWo0UeZDj61ohOiztG8RCSkXkpbg-v84YooVY4wfoVcp2n_ar62WPNs-NeCzt6i7kZ53oVp-q5XcOK6PTG5yHccQ8qAXKdAIbYsPFKkUTFSZE-QTi90xauic9quKb1_97l7bDxYJ6krKsdltYTD168i17Nd05R7W8PFin6R6mez1Ji5Ei6RlVIMASJlWShDJ-ZvVjTSc3tpJiKbe-gzWOl_nrsnEwkoEuUQ4x8QxE95iHlAmwO32To94S9zzKzeAXlF2jsPL35qVo3Jg9TT5ERKe5NJsnMst7EVewEZtdRKi961b4J-RJh0HAeaTCyn1cQLePunXxUwQU-MSLv3aVQl3a9dvsgd9z6gjV7kUdKjH8EXAhGKpAjMf3NLzN22n03XbnhtRtFdB5L9pdOaG05QaJb5-pxGbW_Yk5USXHgg9lWExKxZQWO4Z1g4cDV2Znn3UompgU-CL76pXxB-wzNIdDJ0Yj5bAHRyUhFc0mL5p7_EPhCd4jOlqRPBEUNnlIAxCAz4do71OOJscLWO1EaWmOF5WLSvWO1LNCCd_VztTqOwbiex77HlGk-Ednz_W4EUHgxIXc0k6I82yws7geyE4Dm5HvI-Obx0M7oqxLI2ufhMmILgtOms7EuROAQ39iuiHsTVqXAk3AO5xLmof1L4y3Lw1OD5oK5KK0GOx_A3YjrcAe1RyEjnBvBnaFLGCoQJ3DVxfRIYYE5JgtZdRmHWuR6A9ox8N0ooM5mv4aGc2Etw4qJDV5DtBJs_s0GB-FvrywrghBKPh5xbJ-mwpixOS3_45lFwAraKZqewg870NLl9ENY1GdT9UXxGGhNRItZYhLj9xHIliXdd-wcBr62--J25jS11ynFZvls4Ex-wofPNU2YKs.3CjC4zASqZnYaQXevGcjsw
    """.split())
CLAIM_SET_KEYS_UNCOMMITTED = {
    'claim_keys': {
        '0': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAk'},
        '1': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANI'},
        '2': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPM'},
        '3': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQ'},
        '4': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAN0'},
        '5': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP4'},
        '6': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB8'},
        '7': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMc'},
        '8': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOg'}
    },
    'claim_set': CLAIM_SET_UNCOMMITTED,
    'claim_type_hints': {
        '0': 'Person.birthDate',
        '1': 'Person.address/PostalAddress.addressCountry',
        '2': 'Person.address/PostalAddress.addressLocality',
        '3': 'Person.givenName',
        '4': 'Person.address/PostalAddress.addressRegion',
        '5': 'Person.address/PostalAddress.streetAddress',
        '6': 'Person.familyName',
        '7': 'Person.address/PostalAddress.telephone',
        '8': 'Person.address/PostalAddress.postalCode'},
    'object_key': {
        'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
        'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJs'},
    'trace_key': {
        'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
        'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJA'}
}
CLAIM_SET_DICT_UNCOMMITTED = {
    'claims': [
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAACw.JfaMCeqUTaw'
         'yF3z5kpFxTFeaq_qDlmIOAz-j0sfPhzlXTKNCnpgXHH62nGkpVn3B0qfU-iT3_D3n'
         'tR2cfhXbF7MTc22P6djzxaI.vtOZc4uc7IyiA_wCGetJZQ'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAAB5.zY2osffTQ6q'
         'xB7KO_iLGxkw2AB-UaCvW4RMIWKVDwpcjXo2eqJ4NTSHtYDCBqi5OmB4OB6ykFHe8'
         'o_DRFocLIkQEpnh0y8he2epmeYP7GZY8HB5oowSll2IRkDJTCaPSk8SoUOx5qqECn'
         'uXoIbf28kTPDnnT1z8.kUd6aQBDSAVsNpikPeRt3Q'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAACa.Y8UXHvNkpZr'
         'nullpvrF4x9YpSjeZH16PW_xL4_HE5hLp9Mf5H5q53QnaBPie9qIqwg783MjpdZJF'
         'shCCFRBb6v8ztodIu-HHuCwJ0n3VVL940wX46b4BNZ7dTDMZT137CkMtivfDwFJf-'
         'nHbsgIJf_xiemA7bcXrMg.aPOuFiThvjEohl-7WvVdkw'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAAC7.qPLk0vNRL1N'
         '1nwOwDwEbLWp-qduVCU8yVeCDxlFx4lLjb-3k3gJUSoMcDeXtKHV8P0oyQjmk96rR'
         'g8c01nZ6295poAGArtfMS-d-.vH2l9MEhxglYs29hm8KiOQ'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAACE.Fbze_rodQn3'
         'Z_ldvxY14SO9aUo1jihb6qioDkU1uGI6ER-2IW4Pu9MtCJrbGMZGCRzQBOvotmqaF'
         'vedHOdb5zz3irI0myKCCf55QR2G8evogcMHp3IDoj7a98L57IQa3V6VGuHhcG_xzI'
         'FmHkpM.piEbWGgg3cZ6Vc3sjdGpYg'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAACl.VUObrFhoh8O'
         'vBZohPULh_v4jtkI7j9A2AmuCrtdvtle1WOAhj29fbCa8w-FKfXzt9p_nOLHUkGl-'
         'n4TN8iRvw2LY2Swx9nTjUfv4BBMnn7JWsop9H9qqXsdMOR6WxsES7jb4rnA2-xiE0'
         'FxsfW8Iq2KtfKbHJQ.K-UUn7xNTp8Q-StNXByNRg'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAADG.QRcfJA276jq'
         'V7i4vvSKPWlhQj6T-W-0EzEsc92zB7w1dvN_c79EYbWvc4gL3_Q_P6gU8_utW5jke'
         'Te-67pZ88VLTMzUhdNs.8zup8T8DZrROF8xOmpwrpQ'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAABu.9VokxOyTobd'
         'oF3wAyRMgzS-Mw7EN-R4tamGRsWlTg3I1sDpoQbH7LN1NCxFRFa2BRjOHG8Z03ZGJ'
         '7DLZqRHJzomrUpJWf_r_pz2FO1JQgwbcZmQBpzM4WLBNwYoVLofo5LHLKFqMArg0a'
         'ZGD6fb5S5mJm-E.6r-rvlezEBnvzbGlIx5a1A'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAACP.N6XRqvJ3Mtl'
         'D-XK9BdHiC0CLtiAmzRGk6nQ10S5OPMfn4VPILiV56vcz1rE6huhSX09iiW8FNiq9'
         'K9HD0zYhWkOarmiTxFSS7jAZCD7ugafLtlrfjcrBA_tYs0MHyZXE72cp46RxNamgV'
         '0LoyQ.GL58Yme7_2b37LYyB1CAmA')
    ],
    'iat': 1537086642,
    'exp': 1568622642,
    'sub': HARRY_DID
}
CLAIM_SET_COMMITMENT = ''.join(
    """
    AAAAAAAAAAAAAAAAAAAAVw.AGYe-S-1w6CTBuqwO8WcGf4H7Otaxvui-8yiwtPTzOU.
    AAAAAAAAAAAAAAAAAAAAIA.P6D5UiDS1rJuZtnkMGX09alY_9iqN5iCO6vkW88f-DM.
    AAAAAAAAAAAAAAAAAAAAQQ.n_cfefdypJ7cFVoIGfsIMI9ky5g6hfHL58EpI8Yy0zM.
    AAAAAAAAAAAAAAAAAAAAYg.Xuz7vPp60gsnC1z8INCsT8htaZRYBBd9ZpUqvom_A2s.
    AAAAAAAAAAAAAAAAAAAAKw.HYEHYcn_fhilrG4M-uhyjhUMJyCG1GQL7aWjC89fMrs.
    AAAAAAAAAAAAAAAAAAAATA.A2za0GCWawKao391NFNu9J3BO0gfFJXcAWSmSB8SuUc.
    AAAAAAAAAAAAAAAAAAAAbQ.sKDjE08J31ttkG8IV5vOMKhFWj2jpPwtf2kfz9DWg6U.
    AAAAAAAAAAAAAAAAAAAAFQ.p2NZUVJaASF7vNYCLDzqYtjjdydqciMxOacOI6RiANM.
    AAAAAAAAAAAAAAAAAAAANg.Fv0xtTtPHjW6jCSDcOgQ7NoIajwQ0ndbDA1A_eJCstY
    """.split())

ADDITIONAL_PROV_NAMESPACES = {
    # AA
    'aa': 'http://www.aa.co.nz/ns/prov#',
    # ASB
    'asb': 'http://www.asb.co.nz/',
    # NZ Transport Authority
    'nzta': 'http://www.nzta.govt.nz/',
    # NZ Watercare
    'watercare': 'http://www.watercare.co.nz/',
    # Harcourts real estate agent
    'harcourts': 'http://www.harcourts.co.nz/',
    # NZ Department of Internal Affairs
    'dia': 'http://www.dia.govt.nz/',
    # NZ Post
    'nzpost': 'http://www.nzpost.co.nz/'
}
GENERIC_VERIFICATION = (
    'kauriid:procedures/evidence_checks/ipns/QmNXXXgenericXXX...51Q')
GENERIC_MANUAL_VERIFICATION = (
    'kauriid:procedures/evidence_checks/ipns/QmXXXXgenericmanualXXX...uco')
OLD_MANUAL_VERIFICATION = (
    'kauriid:procedures/evidence_checks/ipns/QmQXXXoldmanualXXX...ETw')
CURRENT_MANUAL_VERIFICATION = (
    'kauriid:procedures/evidence_checks/ipns/QmdXXXcurrentmanualXXX...YgV')
GENERIC_AUTO_VERIFICATION = (
    'kauriid:procedures/evidence_checks/ipns/QmUXXXautogenericXXX...1Xg')
OLD_AUTO_VERIFICATION = (
    'kauriid:procedures/evidence_checks/ipns/QmWXXXautooldXXX...FtL')
CURRENT_AUTO_VERIFICATION = (
    'kauriid:procedures/evidence_checks/ipns/QmcXXXautocurrentXXX...3GN')
PROVN_DATA = (
    'document\n'
    '  prefix kauriid <http://kauriid.nz/ns/prov#>\n'
    '  prefix dcterms <http://purl.org/dc/terms/>\n'
    '  prefix did <did:>\n'
    '  prefix aa <http://www.aa.co.nz/ns/prov#>\n'
    '  prefix nzta <http://www.nzta.govt.nz/>\n'
    '  prefix watercare <http://www.watercare.co.nz/>\n'
    '  \n'
    "  entity(kauriid:attestations/00000000-1111-2222-3333-444444444444,"
        " [prov:content=('foo', 'bar')])\n"  # noqa: D102
    '  activity(kauriid:evidenceVerification/'
        '00000000-1111-2222-3333-444444444444, -, -)\n'
    '  activity(kauriid:identityAttestation/'
        '00000000-1111-2222-3333-444444444444, -, -,'
        ' [dcterms:hasPart=\'kauriid:evidenceVerification/'
        '00000000-1111-2222-3333-444444444444\'])\n'
    '  wasInformedBy(kauriid:identityAttestation/'
        '00000000-1111-2222-3333-444444444444,'
        ' kauriid:evidenceVerification/00000000-1111-2222-3333-444444444444)\n'
    '  actedOnBehalfOf(aa:staff/2omXXXbobXXX...WEG, aa:Organisation, -)\n'
    '  wasAssociatedWith(kauriid:identityAttestation/'
        '00000000-1111-2222-3333-444444444444, aa:staff/2omXXXbobXXX...WEG, -,'
        ' [prov:hadRole="kauriid:attester"])\n'
    '  wasAssociatedWith(kauriid:evidenceVerification/'
        '00000000-1111-2222-3333-444444444444, aa:staff/2omXXXbobXXX...WEG,'
        ' kauriid:procedures/evidence_checks/ipns/'
        'QmdXXXcurrentmanualXXX...YgV, [prov:hadRole="kauriid:verifier"])\n'
    '  wasGeneratedBy(kauriid:attestations/'
        '00000000-1111-2222-3333-444444444444,'
        ' kauriid:identityAttestation/00000000-1111-2222-3333-444444444444, -,'
        ' [prov:generatedAtTime="2018-09-16T08:30:42+00:00" %% xsd:dateTime])'
        '\n'
    '  entity(kauriid:attestations/ipns/QmTXXXattestation00XXX...Ftu,'
        ' [prov:type="prov:Collection"])\n'
    '  hadMember(kauriid:attestations/ipns/QmTXXXattestation00XXX...Ftu,'
        ' kauriid:kauriid:attestations/00000000-...-444444444444)\n'
    '  used(kauriid:identityAttestation/00000000-1111-2222-3333-444444444444,'
        ' kauriid:attestations/ipns/QmTXXXattestation00XXX...Ftu, -)\n'
    '  wasDerivedFrom(kauriid:attestations/00000000-1111-2222-3333'
        '-444444444444, kauriid:attestations/ipns/'
        'QmTXXXattestation00XXX...Ftu, -, -, -)\n'
    '  entity(kauriid:evidence/11111111-2222-3333-4444-555555555555,'
        ' [dcterms:source="nzta:dl/DJ034005-v193",'
        ' dcterms:description="original document"])\n'
    '  used(kauriid:identityAttestation/00000000-1111-2222-3333-444444444444,'
        ' kauriid:evidence/11111111-2222-3333-4444-555555555555, -)\n'
    '  used(kauriid:evidenceVerification/00000000-1111-2222-3333-444444444444,'
        ' kauriid:evidence/11111111-2222-3333-4444-555555555555, -)\n'
    '  wasDerivedFrom(kauriid:attestations/00000000-1111-2222-3333'
        '-444444444444,'
        ' kauriid:evidence/11111111-2222-3333-4444-555555555555, -, -, -)\n'
    '  entity(kauriid:evidence/22222222-3333-4444-5555-666666666666,'
        ' [dcterms:source="watercare:customers/4711/invoices/201809.pdf",'
        ' dcterms:description="digital scan"])\n'
    '  used(kauriid:identityAttestation/00000000-1111-2222-3333-444444444444,'
        ' kauriid:evidence/22222222-3333-4444-5555-666666666666, -)\n'
    '  used(kauriid:evidenceVerification/00000000-1111-2222-3333-444444444444,'
        ' kauriid:evidence/22222222-3333-4444-5555-666666666666, -)\n'
    '  wasDerivedFrom(kauriid:attestations/00000000-1111-2222-3333'
        '-444444444444,'
        ' kauriid:evidence/22222222-3333-4444-5555-666666666666, -, -, -)\n'
    'endDocument')
ANCESTOR_ID = 'kauriid:attestations/ipns/QmbXXXattestation01XXX...PTU'
ANCESTOR_OBJECT_KEY = {
    'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
    'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACR'}
ANCESTOR_TRACE_KEY = {
    'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
    'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACS'}
ATTESTATION_TRACE_KEY = {
    'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
    'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKc'}
ATTESTATION_ID = 'kauriid:attestations/00000000-1111-2222-3333-444444444444'
ATTESTATION = ''.join(
    """
    eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAACc.aBvK5iDzAJKs_WHgf1NIlS-x_k_J_PaMjPP2WpjrNt-WgjS-xp1pTvLep_Q0UNUcvqzpxdSdXm-j0LcwBPHZYjtgeB-v4GAPnM8qvXtPkyCpkVMIiln0R5dt9ctMJ9XOreMxTHxogVoQtN0m5-PucL5rKnCLGS_Q6Oau0LhNCf2AdWLCtGw7iNX8qmfKqeE5T6UEm9N5er_o18KpKy-WYOrBN2izSc5KMDPwRk908wMi3XuPqm5MOx-66fiHhAO9BPf659oZJywF2ko8_5Bw8nlqoLloAcEgtW5aOpryIfu2gBC5GluRsaCmt5Bwmxwm4LrYhBdtgQITORPex0S8jl6C5HKbUzTQH3ZThe3qRUG1ulY679NsZfvdB1XT5PU5RwHo_2raV94NOYSbLJPkW39kzbbs1DJ_-Af6jEVl_Q0yg8NAnQCSZedYjMQZc_89cMBuzPIfCNOb_NQo6J-3t3xqf4iUWzqf2SHD5XClEkqchdOjFh8NcJiyP59cVKZSLAWRdZmNqz3U0KO_PFxx0qKYESEMm4J64hHDO3SqFZaGW_2KtRolkisivp_4Phl9legHQQ_wW8vi6kLwUQlYICF4FGbQOfyo9s-v7almct0S2SoG_zJnB947m60MDyibLrKySG0lYwSCt2X8SI55T9BxDq6549nSuyBl2pamwJ72x02i_mVOdvvhvvmqYVhOBV4a3_njTKh2WLLzSR1gMVWcLSXiWqTuDFf1gpbcyPBStruAijphedL4k1OEBn4pPlAIu2wDUz38IxXlFa75TMzGrygme768Lz3PUaByMKPXxrfq1snZMw0Ww-URUyr2hn7_DGG2OEYSamIkRbWndG4ChHz3I5Ed4phO2cHDW2ITZ0qOpdRepArkDLDvpKJdg8Ys9Q4m5YVnf5E3KaD4RZ9dw9ockDT42ny5izluBekmgAOHhpee2SDx1mqLV00rHk9hRTeHXy9BwJF5J58140VoPshGXSu1C5i4E3rAA4FrM_ulpouwi18HlrYYgGeU25CuJmYBeolY2XCTgtOOreXAe13NU_oZC0B9fh-d_9M9Yq1_SJzO69JRwbiu7uJva1aCscBxYapHPITdfpb0Hgc2V0cnIVUVBlK69_kjZMRy0AHk2np8D-aymBEwe6p5qQ3JuQsGuYXsOn1kAJ67W55RWReZLJ3Fix8b4AjdUQkloeEnQbosiU4I7-zl9HLyKkZRYl2_XVvBgrxgMAI4WR5KoOzCnvBiaYFpYvyVHkrIusL3tZjhCv5GmzlEL0QioXxaK8gVpPfsfPyhrHAq4tKsD7bFG0YN-7eHKye2VORtjssHZ18s64xyrd8ZHEjLnvZkqxd35zRy1vetvolrpInJQUPWaX-9DD1X19-0huZ5lo3ldWAipJsWH9ckm0_hIIrJOf7WCcS0Nv4KiOjQIHKmqTvfKxr4DPdHL-flgL2bdfyYBSxeIthNR_cuWoJocgZwZIB4yVsfpnYzTgrAV15d84tO4DVKLafxTTtjd2yRBf5nCAYjEAefkFGZcUBlmYl_FJee2KJk18GKOkTY-0AdVDp_zlcA6Tc2HzvehwUF5Cfzy-XsIyD4ztNFoqEUc-H2cA4Ihraihqo9z1UwlponmzvnmLB22AQhpHmbbnneXXgI07YDqjO_rV5uUo66dIt3QqdoQ4rhSPf4wN7sNNtOzlGFPnbcU5dzwnyBsuS-wianLVBPy_o-iAS5Xn4sBre28vFslVN9ETOg_7et32LD-hgnICeuBuE1PzFrXWHCZOdSVri2OiGyQNIp8FeMEZky_m3H1LbPQHpbCn7QOGzp5SA_UaZchSp8gr4LKfSIyszsrpGjdtUeKoFqaqzM6N-1Da90YUtran0sgg6v0kfh7L2UuewT8XJc2p4Bcrsb5eTOuaWjQhAWpqld5JXzW9m_Bd1lW3rh1WpLAeBDzCzelP6I3FDbNpyoooWdarXqzsDPoE74j5uPlQoGv4HKPZFuxS7w9p_ChyRhLfunsujU5Tj2ka2WxpQb8v8lpTIFJzp5Wi0YblXMDxJSknAdUxSXGMiKrbUnLBG8-3jNgjTvzsbzR7B1MM1ibTbSmCMB1og-MWK7tebm8tMw6YRJx7O29huPcShzpDUVtppigqOH-_1P1JonXEPlBgC6Ugw8XV7VPqLsMkt7ksJLDqI67LSrjW-RkMR6jdhs5mflXCeYXaluRwRDv4uz2yGxXCYRkuQT9U-TKp_Dlca3EeVW8ZhjlLksuSZGiPa6_DUwHnPesC3PRJQrOdrmSS4J6FqrvICrsE5ldlhDaE__YtXuRwfENQB3rxehVJhVTwSBolL80s3fAeBvg5fugQdetkOi3Faf6Db2tR4ahDA5sBy68rqD-0_4fe4BJDNXg7IMscqMhgTtSg75k6_XvKXS6PVyw6QVj0yttSumCdrbp7tosYwbr_Vwepmsc5cMo3Jh4R9jwN6wJZ-rzS4QegOKQYIt7Bj-qpzBAHgSwcSyZ_Pe7RQqQczZi9YJRF5Q7Jv5Y62YT79Ayy5sTvst_K_brpXBFiC1gT25F-p84uuTFQ2J8zpjIdCwa_EV0lPTnhv_eKEtIR9W17_vegN51zBRTs7PmhbjIiTVrBD9FOJARWUds4le1_2xIlRnhLaDqn40j-b-ICFEZ8epjIsrlOz98ITPGecFzQZ7wMDsCXFPPZw6FD-IBeJnBnu9u9Wg13Lxui2Zex59089EhU3dOtnwxBnBT3mzPpVLl3AGDFkGVcxAj5Z7UJ0Tj7XR_EuyrsVTyD6Hwdiifv1gF-UwWg8--ufjvdwdG6MhGv0ysU2kOcMfLFFHPw6BChpoP7NvBQmBb35NmCWmlbcBlO56oq_zdsOsXNJjvNiXtQYWK2PkKNURiSKTAmSdZOwQxwuN_LbJHC3L7rzzqtlLP-jSdGWG5llEcLrPEFzc8qbF3SidqfxLKkPSUWDpgp_9BFNP4IDzJXlZEN9d0nWTyhZduPT1A6AJ38AE9aERJdmHL3R_9bcu4CXyfHa85eYy2qa7D-DKNglSEquIPcxA3WGMQtazimIUP8ybx_pj7OiUMKrDL4gb69t3220Y9TN1maB1pW3K1-1DUZ5WtYvjxOwJPixWyq0PJxIifg-nqJfvQxH-DPu8LMFeHtrUeDwgjQgFlyjyApqXl2KydT_zhF1IzWFiz3UAw3sABizRqNhsNPOfX9t1sqLYxjR3D3WdtxHaYLn40MkOs6Zx7r2sBvoi76XvM5PaiJ9HhM3Uz4zf0ziVDmvJ5fNwJMCM_4ddx_S3A90o5Y8aLmKEMoD06iGOccrNVilzxCWn_-dbB9ZG_P-AXTjulu7LpzvkfoH1YdDQkAp3hrx6ti4wGJlmu42FAFG9txo3pO9bQThnQ270Ha1Yhmx_Sgiqgn7N1FCXk0JStdd-lta2_qdvB0q5boAtO0nY9gVC6fE6zP8elnZe6yk5jGcig4wkPmaOV3GtsEOyOqzmKOMNM-Ui0pymXuT3G_CAQ1nC9OYFUPzTS7giH3Xft_ctS4Gviul_ak5J0IGN1mN5RoXryCKXHOOCdUg7GqkOchbCuywPWGE7PJzJJlsrp2EbSysp4ePD90NPLPQBHd6ZSXsm2r-aLAExlhFVCCqkB0c8drNT-jrqPXj4BegTK6tbPdCynCzkNvpr_TmboSVB21NjAkq2fk791fcmXtW1XgXia97-lsDgGNaDRF7UVaTEELpUErz7Xvzuy34DKLc5BxOW7LcGc9O9G06TJDSr334xZChUr6_58pJg6h4mqUwK_o9BRCVuvkpZH0XW0v8glklwT2sUZ18dFU8eEKgTUEA93-1n0dDWCGibCCwKNDbeMxyvnFczczPRNEp0w5P9H4YeqGIxrq-BqGDA4Sv9g0axrh-VlMSL_D_tZ7ECSzzFxCZy9awEWflD3d_2xxPnfJ9GhVL8Amxqz3oTbrZKFnAr0f28SShDgyVxC6tWvudfebjyrMvG0NBNCbqKpxttjsggz6Xg2LhSPDgQMVBijfM8cTOSmfsbG8Sy3CaU-Jcc7Sp1aRZyfdPdjCEgEz00MrdGJZJ6dF7JWr9AJagntuRRcCqdmAGytmncFd6CQs0JH_W32avQRAENs4BcY_E5EaeLsJtBI7V7yHPuFoyY3hbbwNME0cFCtktpngmA4yZiOJRcUEWhEhExNJtjpV9soctpujej0M--t_m3yRtk-0qjhmDauAnDhcfLZQ33ayXQ-ymoU5Zx9iCDbYIjOYggmUAACOMDvxCJRnoERP32kw34_-Mcvw3BEf20z9S0DCcka_DSFMffZFj8I7SQAzXHN8NIooO-VbcMzswt0WG4yAbVOCCriV-MYWF9CBFCx0zDFj-mI3J9RCu1jOgJxzvoMikgl6C8Ji-7E0WtPktLQwifLhRjaDytWGXO5b2A0s3tqy-NlzBTFvPjeXqBDx25KbM0hr7GrFqb1vUh6Xi0h9Z6HWDBWgb1k9bVy4wcOv0TyEmjrUQ2ZF0gjQQZ0Y3MY1HA4OsCGgsD4Vhwx-gKsqy_gNSnc2DDsq-uI5VC3KlH4kJb_nc-d-G2TTjzt46awMd1GDGMg8oipsB00eLoFUWFsQTnqHMP5jovSQHl8GImx5Di5SUmi8xrI5541XkwsstJklz-0_bnxfUoCobdyLm_gsH-fEFtnlRlGfbflcBl8b_Y8LtSmknyg9JJJzDwq-WTVUqoK7LdqT5eRLGCFo4qmaefoaFk20TsH7FXi3DxQxDSPrsTX33tGB4SCk-ABJ4jFDTDZ8QeAqy0hoVv0XG57iyQZjwTTRNHb4d8dLgexQHv_gw57AZbIubT3f8moUirAIbWOjH6W_mLTvB0OJpPR-cA3IjctdDtf4kfnJN1v2J0SVCjVqgRts5eENq7_cx3R_ai9jgS2HyU2PWk621Gyh_DM5cux0h2gE4HuZWIFmr0TGsRAZkp9hfN15C554SO_gspt692YAY3b_9lu3oOKN0o9o6WQBxge3Vca6bxlZ_-PmOou3vcKWCkZvdxb1uT2Smu-NZ7KqzxJUU6PSBY072lxgQzeX6xTbCMYYzO03akRQXVVtZah-r-C7Qw8TTr0GreIRXcLRCVTZfAlS4mYdNVw5bYSTKwjt087sEWQOJwVR2hcTmoPqIgvcEzVbbr9B3JWFDA0B1H9eqJnh8VhtlZAuj0TzHuq4U2bsbWzKcvxVdOduxNfJGP9gqD_UwQym3KtkXmnhPrs2X1qOqdNAyqwuHP3G2AcCpNB5fnjZcNnfA94x1xSO0DaViXbUYf6TiHKwPh0IIQFMwBicCdrb4VWcRNeDjBBXq971zZL_5f4Me4YAcnX5AEe_xtXioJe19J6kYZveOasN2WtO5K2s6DB2g_GLSzWain6nNZhTHXTFRggDfbyzLFE3OExIFiT7oe58FuI7MEP66xec6HsuXxlqwIhcZUpAPMFbyJ4DhSvX-0NDue-DatEU7nloxRovHfO3cCuwXfJH2EUt6B193yFFwikyZKD1luDNvCRyhd4tZopZ4C2DaMJernyxR-sjbVJUnstWOGbXwLG1d9K_UJkDGCki1RoZv_fNb-5-slsJNmG9Sfdzvyvr_ravgVjZNs_Pso6nulnC2XEXnDPzpHDn6R49nmuhjGqMxWT34wkKD7rsiv-nD6lKW1GoI-pLWCA-T0rw7Q9djbk1rYWgUS8ct-XzcUsTA-XGhCrSkQK2kZO1OD_-YidGzv2d3W4EleCc1zQSXORPzh6KtJpwx-ba3MRD-G_Yha27--b7OAwnVcN7uCwyGWqJFP8ly6DwS6Wc8dYG4oZOgn7MQnXQqWc2mQsppvvJjIXWgCFA8cEQ6ErZvaIxEIYUcXd9IobINBdW8vmOQGETUOjSy55gT_IOtImyG-HR8dL9CyN8pQ2MdZ1RmxJboUtCbO0pv99pPiG23Pd5fPKfzlLOin7SP_dFsOvyuGdjVL25NYjlXO7ckPgtFQxyMqYJu4NRSeL9O-0EYPhwSr-OsXeNLRixdTbbM4Fgl-V2ImHXYgueDepFfqi-nUoeziCTkRDuznAuOBwgR8pr3LdCmo3Jp5l1_NtiPIbDxdEWRMy5PTnVoJDxFNGYt_yY_Kk8Z5hlNS8E3IP1TdmfnxEsLK4eMYJE2kCwFlOyOk0hMnyII1uS-i1zRESK8akLNg9Gkixp_NjCzv6gbsEBjSU4wEWuzN7uY3bNosqPIJnHpylLV-iLJ2ddNZxG7VqpdrufCHI0qsYcWAzR1WQ5ZRLuTp9xWYXdvJbbgbO86dMMnS5pQtabZYM64k1pZbfAsiMl5w4RqqR6PMGQYMtTH8l98UICOmsA3dbJ54QEgDE5StJ6Bz7B0i0c6rN4F3ebDwIvREr6gFo0t7zSmmXxPcbZrZ-R8lDf4Ljbqzyd-o6rxac7G73J22E84uNle3R2lrEQJrwCLUFegSmUdTNm6Tg-zUyXSWikByqCgDnNbC-HFdH5jD08nVSVWeqAJeSdtAY9OlixPRGBtUuMRpfgxrRd32oi7RwYockIKGLxRDZiUivMMM0kkCUOre3GOg5ABm-tj3wwhWatOtOYtJatmOeUF-l-C5S-9xp1VFgdt9UJbyS2CJb9P0hbkmY4ieunzCAy0vKfPGcRUUmbJ5Idcskxt-zn8YElwQ3xyPgWfRMhNMRLq9-vT6SZJAfzEm8QtO5jgqCJvwQOxcQxhFFPcSr3vLJuis_i6CL8XuGMEjV8eERpLMufrms0o-gk2EEAuw6OBKLReGwthmtE2XZ8V-DIaEA6cKMVXIWPuS_I8OCVrCWxU88L1a4j3AxQV1drmxatpYJ66zIfog0z6_eQ_6NN-BNAlGxanHqgzAaMJOX0_IdPLC2G9uyUXJwAXewHjW-XqLjzHM3Hlfkp7rkPxzMVyRgaxN70-cmZBplRQiiUPd2KIWfMM6FiTvS-JeXBnsp3-4NebgIBVly2c3awViltUo6bJ_gsTvYOzv8bEbujZWWp5WbNzoyrNe3m0gHzmMZi0chr43n2-6KYEe1DwdvWbKBZFTtzjiLwPTsJJmeZB5SNVcgHpRvo12VEHkQbrs9QrHR2yhnRyZTaUtyUtDO_us9kkFJsn2y-oldyTdfvDwVGWgUDNaV1RybR8aa69wKfgLYHqRQGJVZydBgj9KywFPQBd868Ydp869AeUORSlnWSQmOTcaat8Ovsg0a0As2wIclezgV_lPwJa1lIjqx6MU83xDKA1QgIU_qQFtM1nj0VRCRgasSSt_lgqfpFTe7viH-vdlaDd6Xiusco2Leb-KdlTsY6Ra9SO2TODeo3TDgk0BB2IkrMbkx83lk-jaLCFencGpCsLStp1ut7ZOCpldcSlDC3Syxkav0icT3KA4KHOG6hHZmwlTbmax9xLISbRA2JT9abgv3h2URPmKT6ktSwj7FyE774ja72OdYKIjPDrWbYDSKxho9N1FygGKS8WM5rm96YTvHEg7RuPspDpmfSaUnmjXF7u4LFaqG0136kvgUA6JCUqnHpetDm8CPL7PErw3aPyBsTziq-N5BVmlAaygPt1BW1-a5d3lZVu-V7qrLEkF70B7vvDrRSpfO2m7WBbufk9mQ3dU81puaVr-s0tuEB0AMF5o6cX6H-6QikgtS08C28pDvHVgPcoLdOcN2zKQnr0qB8rXtaugetksxetwZf5ZiXFEDl7Niy3MWyGjyK5UpDp-DcOJY4tkOd7f3589FE7nNDcNwOzD790RwGvv0ryYXPHFCmxsKah60KjzrFvIzDzCz4_uxOL7l6_y7zwV6noEJBa5IbHZvgC_NeFolGG_5-7L4dRrlzLVN41dyWCnyRV505BpR8z6eZwbGbMaPN-APuJuBTtOioJ7xhnRRRZh_5fcs0JNGkaf1hQu0s3P7bk6WkqfHkdMwv4cuD7qv5Tu-EucZvYIPi-5MPKfqsUShmFvAwSLCQAVF0h89uo7j1zHi4h8m9fPoaz852UoFYrAtK2GgQ9tTVGiie2ax5VTwhsd_KKz1IH2a5DAyQv6Tg1NEo3S7egHDazLsxBc0rT4MM4iw5X2q81WKTJMYaP0O6mG-em9c5s2ojMxpnWy6OJPGOYSpn2zNP8Dvx8NoqFyb41FHz3az4jF_v8IJgamndV9jnNSxJToaYuYTZ9ud-La2Z8kNcctfvu37pn1ihJ55HkW2jrHF4gbVg-FOt9InZbs7zB8z-J3Au0M741hgIXZwxYjAdPYEMIUpg4-a2Nk1vuBVbBpZhjiIrEdpRIf06RvdnD7PGrTiidpZw2snBdfNGvrFY_wihnCo7o4ZiN4QBKXOj1sWLEK-P1V01RHxDfqIzeixsDdTkVJbSA2DizizT1waSiu0JUcw14x5tcXJpaQKQ2FDu7MhMsGXCP1rJlSSTkspow_BxJ4lPSRmDfJLaQleS1wsDKuF4mDZ2lBCUwu8zAQabyt-qKW6mi01BoLG7bdgth2kjrDMq3ywzpRK5G3y9bltddNTmGEwNb1ejDgxFqdf6keB4FLGMU7rYwb9D8nAB0P3NACk9V66XkuMHkCY-TvjAk6zJP7IR1c2wyxio3djNINEg8QsSAKzPz9jYzYCyTSXw2pgsPh8bEg-ZzWEqa5NGKFNmDeWmRxHYRFJiPQ-TTgKBpa8VmBe3EO3LIq09jPhkwxRKVIhRwK-xPSxJc8i7Ka70R7xsVXGBCgC-A9AnAYEwBdyoNH1tkEvrDsYXBmF5AruIYhQG95jd3YhqWAYoK0lgi-NVusTlhvaYH24ZLcTrlJoHz0hxrc2OeAGMGhZrwlqvq3X4M2-o8Qy5OXP43rYLJ0cVbe1_b6SpbBnX2l-tjbvOvdI7AD-jS1cO5VIJ0OI2AflKoy4heDvdWnHRnrUxPK_8Ltv7Hr3SXOaYT0dFEQwe6fejMYdD8vjyrPRSNwdzVCGgD21U1_nUQakTNp_sKUdxl3T5jHdg1n-ntqxoH7duO9FHvRymQSPPiLM4jnm4KYUhR4baIIStyyD13h9IcWxNKO1IFqkP16jOsX7bHpQs676VdCe0r0MscKnVlKPNXYtmpQVnT_s1l7a8M9xv1SOoIUnjvvnAAHZ0WMrGV-eQgUTpZkG947G67-gmx7I76sshYPcWdYYtJCFtzH4eu6MJHPxYbThA4nJwfrCRfC9P9YJh5Wy8N7FXFPvrZJwUWuA05EEhSTpCbXj60TTA4HJaOaNHpoYVJuihOI5MOr-TJjCQq61URLJZ-biJuxJXAw_FK19kSLztqWwbdFh3bA4FT2bBHojctRWUwIe-zVq8QtA-oSlG1GrECoXjd8Gehnrzmd-yNMNYltfXbk8Cpjc4LDG-CFHZ6we7_GxYc-jHuVFTBuSpb-DyXmR6SlnEOmxymW3f2YyZu0FSSfwAKJTZ97SHJlYTkOTTqoBLnkc6wA3OVeKB5yhzR-3w8w7S6E7Txqb_1QPLL0HeaYziIpwIvVXhOHgI7tPzoSaH2doqKSIqKW5XCq-Ef-HmbCRSoBfiGzSmQRQyTGIr5afSIcHona3dkHa6rN4tl_ZQrfWxjOG9gKFJ1ZhoPshEbrxnkVXb88mfEWrf0haTjPf9IjFpJDh5m6g9wsOITkWskSMnKDQCjyV6dwHXI-iPf-nUWieL8ho2W8pH5LMCAyJ9KtR7zQ89qisT4bfnDFCl4CODz8hK-i_TgKi5ir0rvDEKGHuizOogTYfxCW7MCzQ-mZy1gdZ69UYSwTqiPYLIHDZEwl4VuHVS8yWXlx88ez0NcdmvdeMX7rY8fo4GJkRDug0p0nK8u4RBYKMkwFOKDnbOhunJ2MNUqP2A3UQW8vGEAHv8Mi7KtXkwroBikF3CyRFxC5AFV7xHSzvKK4WHJgvfnFqnNpHWxfSZYrmOwH3xCZF2tBj2mmKLDxGYOFyGpjUtCZ_m_zK4NUlCoeRqPMVg4yWJDTNks2FH20qbccQHCd-dzBrDKNZd8ogPpGxIASh-ew7p4cnW-2Ag5xb9sh0iAXolRltBKLsM-z4hU3AbqPSZIxv3QMnyhoKRKrx4ACMxHwpN6oQ21JkOsXfbFTnuJa7vQAHAlV5sQJqqMV5NooKa5MDt3nrl_v5Yp1evR6gGcVwQTlW3blz5A6Nk_m1f3lS1GTYQWeDxFT4gF32Iun14zmvG5EXWjkHxj8m81Y9LZcVS5Dgi3ErHKDLrUL6iDICFlDtFwDdDLQ7GH_CREg8q6nCm0fTtHz7vSKhMY6Fw9XSoeX-Hzq6TG4UAlJkRAOZVl3jVqocy9jKa2_CdTZKzptLTnqVCdzauG9FQUhTorukJG1ChppFK4ZMTqM1GzbA3djupJ7VV1KbkGCSOUKfXmR3RIpjLgcnd-IMrdGXGlYvvD3Wyf-KkD1xtCK-Y52I25JIzToTr0ab-uB-oDjhURFwaSV2344sOFdWfzk86c53MxCsBRoJXFPh3p16SUZCdkKElaS5lpCnXGXuws7_kvBS0vkLF5O6D8pkA3b0GfOgs312-mUYgU-kPewJKeSqm6mTXmxz7xdoncKyUspScwT_dqthdYfvWjoSV6spJD5B9erqWEUJBbcR3hnzhoI-xQ2dGY32Zkh9HwOaK4_JFfJVi-mxZzdCMbFxoeYwio97YrenhcJAUTDWUape1lMCQyT1lgNqvO7HoK41TF_ZmVB-qpbnNPEcOFZf3VQeXk46K3vKx71io4ASruhlYBddkUBhPWgVao-vtaZpScTebu-cFW2XjzYf2_6_SvWs7-hrBCH-N-9YbBPuQRYt9KZCW4Upo4ANVTZ0l93E4p3u0xn6yOUeiZ3rSwk4VRvvkIpGN_qLyd1S4ga5p1ky2Dw0VPGAfqQpsMv_ezi_foBVW0vG0ZhGu_2mbbF3LxaPzCrmahWylRBFTlPg2NMxQcBwg9IVG9lcDcUHTH9SO89HLRDEuI3rbVPMsmYtc_2eS4cH6yh-HuvP2_VfnwCUl5vug3qviWRaqq6p6NQLufKzJ1SR2w1kXK7TH44neHFqxtPS9euzjJBKXUwmb8e_8PjXvTgaPShyUlvNRgXpq5SPEqUKpoBcP5eCAjUd2cpEbR-osyi4S9cvVfnTTx7LeYIj5YDkiq1yT9y55xTxUCNkYB1gWznbcchMzSgFyL7QNpA1mq5nHVSIkJz6xJmvMDozxNzzQ_VBiC52NwZ6ZQkx0uRqHO3DuhsPLB7I0CqEX3AFSq4KVwOvreyq8mwyErjaj-7AydcqwAuIVrnhvoxTjW1iMDFFvUj1V8CLisG9s9w-y2K_WPspwkv6uNnkyKhV2KRvZWBJTXKjObVlzVd7tmyrZgj-KLFAIqwpzd1Z3GwwaXnV7D8JIkDPxYUffJyP2vAP6h2fbWhq03KO_0DUnQMromcVkV1E67bEdM6rNcKAOhT-d40CjZVtfUbKfGgGE0q2UDd5k0_zBHm4UoJG5SVrOki-HPuZvfGOiaE0edGyb4zbmmn34dwLSQA4AWq4QT-Afq32oDlrftMo79yAfp3mf5g-7maWpFYHhjRIhJHY15NlNZcaIKvLLhOIr-wUTqnKGbOAPgvamqVLxwFWBBJz_WANETzZ9QqKuVmrI6mA6HcOUF02WUCwfb-jK5tjRZ2TBsik6-0kCHQbRdhy9ZC3L35NhD__JP9KsbOvKkcgMg-wrByhWscKkGh9YyNmQ4WIYDyYCPtPBjqQ-SP_cnL8R1Se36DYy40sPRhHfvKC3iF1zWT67ERD06E9S6xb9vWaY-og2h8UUFM7GD3g57ycDjYCknw_GCZK7D0yYQkrW2a2AEdngQ2uepmbFRxTCP0_kn2yg4Q04c9ipnnknBKUQeoqmOIan-ZL9utG1MJ1OqbLiXXBQbpLcYypOcnWWzoSi8XVe9-aYFh1-4wSNNZJZx1d0r9PUyUiWlnxz42NoLwaUUhZwL5KjUtyMeZBM9GAg3uIPyJhsWfKPnIcLFbhPZ9UN4eZbOCZo8KGXOJd8XylbWGN21x21cq-6eBBfxBdYVm7re-nZO9GI_AsgoFooGQzh7NxLdHP6S3N0j45jIsAd-nT8xIKQddOCTItavZETZa1qKqlXTksWlj19HjS7jaWfXPffcBNQ_G0t-JtlBuAODb-7ThK1w40gxvCftaBlz7P9CJBm77NaPitL51WjiGWLcREQS0JrUf8ekzsTKhmPc299vQLcxuyue8sfAnVhE-RboTy9QoTMM6B0R8n0Bg-TPPc4JEUjQwia9jIYc-OupsmRnyGg0EwaCXt7tvz7E4m3dw3HBStb9GrA_aORHsruZP2qfSPDUZeEv4s8OiqF3XrN89W_KGkO3-AmG83NuG-UtjafmdhRSqm6xkVuMJ_s1Jlq26-a7iNkuVYIf7g2UbbeXcKeAkADWdtbMGWxygP3kVg1kcNJTaW3k3P0BkcPfyH2ezH-SugO_xqGmMkEp_bk1RYi7UGwoUQPEqs0E8GsPayXKg7mE5NgBPGh1WZuPBJ8muUUmI8XnxN5QvPIy8jGe4tSTTcmBLz3w5YzKU3LwKFAwx8za_rSn14nZFjEsv5h28m_pjsT1Tcr8SeBjh8J43MR8CNveQzACaO5mwyaT6BRgf3XlINVgTY7e1RQD3UY41-t37G2MmG91V-tJQ_cHHAzkwTMHgSxVmkoqbMxP1OvrwkxoTSJArcpOhCKLIA3n-En_ZfIQb-F88iMu32qxPAHSRmr56jYgcrfpPutw9J-S-kbm2oSRZBC9GW85GlszkthZqTRykuqSnm_qi0mc6IMINn7fXl6Bqcfg96d9rKrDXUkgBgi24qHvTV_--rmtyvjolPNQBiHxPAWJbLZADG8pQF43_pEDWQ-F4sIY92TI6KLgBg3ywuDMKE6nReT5NzVS2VT2ctqYzp5xLcphl9Xc5EiEnK87kwkPLiGuDFc7R9IFXGUQg5BqDV4RY6uxVT4FrvCtZVUPNIhNuzNpC_K_iBewQri3hH_zGzNKfFrQEkhtXl5jh1QjNmlmFPCBTRrt7nB2IGattiZLuabcrKMJwYnrzppTrp2Hpr-SJynMr57B5QvpqOwR-UoUYb0FvbzooFemP3jTlUwClvz_dbEZQfF8uzqcN4INcdiOYDu3YvCzZQ4VJ1k7yTWFTNXaF9bTZtB98fGyvmHktG6qsKQzyefJ9xD4lGxT8T3S-DcmYJkJqsoM6odKlck9HQI4HBCzi03KPWNE.XxFVuuUbBEbweClHxRftgg
    """.split())
FOREIGN_ATTESTATION = ''.join(
    """
    eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..JTIVmsoEHDc-Ud7m.ZgA6FU50f0rQvB3fqkjB6htWLJj1vzkqqduyF4BVocBfl9-OT3YLDrNIzoXt26ugv-W-B86piUmlDL70oQc5KgEZ5_7Tt9M1MtcFyAMEjYXQt4uh8dGo-mpR6gAaTTvluijUZV5iVgbtSMeOSK8M44SbMICV2Lc7_hgifBzZ3Fv_XqSsPQvENRQbON8GGYTE2-hpIoSzIEuYuOE3xrjtR6N6ohhAs3E1LRUk-KzpZUG2BNDo68DzoNrIZpXQzoJqEORefxadQCHJTNrPwjVaoWJ1Ex1p6aGRXhJTHuVSdDhpnMMEx5EVHtyKN7mIwzyCOYOMBtmLe8qwt3L3f44dN5xDO5bDhVlQVFi0MtVpGOVKr5OEeu6X5P2REhRuVzozr6oIebvAuOpwTPGA8zc7vWQt77Z0lJYnx0Utag4VKEhlL6laPwnzrtqazKNNBuVEdk0m0AMZZstyKGk059ysRNZ2zqV0aS2fUgMJZiyemo7u5sc7Fjf7cPkuMIBwcsq01EYf3qL-_PqivL06zLi5GAHU5fJJmDmYlLKUXFrSKOLhWQNQ3zuS9CMvVojq-_pL5ttIqUXmBjP6V28eGo5mKgImNtzRUxvaJKrrlxx1XLkN0zqAhQIKLaBXwVlwuaUscMaDm_wlEmUgEYfeyZ6ytfKUOO9n8zKqdsFFWgJzUsKj85ZSUQgw7imIy1ZwNbF38fa90NVKCGhlbDngsqHufUj_45eygRBjIc4P8d8KO07iefed4P-mKfzBHJH9Fk9nk0-ZsD8QuX2cysso7OUkjFV4kM_PFTg0INqAA14pbr_ySVptzS20OA_2F3mmq18uwiaJ7t_v0is-AZ4ccBo-5lqPzP1w4yT9CbwqK8b7xvqhnk9LPd0EEnstcqrLxjtbcKce6B3ctPNDTyjikiFf5IWOxCRvR9x2gY3g7QYpefYuEOSrWjEvUItR8169nfm5KK1wW6LdnL6vFxGVPW-CdiCR5tGSWA-Bbuy93oFzOJOsynJu0CB5Sp9HyXBAlkZswZ3PQGdtgujFu6cAr_Kloj_1oG72EF1iwbaG_9GTRoZyTb0F5UVZF9F3BO8Og8lmOH3ehsogtSUZNYwrjlAVnW7OTsj5sydlgwgT7_Oq7PuEsewre1g9w1PasHjQPWInjYV7DVpFor__BRSnlJQw5PmvtyaD04n4tFfjqn0YcDsaTKmQ67vHTfz77tqoXBSUgI8j9NlcdU_GAirQlhAYycv4UDhMTXSSenN2vU5LqmVCDEgDHaenX8Inl9lVtYtHiizucvt0EJOwVOXR0fvhqCBTktT2mxys5KTNG2NpHFWsLjt4zVUWwn9BXdj7oHaTCnNsasVwC_b8-QCuin03T2Z2dqaAIUGsQDEh_YRPgHX0VK9FROvXdcEjd5hn-_So8gFEEY1_o_SPAVvOiTlpN4mtebUigHLC8KnTLi_3LNucG6byc41TGHI57hDFA-_odQTWwSNgj9Y6GXnkw_hVpP2XcNtPaEGWD-bVPjf3eUjamGsVfrM5YwYN1oxlE0BRwPgJ_NgBOpmVF__7s1NXZErKLodCSU9D5dQLi5UCbX3Yr7KBgsigcGxIO-lat7JZl14kyOVrQvfGf75DWo09ZSXrNEU1JFKTyX-OW9pCAKkAqtAPmq34RpFce_b7bUhymG8LSlxcFEiNUiqog--SvkSjM9YoEfkLvcJpmRky7moXQZeWBiIWKu7B6U3mI8lLDF9DCj7NRzJWPWYCs-nEpjU7qYf3_ivIb5m3Wzc4sFwTkY4RWB6FQy1GuVFHpK3HnnnVW_RnTAab7z6KQn2VcIw4PfUIRiNpP9cgALT2Fb_CleaaLQsHdnXhxIT4bugbRc0zxS9luxGL_4ji579EK69q756RO0RUA04FyAutKxUUlUPul7jWM1F-8Xan6hpP9jo1ko6pjzUu3CKdY9gYZoBBfBvMwxStNT2RuKzlK46JOwvJ30kJyrlNqoZZgDC34Ki5IAPf7WQjAcUMIJJOk5KOjEHTkHvlV3S6Pw7vSTEs5FMlIvEL3FiM3wylI8JL0PyJvyvXN1FwoQ4fwzC4ceL-v4vYw9M6aWnlIGUGhId4_eOAZvZ6JVD00E2HhTSpG9hEbPZHsQDJZRjZyAQRqIDMxjW8rQckk7y46UHP0vrZy3xK6CtryO7u4lLuSsXN9xCN72RvqcwvbLtrwommYjUAQxFfTLw7xkb0POMeMz_9OLQMmCnyYL6TL9kh_eJFdPRfQR42LPxJjgUIrn5ZxvuCOp5EkyPYyDwELCiGFNjm3vompc7z3GCDqBJfh3zgFugZRjzLn3oGdUAp9Wj6M1pRnGnqSatEtnU2GUur86ysIjI09UtPdM6pNpYD0dAOgGUeTZ9TTJpFHau0wfy0C2IeYJ5LfOXZGiRodAfB8fjoZPBcYdl1dtLAhJ_8o86izYrYrpQVmDrs_v2zwsnNNKVSrFs1HV_X7d-oVomsuhM-FyDqhgsfHV3rFfAOQjqs-Mt65D5JR5AI_rwuUj6NtiXyM8wsjUP_MFoe_p-KdfzGleClaYFWirS268K-BdQLF1HsVViMlV-KikrW_tNqGzB3vFdMQhxS_Zb8YIGJxmWXVVM9bf_EyCTAZ17WfqPci3PUEHyYHCq5VZw0v7XRleU-vGnXcnxcGiHCemOjf-qqjEiz5PjOxiHDluS4SQWtsmLj_J1t_72PiJ0xZlb8RllKadsuGxbzZ98bVg3Rc8ArJgsJjOiwdnpCVPF_0BzVv8mgnfPiY-FurbIcVegC8fhzBUolF7sS2UNgz5om37QXE-XVboZnWpzS9A9J-MmEvuc9VpY0N4hUuC8nFL59VF5O4oWNDbC7INDD-QGL_ZzpSARunbSHIq24DXn9_naroI2W2V5lhltHddXnDuEkrttI8FISUTBIc6-VsVacCxuLiQSq3ziAzu7aJiRdiTTLZ7niJF_mEfw5ZYsKXm4edMvDMP9dl7uS7DNMRLI133LrEfRONZms8_0Y7jieLnMvDpRSYeB5ZpmZKojYOXVFYOjoH4SxZnypv8f3gQeGce_o2zHPkAN47YFRcqPhmpg32dU6XlCSYoQJVLonOiMzcogMF9Xj0gJEKnmtlkcDXruM68yFr7fCZsSt8pR1anXiINp3ZCQ9X6APBDYa4ZdY7SDRGjlW8_1XiDTJvhsN2cDFv3VDPl0G44KIYjo8rw0COld2h_328XJ9Jbdpbr8NrvI1TGrAXR0Vv1vnzMuYwsaJya4sP2x7WvUshtKYP-A5hNbTOH83cSROMtLX-f5P4obMr7PqLh_gJZwdWut7OFnYY9uCe_W3ZaN9EVsUcqHSYHwUDFNp7PwkhEdQc5BEOFMFkk_1DdDqJlCuLih4sTv7KF-paR2w6mTSMi-NMDGiQI3estRjYaKf20TlPAFklYqDSR7UNBh5t7j2VpgMy5q1Dx-exEeb50Ojzv6uNLAtIqJqJ1nDm_9Lw1JmjLdhymtbJrclOS5zxcF1JhVJkLSG_dJhtnt_OOF57c0kD_miOXWYijZuThMmWG8_Y4LPowVwshUEBJVyj4TpDe799wmrysRwnVTMJqC76HD_yWQZ6uknqRX5ynt66yBM32mSv9lUpVNwCeRg3NuaDKU33hcs_dEVlLZZFbeX5aTUuNRmXQEB3Odnb_X_iXCbEULbhc9Un7SVAT7u3wodvjesznEAjvUucz5f6grK2ICUAcX9iAGBtCmbMVhxv1GaMcTq_VofujKsUK-4IO7gxhz9_tYpQU7pZJltZxmqPCtVCyeKYhrOS_TY6GnMVwqeJkonEbaEnlALNfzoIasGYjTSR1z_L3JtR8AiKfUDoafceEDlhAE9qhroC5ct08JzD49gnpZterVCNQMlZKWCKUCRmFWxD3Cn6LUQ1doXWd7kGjpRe3UjSeORudOPNJnLv_ct_dZH32vr_Sp1YD3Qc6QzIpXRWqH6OADTq-uToCLj0NMcr_tTnxCujaQMgmcSo9u_x2jrbKSmTC5A_YiFL6NIpBV1dcxgfcWZxbrsJWFvnVt_c_ZRn2LlYvnSF4hBhbmYPr3V8Fqr1CkyeRm7AncOwRcz_iOppHQNgZ1hreOQcGIdkL2VSohpcjnruAsPlM_qaOx82FUCajjEMnZJKBiMXbMt1DnqBEwpCMWttrgehK5tswM00x7NALS4cgyXizg9F2E-BaN9dhuQQWB0mBFV4g9nvM_UsY7I-GEx6HJpUl0IIdZuy3Lmev-fw4ZwDOTN6pSWI6r6_vaJ4lQIX2Fp9ttB-MYypihPbMsdSokvV4vSuVAWp2H8b7qN2m_ehtlF9wBuV_5Iea81Qx1Cg5I06eygKRC2qnmyjwW7n7S4qL086T-Yeuh2Le4z0OKn7ks9DbywENlQYXT6QDYmupBT38dRwG9praSEMdy8QabvlSrBSP4ySEuPmdpGO8hbNqtE8YJRPRoOeSXgZzPVqCX1WS36J8N2yf6XGLtKCnJ1g6F1dJuNQkXq2vb7KeGNl-4RSjjP0j0U1t6gtSwNFHYaYq-7K-PjA78ku7gVzlfPG9SAcuokOCb2peMjftDdjWltMwGuLZjy9Fafs9Qhaij7sxJnxRcsKw3vXxfIz5Gv8qtgideIdAXioL2qlu6fmW0d1RcF-sTdm5iNWv3d-VI5zHZTzOlr4QqqjXsgZ6N4c_YGLrAZdqFLTuAbD6ajdExaaJIIwTcE9f2cDAzcPszCJVGyeDShbAYLHXWxCprhb7CD2ETziaYNgtSKakkg9Bce5KqZJtI_MB8InrBi3180RUaSrfw52iSvIWv6NAyEZfPDiNjaNbCgJeDV3ZWnr5cVf0WZ6AWt_qWwLtdQuAIPv6sMkqqXspZN_kr0gmOfYJ95vdCO6gwcaz2tt0SSFq4Tik4QmAUAFIFFwk16BK0QePb8JcNu4CrBpZ3LKPrO5OUgWBHM1XQc6uTc66Qp3ryTaD4DoOj5Vb1FQpA2I0QEDBvJMqTyU_96URGQjEZIGArqkyduYwWULlvzLsI0tXwKECo_qWD8_Kmv2bM_yCCsqPAg7EGjeagAd5Q63p-DTlE3QeIIEmwrnH6jxTIx1IxqxiKv6qO5nQz-FCWyInLdXA7zCl--26sKM1vlbKmxYHG3raq0WYHbXaNSi0Pq1IVK40GOK-qAiH4WyDdYoAor5y8ArJ7bdIR4lFzJ7533ph-4kPROA4XCLntDWAN_JgmEgQ5qNWtxj5xJdzNFoYxYTSoz1oHgJ4UWD8137kTZRMyZJ9B4OJhdNhjfX3TnFdTrgkaRAKKv3C4sezRTM-TS-MZa5oZO2BOa31KdhIF-vYPI6nhWwr7FKFRXN8bpdKeWG3i9bbqTV0ouQOq0sr-jLxr6_mRgN5HBCeqfCFOjUE41G9nyEJrS8zLR1wMuMdXZWPFDtV6GDFdqp6St9_BUlk0vnYC4AM4e-gQRPYvI_ELrQfKrWDYwV6pUgR08z2h8RF4j28YBWEj_i76kE4T_rktktn-Swlem6vNSFTPEVWhJiir0x2gyv8xfG-CCRTZ2CXD9r3mBr7ZgseXcRA-gFqseEjbzPGva8zL9ZdqGQQUVya6uMkytBwdsPb6FskyfF8yp69BP2DudqG_Iue11JXKqWbcHv4fDJ0K_xnahGYriz83LVHOSm78Ug9Mntdju9KotOzuuFph-POKkzyt8Bprjhh16IQR4fRevQOVJSOWWD7pjcta_ew4k5elj-Cq8ZQcqDYqyKxNGaeZ0gLsxoBZrmXA_zvD7VcZ-FSe-a7da_DQjtkHX2SMwueS2JsB2UHBzcO3iLDyp23kxx5ZuusYvjRbDIF-7K_lbxPNwp0oXiE0XK5qngzZse9YkFxhsQwVHTBAyYgl5Fc3IlnliNIqV-9V5Ygg9jkb6brAyWrD195yLNZ0f6_YlLiw0rdTYq-pN17vxshTUxK2j5rPCl-a9FB0NMLHJ0eg-6Jbu8brmIN27mR34VQYxfM1zTl_2NLrO17EXna7Upr1PkiOHVnHwk6ty7N69XAszWhnGyRFDHFxrCkzvYz7pEvIFGu-3H7Lp8n8iMo1BpLyZdRVWyqlvJ7EWK1R5Sl3IGqkSwW0GNuPz3nAutGQtFgJtOJ5kcYmdFJxffP2dfxm23TPwI7TYKWiogW8whCkKIYw3gMHeeCLtpdkAX0nMwg37dJufX5m7OTLlL2vWwUMyp-ms4G5_AMTx5LhNHqdAnXtB_paYurcScsUtwkSr1_cUS6V5WTBwPlz1wSoVL_MYmdhJ8fJngY9M-3r2t5Nd8DTpCkh_quOoTg7DUlqjwah-lPsL4VL7NihOARkA-XpDqAQTIM_sRulpYmYeQvOSwhbbXpMNq_XCuHO3nj6yRHlDu_0JYfa_cUTQRVWTIGxVPBoaMnYYTv7e7MuhL25nHMwuzL3tM_MSgX1w4bz22PfLBhgKPqss-oXttfrAmDmrMiArPfHEECu1GP9E8fTIHv0LNZqNCXDVYlabvxPyWsmLju-QkkBFQaYl9kkfYhEQGVyHsL8I73h_OWQ3HRwyNonlVtMWuASsQWcnnHvlPwKxJKoCRfFYJ3tfNvvkNpwdY6GydNzJI5GUrVMZFZ4JjvBs9mbYhnJ3XyOoGd3VrxYvqOqZb4X4rbC4NsZOYnzY_T3lbsCd8ISlfu-fl09K9FxG7VTwgguQPkIr8Yzp3H7V7bjEePdlHBYoM8w13FP3h4Tg2w4wUIi22yb5g55MI-LtPuTy0bBYuKf_MR7JzLlTKMrtvF4RHb2NgqA_UiZs0TUm5GN27o5zt6iB_b5rkJoYZ9zd6Klwd7N4N5sXGmyk6kdNorrfawiBB8GsBYgRXKu30-jHsLnGYeBLzUlodFLr2h7pYmFlRZuhzsEZEHm3zV3YwM95aABHsWQ3S9zO3_CMmmjDeJy3EhgXVHrw6EobYSFbPQO2hpmdLKK5uD1HcLaQIU0rzs_vg2uWF_lm4VmaT4fEpffcjNkPJA_zfDS7ZWmeMsbI4vMCJMIgOJlmpMWuD2-v5RUUHUyPJFxzD1sQfuchakh_6pHF3soskNRTmwJ8xo7nF4o3GuuurN9PxHUKDKchcjiIXDg1EBVPMujOheYE5THryB46iuoVZv6DuA59P239-GlAJ4iyXhFfmDPlh1CchnOmPUw2PGKFAUKiisYKkYxE2kHwY1DP6zYn_cK6kObdbP3es9YMmF_745b2dvHNOvG2p7OO9Tf-NNOGhwOTlss8yRqjNF-pikICPvbxuX2G7SMI9PUnRjO5UEpcTXeXNVV021qAXPBYCw2pXQlHWx4EztJEoklkA07a-6-a5lY-oWHvwBkQS60R5AaipengXqol7SIgC0sPnVKMOq3jmndJ_RhpXX5YH-7qdXTFLgo-FhzV0sqM49zFciJ4UPKOBdRrtn2bIepoBcAKel3Tf_DpDBIh3BjLUzOs5OhFIIsitkj3AziVTv8t49dviytjPzv8IL5lPXAxcjRjCXlrtJJnTao0Fr_fXxi31JaPcfY5gh9xTb915FTWe03G6cZxeNO9wuicI7OySsfkzVhurND0fE4z-zel8f6-uSyxUD6mZQbR7pOZiwkyVdaEX0QZNL9nybhobwchgoUxTKXmaD6behh8JuGy7Wn-gkhtidGKx4ViKY2PdunsfWTotvib6X6xgNIiqTuAXe39_v9ve4I2rYOAemBYlTFux-l57P9fSayeb415tBlP6M_tNSpBXU-U91qgWAwXC7ldEY6H1b3BCcTPIY57UTJmr9Y1EHzaeX63S-ZMU2j4nvux0H4JZJENC4fTJfgqyo28VNYX2xzpu77rr9zvNaiqsD9F8OAGKWu4ljuzIwru7J892mlpiK0PxPbczoqsTnlFT7Pj8L1yG45_p9hm16dvQvq8YadGKf2fesxqHYfpVBINggQOsEjisMPVDrePwPxAWsPtXeTvow58HvjSq9obaNP0YuMod7xQ0rPHgQsnxWNY8Of4jsX5xthkheTUEy-Vzk7TegmoaJ75vg-6KqYEDJy9OH-d2vmJUGt0IW180NKWxo6dz6xs1HdGwhssLUpFP_x3-2GfhcIxYW4MXMKPl8HC0W1N0cVRQf3xl8SB4-zWK7_IUpfmmyKSLALB4e_0ku6KYJrei9WvR_866cTsVhq2dnaU9GRZRQfjHOfZ5Mcm2K4wvrh03nFQhg-8jCfVissrSS1aNVQlWp9kh3hp1RRW7c1r0q0h75DXMXnizcxoiaF1NNSW5PZGQi0U6huWmeaUYs9enUg3m6YEe4g-wbdSyDZgFbIYj3qRte_-yy69nFwPhre4It7N1rJEKQqfGiM1Z5_nXSp21ZnlieHCbtneW7uZvOPS2a3Pq7qxtoVkp5se_0_R-b_HOOJIOHeTtDkMm8LNoaY-9qrL5pU8_Xb3TWpuBSeoPCGIg90_cxZUdIJow-78MigO8Kvv-5HyaucIr1OgPaJ3guyBfJ5AoRpfY2h668f7ILIEdjlB6x4o38EkRCkrbvdV6ZhPjxXdYsmfncfG91vzpzbHtGIlH8pDjgSDPLSmOUSJrpiXfOrhUneLF4PaOZLl3GH89TZzxVTG5cCUL8-rQ7qmm3jRNCjZt6T74uxIAby38y-jlL5279zOT9hWxGpMX2uTaCDvRprChNH3m2Q1z5xborHpX_X2sZpFFgmL_LPqrYVFyRdzOu7Ln0y8mD4WjHcCMwPFaLOSdMI3Km6WDChUF_tSJ_eaDPu2-3ustLmf6uQVzLvGIJ7BtrFDm1BunU1q3SEQElqRPpPrmKKu6TdIiv7jal-9WLF4y3RWiR2jf6nYhupmFE5I_Hxn0d_Ib5nSmimiVU1jtzlg25mRo2VLYgoXD7vdef_Ty-rOhorcz9XMfy4CFwYtQUurdg9NHtZLYnVKtREBrkO1FoSnzej5DcEfqaxxDTQP55uBFlWlBuQqoKpZh-1thbwDucELa7q8-BNT6oOGFBrEDrHUXFs8d52nONy8LwHVrIHP-rwuzMrskHVHVeAvZWR6zuBsdhk8bwQkH60qtxjEVgg74N4eIMDiGwc-68siNWROLyz-WWeK5fiuC9ij13YgQRNsjLZnLR4O6LzCNRLAz9By2vzMekgL5lqflYXEvR50lOtZYot-NqEAU1w6wWaTyA0HcDGu2elGa2xs0UKuTGPECglz9n-3vX66_FcrsdUYZE3Sq8j6nj0GsN9jPO6xwlGCisB_JmP3MWQUZHtOu_v8icQDOQ2VZ9tuyS_t_jFSSHLnhNqOGDOZhBv_Br-4eJcYDxyQS5dcJ_6M3mrY7TNwfBDtFLlwy_8Sa76wIGUzP2E0FYYpI4zctcOFX9NXuK04TJGKlZh_v3OBPdPcKbcQsUD29m0asBztxsytOj1TiKT42kY3-5H6V58HTtERrqb0lV9tKOh5KGKhP8OCewjaCnWVLACubFIcicg9sD25LxEayZ3NosASmAvCsCllm-Fb5OZQ8OlpH8PYFOnLeJZ1WhXiZamqsMzDnJ5onZYB9P2xWEnittBDgT9hRkUZHRW1Y5vHhPaBeFAQSU_yRtzecz1OGs3RBgfTy3DFsQJE-x36AB4_7at8A1Nz1bnIA4S1ToaDBLU72ZDj4ytx_g3HTeWe16XxeXCOIU1WjQu2gji28w-7aF36P8ehefgwgzLUC-PcN99BJmfFvFkiML_qBO_XDmUEXV19ypN5IJjyaqGA9_U4COKlgBDhqoI07ojzjF0F_YmhXkFK0kcRUeIWWqL1RjgUsHc9ciL9VkwnKxJpi184WQKqIGlYG4PPTPoDqUXK1Yx1Xy1Pbe90QRBSLZM5HaCLQ3hU6Vq21FZQeXPRKtUJjCz5SGNXKV5ngmdVHsgHIK_j9qD-WMAF6w5Ovx1Ag3LW71-5uFNLzxKwt_mgCdh9Hv7bv33g-71p4tKfTCHpG90f1Tu_ZaaJtL6JIOAf5VQM-EQA6Qb-7DRv49LWy0wlYn9mQyvpO6U-8mstMrUY4kOd155i-YJiNP61olu2fgjy0pS4BMWnhrhztooEfls4x_DlVqbuYFhxEsNpcd4B56NJfEg_704FNinVib0EAaYpp0yFcI5fmNOoGe9HAjrjUwf4OIXHIVp29ldx4xEcN72U1NKzHSbHy0duJ6RJg3Rdl9BX6Ylmek6nIa8TGGqBOctZYC4VJ8ca7jYLGFjzz5vNZupJQUUUsIar4jPRkz_QiTSlWVSQQzTlOVz86gBd6JGCHY6rN3hUZMkJQeajU1Dta-eGpLoyfBTFkZle-qBfYY_Ez0s_Lpgk8aQT8n51_np4dmYm0mL0a03paMRJaOD-uHu0M7hauTiyNsNdQw_5LRtGvT2F1_dFcUpq8GQalRUa7f8vmYP_9dy0w0h1GIDaRw5FeXuqyZwJKqzeZ7jcQ69R4mD2kVuyq_2Bo4HlpI1L6xcrUHDRBydKaZjNeEaPj2M7Kfc5a2bwDKF_vxgiYvj77fa3Xd5-jLrKRvF-6X1a0BcapQVv43TO8G06g25BwEEg8Ay1ft4Y3fsxzwMVSW6OO9iry4Bg0cwBWXP3jCETJxmaAXZ-7QCop43tVHfriHb6f8XqXp9Jbovkam5dbFH37OqlAc9gABv4xu5vYMXTGUsawdq9FugrAcJ0dAzlbmdMWbCn3l0O0S4zcgVlTxvUzV4QytT1sPDVuBkwdAf501lUfClLGroATO5GsUK_O7w1Ni5UylbajDzC5Rhl_Ad5Thb5BsFQrzwN9k7tVjlXyduITaNNFVbE4tVxssj598OmSo3C6aLAc3TY7y0h68qjS-dc50IympUt_FOOCt_EiASGfWiF7YDSPe5jYgeGlhanPyLyM_8L8aXAsUKAvKL9SEDEDInW6nzLoJIPwU3p3DjokNa60KaTRVSVXdswChacH3okawrhXSg-_6p_Ytr7RF1RdcgHz8C3B_Y6bBbw5KEe2ks0px8WADHebuU0bnATmow_6bWxOVEEb_GgfYH7Nqn0b4aPM6QE9LhQLawGEExaI-aVcS9PPAS2bDGsysCG5B7LGlNdxWMrZYzizujvXlVcNlt2PLxV1XujXqQpNpKWBw-vKH3XmodltV13KUNZf1_FyIt3P3jz-9jgv5k6ZLcxl7t7QpwTjR9x70W4ox83gdwQb8fo7ikI0VXi6-YusoNjARKjVuvFBtGWlbm3lC2rwX0fdM7jAvCfNQ-xS6fAgM1AhvFl1ZLYG1aE4AK5XIXlQPw2MJb4kLvRnI3rMwyf-dKqkQba49HowxBS-rPwdiwky_3hNMBvE5eVcHkIqicpbnc0JCqS_V8zO2Grf3o4UiPbYwgn5PtsrPrgioCm1wFYvjMzBZFSzl1bHy8qUDvXdaqCSPTjT1Rv25Hc_0wRmXgVMO-enm-u8INjbKeuFR2Q8UKOQ277pFyHVPx3FOWigUztypycv8lERfukgTou01RLdd0JhjRXY11GzP7ryGSbafX3chQ_btgINCsUkcCwWga2nu-xj4FDV7WsMvbIVl4NMT4cGDx2QJ-ShX3dWvEkJUbsrpmcDxWm6uG_JgnSdJyxjC-IxqrZU8st2HUQyaBdnAEDLS_JrYVRdWX0yi4j4wHFErgPeQTIXrjH-rNQ4uitUcFko007mMkVS4KYTJzjhGLjxHuqCk7Bv8QpRtHYq4cuOjWXkq5YnXGRdLps5aJj4MWy9TkhJg-EntkMfcNKb_HsBruneD5AuCgrjx_tNv_zEYD5GeRZSQcXborUfxC3x-iAS4-PJPBzH4CQWFbBnBf99h9Y4hHaaltbElqYOk7x5-p1HyCW8wVx3jAiDKmRLbyeKa8Ya7H3eYySCZK8BeJmO_hb7Hl8v2w6IhY03JMq7QtYJQjY5DaJg6q0Q3xfH-mI2U2C_VdlcIPVwLNZVbBnG8QE7kKLtcq12aGD00zfcnF9HPpVcZu6dhOmDF-KzKvkhSAyfCZJBJ8jqSeZsz_4dtmpCzG8VQhQ8rfEZoldW1MTtaxUSM0-PndtVKGtF9F9YEya0uGxbIJ_T5w1bVjGcclBefI3PP_vi8Urk6NcoFIZ-zBXOhsE1X18LX1cn8YSJvEFp2Jv8KHE3buQTQua2B5B1hioSoLdbcrIvTYuYIUa96C_UmOMaqzgvqeYLkr1lXbT-hTlySwDpHkyuxu5ODgsfd4zv7UKqda-RMpFmx9l1rbPGOgvajWGcZw1EkKZTl-zH81JB_nLqoF9qc73r8r5oDlOBN7Uss44LxBYOnQGaQ-1f18tA79CtRL57jSn0xGbNIX95vCbW9H_SPESVziB4BDA-g_CIFxDeITQy8VcEzwrWRrXbsKQ3gutrRBrHJw31TMDeQCpkh3gdxeaObDTjiXqqWZqYmJY-HjXBMAOMC6dAaYlkn9ZMIR_5F5ue2vdBTHnR5kbGNw7xkz6F6maPU7NECKRJHnHNsK3kZpTNeS5gwys_R7sstRwXteoNDyFIqXO9EY4DmRfi1xNgGrTtAU1s612bKWXKeJQsdK6TALR_vrYmc_A5RwKTW8H_zmzfj5Xx2OjgG3cckHC6BH9YNhnBsQcE0377eunc-s4S15vt19aVe7xYwZTkcJEjmWB8o5Xl3Hn8AL81o3ieScdpjpqDaZpooLn3UJUkm80dXsnhJxAcjwsH1tMCMTrc433i93E-K-u3nCZEhOscWGf_RKenRT5jxmTphpdyeSkOCPE_96WzCvfYQ94wiz-N-hvdCrMEzb78Z2CX5plL0_B-BhPFj7TXSD4hyZtjEdX3dfT-nfCE4UgJcBCm3fEc9LXm0y_zhGP-2qfAM4yC6wrzzjoUCKWUWAoPStpxmMHRWqkkEe0H6XlqSyAcrNI2nkYUeEy0ENazMpM1HiT1SpmXsaV_Va1iTpbGULrt3UNming-5GYprC-_4lckRUwUL4XJFc0bf-OsuHsAwEXQA4ALnOElzeLRD3QOpwULaUXosgZPBn0H5Aaj-TK14HD7ZEMKm2yrPpoROlUf8b1HUzweYMMLgKjtw9axV5c0MvR9wHmYRa2U5ecELK43scGRQyv8Z7k5A7kMETUMVlwD_BSTBrMYdjqsFZ4B3nSFbFnirRS4KCG5pRf9tiVuBT_dn0-j5HCNgw3_gp5r_gt8XZYE37MT0NKOQTLCNw8Q3ShrzQxfyWQ8fBjJqSqnW6aZz9j1Ky6Q2-eq460YmjmSB-ne5rV_RFSb81sTtdxpl3iq5f-Dtm9qq111TJJqCFLh6vCmIRJcZ0ZanPPNKbMcQWcclQ0eK6D9qKXJUR0puCsRoSrBcfiaBT2pzya1266hs_s5v9GBpaouSL-iUGnFZ92KZysqdd2T_vLC033Z7OunBUj4cHGN6dI4OfFAeIbWaSVstLOrCbgkfisYvyniUkcXpfBydddrVVhBDgi0xvxMycphMZIJJxD7WmwCdInQ7A8DGQGzE5yISIf6RCZelVpMJHyDV80wPLiK2cjuUcc7MSR5-l8eJdTdQ2Kt3jaqgnk_dfSRSlpMSDDT0oPUfNU5QJ25vaJsOO58glklvnbOLPcm6-YKtXdXA_mQlkfXw3U_8ZMJJ8my7DlLpiXmHbwMXPFhBNj40ptWxMeQ7BNzOs-pF20vdFDSThUDEe7WUWRyIMa-c-gnww0J7p4LysYjNAZ37WQ03H8NKDh-44d7VtIWsIt4AC3yfLOBDL1aU6X5rBWAWKLFNGTIqYCE_NpboBGCIzb29EAotTUdNVKxlABUXBdB9PyGjgW36JxzUV44THWSu3ExfH70JDyrdQlzgvch-1ZuUHFni_ICNkPtkVf1sbgq9yraEiHSFOc1OCeA2266RpzS9flbCfuxOk16GSDm636jUjSMId-kyn3qa6UyDlOZTz2dU2czmwDDr30fNYhQTcfKQQKfYVz6ggz6eeLFebCB5ygyIJxk7wbng0Ez6MsJEfniBOP12qKELceYF7vaSzrQzxqGj6UMRpYngIKsUEbBD9Iyw3ZYzttLw8kHrS4Ylu6frZlfA4_1wZwOYniU1LH5QLZn2DC4uw3JRzdaAHhhDsbOWRvJK9VGW-qFSZK51DwgpaiH_46_qAieoy6Y5kNQFt5wEc8dsqpSjB_hyCRIGIhuDVJ9GeXLhTnYkr6iRX_EuV4HvKcvqS_DJkb2_xVQa-4Sy8UMKVpyY8Pdlyxn3QGYxscdhNC4NkWE1tbmpJId4s4lLRSgJwmx1IR341vfEApiFKk-fR0bjkPfTUoAOX0CeeLkWGfpf7RrMcEhf6TDM9OR0nQAeLuvhus6GqEr2qWHTAgehcK72tZtQIL8DH9eEZgarz-mkjoOKl0YfmfUl2QJzOyAObnKc7iqCCfJ2mczpdeConoE73Z1N7Bc3v2Glvwsf0uIpZuG_VdzGmebKfoDz9NT3gU-7C9Ka3_9CQcyRj-QKzSFP9E2uuZgqd7ISFH4XJVk7DS_UuEUGxo6ECG-KC9at_yjzis70EVbGxvfXirtJJjagmz_5DURIubS0N39bdNktIMg8Pau7IW3FWfe6XswdOeJKAqGrlmsbUR9Uo76QmYCpWQDgXBB6ugsKTwsosSXvw1Ffrfa95quLczvNlUk9ccKBKnoCKPlbK00qhiZA9Zq-liOLWXdqcukpI5_QJI5N58oAKkf2SSY46f4kKT2gQfBoS7Jb311IzTQtvmlVNGkUV647gfSaFIkrov2d0Ljb9-a8snmZZFiVfnrLbmFB-tShpeFN-I28tU_QYcrpFG-B_senBsNNNvFDV7jaMwLaNu7cvaUROjaYui35luE0f1Emk9cBi72PIWnQw56fzKuvs6OEt68uU8NmuGkFExYWWWcxAKaNLY4VayEMXvR9oth9pOO1cKEhddTBaUF0UoDrm1n0eamVgxvSCfUMPIM1dbEypiIlFbzke1SrQrJc4IF0wL_fkf0n5bLaP-UV78o_jMDM5UsOWO-HXkwmhMSJ3ycTM54y7veBm33ZCzCLOHwjaf5RbDMos9UaaPfs-a2L_Riw1AEsYMllwOZyUhM7lz4UPYdu7Ezqg0yKao8DnBPdxiJalt3fl6p6RiXLYJ9o-oy74Ca_y6x5hY9AG51olViziKL2q2tSlJYAKVrYMwXQzciRtdkW0casce.VAWhxI7TnPx-GinSX9RiIA
    """.split())
FOREIGN_CLAIM_SET = ''.join(
    """
    eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..PzFNInV8-m-iFieP.UJwyf0pxeTvqaZd_5gxXxlviYGF18vWPIXhNtOXzaJV4TRFESBKfFZtp8yAKpRxukG9eIOPWwp0I6sxzG5fJIifi-_rdrAoKWtOURKogbat6EXgp0eE5p7KF66l4Kwq954lUav67bfVFBUB0Ufio08QFpS_uXd5Z0q2ZYeJP65FgyRhg_X9V7CzuTIs7EL0UXOfWJ4ZkkMJXP7hVQLLGQITVbdHqYogqj9MCCoiB512aFVbMXGYwKNDtRpYfLgKAgh2daavhufajiAbMWKIx-8ZZem5HSc5dk11Cvt8nTq8maVmhbzD0X8iMBA6zydO0sPOE7bKaAv9K_-MdKGhLEE-wFRGy43b9FHNAlRMn9sOBajssdm3K_8xSE9iL8c88zRXxDNvb-Dqvb6PkAH9VlWLbvTionGl2ioP9erzUoCliw6_xEAiDi7mzJYdswJDuv4v2bL7XwRiXDtjUTkGnWIi8lJq9EjqIhfrP2f1tDksxoqdXiFviC4PPfDrkIVjtL0xYVraIMm9zpHFka_ZsmC4bk417CeylW5hQXuf_7xFuKcb6btLQ0_wsTz7V-FSJNzYhpcHNWj1yW_tf2L_Sl66U1dDDCm_jg3i4NBlSorWqEczYPq9mz9NWvx9jigddsvSkuPezPbw2LeXQRUvzBIumCvknolbZw9-zndgmuiMDSV8PhZZijf-OCHSURAws147LkoKmqUf03CY2RKn7Z-Xhd4QD1ltgQAvX09JePHlOvZFMhIXYZZGEQ8PAJvwAlWRS-FjgdFns48A4h78yCX1usns12g3PW86ryykm8OpPrhyavNotUt1cqBFQm0ZSPyRq_OXObqPKrhZnOfubnL9QIrrXbSwqrqs2p-rAnODZiHwVEQVbNdheepxk07EE_mOooAl2euMWYgS50_zcRDnp63qd2hDMFs8SQv_qaCYqlIWkjKDzQ0x62xqYE_1rffeBagjgqJ3eH93rWBUW7yJm6wleNRy0JDS4qdp6Q0BZOoKghx26YQCh49t8JC6wAnQL7qTxs7x67rVlLQqRiHt7_oq-j2mg4ahEproFwrpXTsQlKPq_-myyktP2zDGW3WpqqPJO5o0-y5Vr6rVADDdkZygw-hKgDJomt5iIz1xuyTyjZjksmFWUFxQMtDE_SoP9mRmutlU0y5KsgIm5_Z_ynQSpPl9z-KNisVEP6yUSFuH5Nc_AksPIb4OUCHDA2Zv5oKn-9_yX2V_2r6n2j2-rWtcA3adgR4O-xzm2h7FMzmN4CHL6V9i3YN1ekbsYS1Tk28OveUWkvnmcr3dclv-Ac0_Eoy3-RZrFvwViC8Hb7g2gnYjhwbKF6dMENgISEyoolYUZRDTmy-r9eMSSdux3ohoknF78UQYvyiqU5Z3pIf2tIxdYq7ZMj-ciuoaIS05p2hCiU7odhzcd0hTAMPz0iYmCwdYSat_c_gUf8EG43eEIAOQXorkFAfVL2IFC9FE9FaXucaa-HqbPS-9BJJmw45Nt0tR0f5h4wJ9mFO1T9mulZEZjZrDFvGsrbk5XpxeHP5dcHoeYJEa5L8CPQKPIEuABqN53P07aynHU9uYUDBCwyOCyc9VnhWwXVhMC0oRWY4SsjD37kLaPyJsA4wqJMmJe9mAw2nlCkAfjaqnNtWWQQNqxhJUMyb4tOqduIR9D4pGyIn_uMYurrpFuw7yYvYdwo-7EABIEb0ITQerCjzBqJkySi_QRcE3vSEYrcDxr4Knu89fHNpnYpEPkLvzSWpMCK6Sdo34gt5J5qqL2fpsMzGDu9NHGUQ3SYwwSbIZbYVCiHB3bb3KLZCWm_93mOhCtg5fiOZCaK7PbTJBYLcDtT0AXWF8_seMlsBq-UJrV2xr0ShxUlFevff4GJ1h2BYUmw16KTO_YgyLzXifScIrTiTYSRXSHN7w3cDZ0r0BSXyIgY8jM52ZH6Z7bV-W-qwlSCD4bfucZT3V5iPLJTuRf47e2ch1LL7ACBY56hiSvjAgDAubzzb9Vu_JMZgflneMliAmOVWJq9bjwfwTJ_JhQBDC8p2SwWItAQ3ZtJREExOCBR8Hh07eqMvOcJk0KQm9QDejO_zDSAECm0M-xrKIOBBV1TIEDDoznUmXqKHMFmpO7EhhKHWaJ_kBVD-_3_neHFr6WA90zlm0RQGzdTSrd0dAhXVkyKsdu1vjfTRY_2J16xpdWnkmU3rdA_VrSuXpOLhG94Qo7hzaV3wejC7T0bj44BLjT_qpbqydd4tSVitQ6gKtWOrgvlSPliVMtln5ri1kEyUGqpW9eGR-gEu2LW5WY4lXauIIypIyDIZ_PjFHCTGuGIgy3ktxZSwNs1eNH83FN_fJCdxCrSNDy9BYOsxSCTA6YUxjWapdcClquUSTIWFg9qGnJXtFW2wQTw4GgGgTH404MftElKlkHpeo1FbEmq0qIxGDr_RwF0b0EPOsfBDPIxo4o3AnekXP8tgaNcFBBrHt_6OpoCNxvxHQEIvDtONxMMCCg6BFCVNMwHKtIeqgdFeGOPrv4LkwT2TWbIUAopsoMZeH14nCxSwAlYXLAR34IhfCFPTl0OQkEn2Qr2WSa3_8BrP_DVQuMgLOH4Bzc_5JXJAIZYznvKDZrpldzHc2bghHeulkSIWOH-WoG1XsnddKkz3Ld7abyH4xTRVpzomcwLii62sMuydFOAs34j0O7l_8AG_QySjsTtakp.DY8fplSMRZcFBO0NPDSsdQ
    """.split())
FOREIGN_CLAIM_SET_DICT = {
    'claims': [
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAABa.5JaB27g7rjJ'
         'JYvjGO6R2_6Cy9TCn-Zw1wNMqt2RLA8UgRC2_N6sUQ_bLS3_sRs0lfBnoPODwXpsL'
         'VjHJRnn6VxCa7_dARGmQj3-Mh5PZooxNOaekplblENz3rbzxUVJlsOgEyVg9KG6wZ'
         'nPw3WGegP5Zp1wGTdI.Tqbyt3NeSrvr0DomFpc15g'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAAB7.-cdL78mFwqM'
         'LbCe1urN-P2tosPYAjbUxMXZwNC6E2H329tGYomEHs5hOL_u4YqqsvSR14RfHfmeq'
         'fAcGnRSLKJ2K1iuAGicuKYkrBVzjsDUdKC0DTr4sksfD6enOn2C1jQ8wu4HJ3-TCC'
         'E79R-FO_YATVxApmAj3XA.SgYm1epvW2IfTCzTqzhB-g'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAACc.BKCVZxoby6G'
         'Qry0Cs-xk3nzx7wzxCqmAJd9xQ6P5KjPTu4SOZBT1VlUBZsyNvf62McVxP-XOqKsm'
         'wjolbSuXxIx-220UFfDnFuPX.ExoHLA3EeOT_F_U0MRHyMQ'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAABl.qkpjcC3Iraz'
         'rYXK37wz6odisek-ZDmwgMmneRVjG0ge4aabhGMF26fDgPjZiJqDgl-hOLeJ34lZB'
         'fSfEzR3mWJi4uZPtdluCBmRwpJwnuTtH9QR8xU0HzU62x_zE1hmQzcu09qKzFW5-0'
         'JCuXuA.HCYK39pFVNNwis3rsZIG3w'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAACG.ZrDjN5G_s9J'
         'KAJltCFBmejya-edH38lYoJXon4NrOBZV8b3ya9eVVvyoNtsiKO-ABLAOL0eWMU9X'
         '6c-1321pVa4ixPs_2dxmc6JlkZllHn3EWALS7q9oLOFLPveBg01dbzhJ3rEDtVUaZ'
         '6Cc-2i9tmx-PTiiEw.VB7LTKIA4nRpgnhw4BOG5w'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAACn.Um-dkZ-yboq'
         'CVR85LLil9Cjv6I0f3_Ay1jt3ay5OL5AKyb-66S9JG21LYdk8xLWWmAqMVrRmXTZF'
         'vYQ71vnq739dYYCAFNw.6AD25ss-l6A95SZqoc7xeQ'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAABP.szHU1b5iLkA'
         '3MvzKtqBdDMW1wjhwpnewKo35xU9IO5HExzY0NZW1I3AGDeZbOv_FDzeetCqw3xQk'
         'nEgwoBu-q1XlvNdlLA3R3N5u1Sl4RdeQSb7zym1lRmp96ltlSLtKsT2s9mcT63SBo'
         '3hVil1PfiCcHWE.LzpxTkCddKMNs_HN2Vy3sQ'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAABw.9br06pFZeOp'
         '0XspJQxVFGf3_Wategvsq3tgQ3ZfnMXoTVz83sZ-J5Ci7DENwzGkzkKRbly0Dsw_b'
         'Uvjv38W7jDmW4xuGwuFw0s0LA_4Mf4PeXEFOlQDnwe9yS06KtSsVxRuxkYz8OYBFA'
         'Rh02Q.1qwbVaHi3QX3TuQs2yNEQA'),
        ('eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0..AAAAAAAAAAAAAACR.rgam9pMCRd5'
         'YXC8trc0XM5NoIt3OXDaSwSzJ_XGZoQp75Zz85sqLnPGwU8IctXr2_8M8Spe2nBk5'
         'HGCzV9VV2helAiIIVNFceKQ.-z55Js9LTrGv5aMwPBNwKA')],
    'exp': 1568622642,
    'iat': 1537086642,
    'sub': HARRY_DID
}
FOREIGN_CLAIM_SET_KEYS = {
    'claim_keys': {
        '0': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAk'},
        '1': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANI'},
        '2': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPM'},
        '3': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQ'},
        '4': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAN0'},
        '5': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP4'},
        '6': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB8'},
        '7': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMc'},
        '8': {
            'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
            'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOg'}
    },
    'claim_set': FOREIGN_CLAIM_SET,
    'claim_type_hints': {
        '0': 'Person.birthDate',
        '1': 'Person.address/PostalAddress.addressCountry',
        '2': 'Person.address/PostalAddress.addressLocality',
        '3': 'Person.givenName',
        '4': 'Person.address/PostalAddress.addressRegion',
        '5': 'Person.address/PostalAddress.streetAddress',
        '6': 'Person.familyName',
        '7': 'Person.address/PostalAddress.telephone',
        '8': 'Person.address/PostalAddress.postalCode'},
    'object_key': {
        'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
        'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJs'},
    'trace_key': ATTESTATION_TRACE_KEY
}
FOREIGN_CLAIM_SET_COMMITMENT = ''.join(
    """
    AAAAAAAAAAAAAAAAAAAAVw.AGYe-S-1w6CTBuqwO8WcGf4H7Otaxvui-8yiwtPTzOU.
    AAAAAAAAAAAAAAAAAAAAIA.P6D5UiDS1rJuZtnkMGX09alY_9iqN5iCO6vkW88f-DM.
    AAAAAAAAAAAAAAAAAAAAQQ.n_cfefdypJ7cFVoIGfsIMI9ky5g6hfHL58EpI8Yy0zM.
    AAAAAAAAAAAAAAAAAAAAYg.Xuz7vPp60gsnC1z8INCsT8htaZRYBBd9ZpUqvom_A2s.
    AAAAAAAAAAAAAAAAAAAAKw.HYEHYcn_fhilrG4M-uhyjhUMJyCG1GQL7aWjC89fMrs.
    AAAAAAAAAAAAAAAAAAAATA.A2za0GCWawKao391NFNu9J3BO0gfFJXcAWSmSB8SuUc.
    AAAAAAAAAAAAAAAAAAAAbQ.sKDjE08J31ttkG8IV5vOMKhFWj2jpPwtf2kfz9DWg6U.
    AAAAAAAAAAAAAAAAAAAAFQ.p2NZUVJaASF7vNYCLDzqYtjjdydqciMxOacOI6RiANM.
    AAAAAAAAAAAAAAAAAAAANg.Fv0xtTtPHjW6jCSDcOgQ7NoIajwQ0ndbDA1A_eJCstY
    """.split())
DEVICE_ATTESTATION = ''.join(
    """
    eyJhbGciOiJ1bnNlY3VyZWQifQ...eyJ2IjoiMCIsImlkIjoia2F1cmlpZDphdHRlc3RhdGlv
    bnMvMDAwMDAwMDAtMTExMS0yMjIyLTMzMzMtNDQ0NDQ0NDQ0NDQ0IiwiY2xhaW1fc2V0IjoiZ
    XlKaGJHY2lPaUoxYm5ObFkzVnlaV1FpZlEuLi5leUp6ZFdJaU9pSmthV1E2YzNOcFpEb3hNWE
    ZZV0Zob1lYSnllVmhZV0ZWU2J5SXNJbWxoZENJNk1UVXpOekE0TmpZME1pd2laWGh3SWpveE5
    UWTROakl5TmpReUxDSmpiR0ZwYlhNaU9sc2laWGxLYUdKSFkybFBhVW94WW01T2JGa3pWbmxh
    VjFGcFpsRXVMaTVsZVVwQldUSTVkV1JIVmpSa1EwazJTVzFvTUdSSVFUWk1lVGw2V1RKb2JHS
    lhSWFZpTTBwdVNXbDNhVkZJVWpWalIxVnBUMmxLVldGSGJIVmFlVWx6U1cxc2ExcFhOVEJoVj
    Fwd1dsaEphVTlzZERkSmEwSXdaVmhDYkVscWIybFZTRXAyWTBkV2VXUkliRmRaVjNneFdsTkp
    jMGx1VG14amJXeG9Za1UxTVdKWFNteGphVWsyU1dwUk0wMVVSWFJOUkdkNFRsTktPVXhJYzJs
    UlNGSTFZMGRWYVU5cFNsRmpiVGwzV2xoS01HVldXbWhpU0Zac1NXbDNhV0p0Um5SYVUwazJTV
    3N4UWxGNVFtaGFSMUo1V2xoT2VrbHBkMmxrYlVaelpGZFZhVTlwU1RWWmVuQnJXbFJ3YUZwRW
    NHbGFWSEJzV21wdk1FMXBTamxZV0RBdUlsMTkuIiwiY29tbWl0bWVudHMiOnsiYXR0ZXN0ZXI
    iOiJleUpoYkdjaU9pSkZaREkxTlRFNUluMC5leUpqYjIxdGFYUnRaVzUwSWpvaVFVRkJRVUZC
    UVVGQlFVRkJRVUZCUVVGQlFVRkJRUzQyZDFNNGEyYzFjRVZMUW5oUFNHY3hja0p0ZW5wMlFuZ
    EplbFJhVlhSMVV6STFVM2g2YjBFemFtczRJaXdpYzNWaUlqb2laR2xrT25OemFXUTZNVEZ4V0
    ZoWWFHRnljbmxZV0ZoVlVtOGlMQ0pwYzNNaU9pSmthV1E2YzNOcFpEbzFSblYxVVV0RmRGaDJ
    TRmh3VFdSUlJrMURORVp4TkhweWNXcHZkMGRvVVV0UmQzbG5jM0ppZGpnM1NuSTFVRFlpTENK
    eWIyeGxJam9pWVhSMFpYTjBaWElpZlEuend6XzdvUS1JLW40Y0U5NWM4aTdpVFIzcVJxT2UxN
    3RLNk1DRk9OTzlRVGhZQjhVWTZfSExLSW1hR3had1hYQm5LcFhQMVVLUlh2X3pvWndGSWdVRH
    ciLCJzdWJqZWN0IjoiZXlKaGJHY2lPaUpGWkRJMU5URTVJbjAuZXlKamIyMXRhWFJ0Wlc1MEl
    qb2lRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCUVM0MmQxTTRhMmMxY0VWTFFuaFBTR2N4
    Y2tKdGVucDJRbmRKZWxSYVZYUjFVekkxVTNoNmIwRXphbXM0SWl3aWMzVmlJam9pWkdsa09uT
    nphV1E2TVRGeFdGaFlhR0Z5Y25sWVdGaFZVbThpTENKcGMzTWlPaUprYVdRNmMzTnBaRG94TV
    hGWVdGaG9ZWEp5ZVZoWVdGVlNieUlzSW5KdmJHVWlPaUp6ZFdKcVpXTjBJbjAuWHRDbXM1WHo
    4NFN2MWZBdjNhdHBHM3JNdDktOWozMFpVa0xfYXB6SFU4S2I5TDRVV2ZIYkFaWGxjQlYzZTNQ
    R3l4ckdIc3d1eHV5blVFbXhhSkNyQlEifSwiY29udGVudCI6ImV5SmhiR2NpT2lKRlpESTFOV
    EU1SW4wLmV5SnBjM01pT2lKa2FXUTZjM05wWkRvMVJuVjFVVXRGZEZoMlNGaHdUV1JSUmsxRE
    5FWnhOSHB5Y1dwdmQwZG9VVXRSZDNsbmMzSmlkamczU25JMVVEWWlMQ0pwWVhRaU9qRTFNemN
    3T0RZMk5ESXNJbkJ5YjNabGJtRnVZMlVpT2lKa2IyTjFiV1Z1ZEZ4dUlDQndjbVZtYVhnZ2Ey
    RjFjbWxwWkNBOGFIUjBjRG92TDJ0aGRYSnBhV1F1Ym5vdmJuTXZjSEp2ZGlNLVhHNGdJSEJ5W
    ldacGVDQmtZM1JsY20xeklEeG9kSFJ3T2k4dmNIVnliQzV2Y21jdlpHTXZkR1Z5YlhNdlBseH
    VJQ0J3Y21WbWFYZ2daR2xrSUR4a2FXUTZQbHh1SUNCY2JpQWdaVzUwYVhSNUtHdGhkWEpwYVd
    RNllYUjBaWE4wWVhScGIyNXpMekF3TURBd01EQXdMVEV4TVRFdE1qSXlNaTB6TXpNekxUUTBO
    RFEwTkRRME5EUTBOQ3dnVzNCeWIzWTZZMjl1ZEdWdWREMG9LVjBwWEc0Z0lHRmpkR2wyYVhSN
    UtHdGhkWEpwYVdRNlpYWnBaR1Z1WTJWV1pYSnBabWxqWVhScGIyNHZNREF3TURBd01EQXRNVE
    V4TVMweU1qSXlMVE16TXpNdE5EUTBORFEwTkRRME5EUTBMQ0F0TENBdEtWeHVJQ0JoWTNScGR
    tbDBlU2hyWVhWeWFXbGtPbWxrWlc1MGFYUjVRWFIwWlhOMFlYUnBiMjR2TURBd01EQXdNREF0
    TVRFeE1TMHlNakl5TFRNek16TXRORFEwTkRRME5EUTBORFEwTENBdExDQXRMQ0JiWkdOMFpYS
    nRjenBvWVhOUVlYSjBQU2RyWVhWeWFXbGtPbVYyYVdSbGJtTmxWbVZ5YVdacFkyRjBhVzl1TH
    pBd01EQXdNREF3TFRFeE1URXRNakl5TWkwek16TXpMVFEwTkRRME5EUTBORFEwTkNkZEtWeHV
    JQ0IzWVhOSmJtWnZjbTFsWkVKNUtHdGhkWEpwYVdRNmFXUmxiblJwZEhsQmRIUmxjM1JoZEds
    dmJpOHdNREF3TURBd01DMHhNVEV4TFRJeU1qSXRNek16TXkwME5EUTBORFEwTkRRME5EUXNJR
    3RoZFhKcGFXUTZaWFpwWkdWdVkyVldaWEpwWm1sallYUnBiMjR2TURBd01EQXdNREF0TVRFeE
    1TMHlNakl5TFRNek16TXRORFEwTkRRME5EUTBORFEwS1Z4dUlDQmhZM1JsWkU5dVFtVm9ZV3h
    tVDJZb1pHbGtPbk56YVdRNk5VWjFkVkZMUlhSWWRraFljRTFrVVVaTlF6UkdjVFI2Y25GcWIz
    ZEhhRkZMVVhkNVozTnlZblk0TjBweU5WQTJMQ0JrYVdRNmMzTnBaRG8xUlRKS05WVnhZV1kzT
    TBOdlMzbEVObWwwZGpSR1kxRjJaMVphTjI1YVVqbHliVzFUTTJORFIySlpUbmhGZGpjc0lDMH
    BYRzRnSUhkaGMwRnpjMjlqYVdGMFpXUlhhWFJvS0d0aGRYSnBhV1E2YVdSbGJuUnBkSGxCZEh
    SbGMzUmhkR2x2Ymk4d01EQXdNREF3TUMweE1URXhMVEl5TWpJdE16TXpNeTAwTkRRME5EUTBO
    RFEwTkRRc0lHUnBaRHB6YzJsa09qVkdkWFZSUzBWMFdIWklXSEJOWkZGR1RVTTBSbkUwZW5Ke
    GFtOTNSMmhSUzFGM2VXZHpjbUoyT0RkS2NqVlFOaXdnTFN3Z1czQnliM1k2YUdGa1VtOXNaVD
    FjSW10aGRYSnBhV1E2WVhSMFpYTjBaWEpjSWwwcFhHNGdJSGRoYzBGemMyOWphV0YwWldSWGF
    YUm9LR3RoZFhKcGFXUTZaWFpwWkdWdVkyVldaWEpwWm1sallYUnBiMjR2TURBd01EQXdNREF0
    TVRFeE1TMHlNakl5TFRNek16TXRORFEwTkRRME5EUTBORFEwTENCa2FXUTZjM05wWkRvMVJuV
    jFVVXRGZEZoMlNGaHdUV1JSUmsxRE5FWnhOSHB5Y1dwdmQwZG9VVXRSZDNsbmMzSmlkamczU2
    5JMVVEWXNJR3RoZFhKcGFXUTZjSEp2WTJWa2RYSmxjeTlsZG1sa1pXNWpaVjlqYUdWamEzTXZ
    hWEJ1Y3k5UmJXUllXRmhqZFhKeVpXNTBiV0Z1ZFdGc1dGaFlMaTR1V1dkV0xDQmJjSEp2ZGpw
    b1lXUlNiMnhsUFZ3aWEyRjFjbWxwWkRwMlpYSnBabWxsY2x3aVhTbGNiaUFnZDJGelIyVnVaW
    EpoZEdWa1Fua29hMkYxY21scFpEcGhkSFJsYzNSaGRHbHZibk12TURBd01EQXdNREF0TVRFeE
    1TMHlNakl5TFRNek16TXRORFEwTkRRME5EUTBORFEwTENCcllYVnlhV2xrT21sa1pXNTBhWFI
    1UVhSMFpYTjBZWFJwYjI0dk1EQXdNREF3TURBdE1URXhNUzB5TWpJeUxUTXpNek10TkRRME5E
    UTBORFEwTkRRMExDQXRMQ0JiY0hKdmRqcG5aVzVsY21GMFpXUkJkRlJwYldVOVhDSXlNREU0T
    FRBNUxURTJWREE0T2pNd09qUXlLekF3T2pBd1hDSWdKU1VnZUhOa09tUmhkR1ZVYVcxbFhTbG
    NibVZ1WkVSdlkzVnRaVzUwSWl3aWMzUmhkR1Z0Wlc1MGN5STZXM3NpWlhod0lqb3hPRFV5TkR
    RMk5qUXlMQ0p0WlhSaFpHRjBZU0k2ZXlKcmVXMWZZMjl0Y0d4cFlXNWpaU0k2SW1SbGRtbGpa
    U0JwWkdWdWRHbDBlU3dnYkdWMlpXd2dNeUlzSW1kdmRtVnlibUZ1WTJVaU9pSkpiMVFnWVhOe
    mIyTnBZWFJwYjI0aWZYMWRMQ0poYm1ObGMzUnZjbDlyWlhseklqcDdmWDAuRmhlNTNxT0l3Nm
    FzQlhzSUJxb1hONzdwV2hIRm5FM0hUbzZHelg5NHpZc2RWUWVESEN3UnFWT1paVUp5TXhEYUt
    0WVo2TlpYMW9nbDVEUXlTSi1ZQXcifQ.
    """.split())
DEVICE_SUBJECT_COMMITMENT_RAW = (
    'eyJjb21taXRtZW50IjoiQUFBQUFBQUFBQUFBQUFBQUFBQUFBQS42d1M4a2c1cEVLQnhPSG'
    'cxckJtenp2QndJelRaVXR1UzI1U3h6b0Ezams4Iiwic3ViIjoiZGlkOnNzaWQ6MTFxWFhY'
    'aGFycnlYWFhVUm8iLCJpc3MiOiJkaWQ6c3NpZDoxMXFYWFhoYXJyeVhYWFVSbyIsInJvbG'
    'UiOiJzdWJqZWN0In0')

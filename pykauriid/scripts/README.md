CLI to PyKauriID
================

Claims
------

* Making a new self claim set.

(Using claims in a file `examples/harry_claims.json` and the signing key
stored in `examples/harry_sig_full.jwk`. The claim set is stored in file
`examples/_claim_set_harry.jwe` and the keys for the claim set in
`examples/_claim_set_keys_harry.json`.)

    pykauriid/scripts/kauriid.py claims --operation new \
        --subject "did:ssid:2omXXXharryXXX...DeG" \
        --subject_sig_key examples/harry_sig_full.jwk \
        --claims examples/harry_claims.json \
        --claimset examples/_claim_set_harry.jwe \
        --claimsetkeys examples/_claim_set_keys_harry.json

* Accessing claims.

(Using the previously created claim set and claim set keys files, and
validating the authenticity using the public signing key in file
`examples/harry_sig_pub.jwk` only.)

Individual claim (usind index, e.g. `2`):

    pykauriid/scripts/kauriid.py claims --operation access \
        --subject_sig_key examples/harry_sig_pub.jwk \
        --claimsetkeys examples/_claim_set_keys_harry.json \
        --index 2

Note: The order of claims in the set is randomised on creation (above),
      so it is likely that a particular index will reveal different
      claims at different times.

All claims stored (using index `-1`):

    pykauriid/scripts/kauriid.py claims --operation access \
        --subject_sig_key examples/harry_sig_pub.jwk \
        --claimsetkeys examples/_claim_set_keys_harry.json \
        --index -1

List all claim types stored:

    pykauriid/scripts/kauriid.py claims --operation access \
        --subject_sig_key examples/harry_sig_pub.jwk \
        --claimsetkeys examples/_claim_set_keys_harry.json \
        --list


Attestations
------------

* Attesting a claim set.

(Using the previously created claim set and claim set keys files.)

    pykauriid/scripts/kauriid.py attestations --operation attest \
        --attestation examples/_attestation_harry.jwe \
        --attester_data examples/attester_data.json \
        --attestation_data examples/attestation_data.json \
        --attester_sig_key examples/dumbledore_sig_full.jwk \
        --subject_sig_key examples/harry_sig_pub.jwk \
        --claimsetkeys examples/_claim_set_keys_harry.json

Note: The attestation is encrypted with the object key.

* Accept (counter-sign) an attestation.

(Using the previously created attestation and claim set keys files.)

    pykauriid/scripts/kauriid.py attestations --operation accept \
        --attestation examples/_attestation_harry.jwe \
        --attester_sig_key examples/dumbledore_sig_pub.jwk \
        --subject_sig_key examples/harry_sig_full.jwk \
        --claimsetkeys examples/_claim_set_keys_harry.json

* Access claims from an (attested) claim set (similar to claims above).

    pykauriid/scripts/kauriid.py attestations --operation access \
        --attestation examples/_attestation_harry.jwe \
        --claimsetkeys examples/_claim_set_keys_harry.json \
        --attester_sig_key examples/dumbledore_sig_pub.jwk \
        --subject_sig_key examples/harry_sig_pub.jwk \
        --index 2

    pykauriid/scripts/kauriid.py attestations --operation access \
        --attestation examples/_attestation_harry.jwe \
        --claimsetkeys examples/_claim_set_keys_harry.json \
        --attester_sig_key examples/dumbledore_sig_pub.jwk \
        --subject_sig_key examples/harry_sig_pub.jwk \
        --index -1

    pykauriid/scripts/kauriid.py attestations --operation access \
        --attestation examples/_attestation_harry.jwe \
        --claimsetkeys examples/_claim_set_keys_harry.json \
        --attester_sig_key examples/dumbledore_sig_pub.jwk \
        --subject_sig_key examples/harry_sig_pub.jwk \
        --list

* Dump all attestation content (not the claims themselves) in plain text.

    pykauriid/scripts/kauriid.py attestations --operation access \
        --attestation examples/_attestation_harry.jwe \
        --claimsetkeys examples/_claim_set_keys_harry.json \
        --attester_sig_key examples/dumbledore_sig_pub.jwk \
        --subject_sig_key examples/harry_sig_pub.jwk \
        --dump

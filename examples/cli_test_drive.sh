#!/bin/sh

# Encrypted claims and attestations.

## Claim set operations.

   # Create a new self claim set and claim set keys object.
   # (The claim set is JWE encrypted.)
   ../pykauriid/scripts/kauriid.py claims --operation new \
       --subject "did:ssid:2omXXXharryXXX...DeG" \
       --subject_sig_key harry_sig_full.jwk \
       --claims harry_claims.json \
       --claimset _claim_set_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json

   # Create a new foreign claim set and claim set keys object.
   # (The claim set is JWE encrypted.)
   ../pykauriid/scripts/kauriid.py claims --operation new \
       --subject "did:ssid:2omXXXharryXXX...DeG" \
       --claims harry_claims.json \
       --claimset _claim_set_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json

   # Access element 2 of the claim set using the claim set keys object.
   ../pykauriid/scripts/kauriid.py claims --operation access \
       --subject_sig_key harry_sig_pub.jwk \
       --claimset _claim_set_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json \
       --index 2

   # Access all elements of the claim set using the claim set keys object.
   ../pykauriid/scripts/kauriid.py claims --operation access \
       --subject_sig_key harry_sig_pub.jwk \
       --claimsetkeys _claim_set_keys_harry.json \
       --index -1

# Attestation operations.

   # Attest a foreign claim set object.
   # (The claim set keys object will also be updated with an added trace key.)
   ../pykauriid/scripts/kauriid.py attestations --operation attest \
       --attester_data attester_data.json \
       --attestation_data attestation_data.json \
       --attester_sig_key dumbledore_sig_full.jwk \
       --attestation _attestation_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json

   # Attest a self claim set object.
   ../pykauriid/scripts/kauriid.py attestations --operation attest \
       --attester_data attester_data.json \
       --attestation_data attestation_data.json \
       --attester_sig_key dumbledore_sig_full.jwk \
       --subject_sig_key harry_sig_pub.jwk \
       --attestation _attestation_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json

   # Accept (counter-sign) a received attestation.
   ../pykauriid/scripts/kauriid.py attestations --operation accept \
       --attestation _attestation_harry.jwe \
       --attester_sig_key dumbledore_sig_pub.jwk \
       --subject_sig_key harry_sig_full.jwk \
       --claimsetkeys _claim_set_keys_harry.json

   # Access element 2 of the attestation using the claim set keys object.
   ../pykauriid/scripts/kauriid.py attestations --operation access \
       --attestation _attestation_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json \
       --attester_sig_key dumbledore_sig_pub.jwk \
       --subject_sig_key harry_sig_pub.jwk \
       --index 2

   # Access all elements of the attestation using the claim set keys object.
   ../pykauriid/scripts/kauriid.py attestations --operation access \
       --attestation _attestation_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json \
       --attester_sig_key dumbledore_sig_pub.jwk \
       --subject_sig_key harry_sig_pub.jwk \
       --index -1

   # List the 'types' of all claim elements in the attestation.
   ../pykauriid/scripts/kauriid.py attestations --operation access \
       --attestation _attestation_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json \
       --attester_sig_key dumbledore_sig_pub.jwk \
       --subject_sig_key harry_sig_pub.jwk \
       --list

   # Dump all content of the attestation (and claim set) in plain text.
   ../pykauriid/scripts/kauriid.py attestations --operation access \
       --attestation _attestation_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json \
       --attester_sig_key dumbledore_sig_pub.jwk \
       --subject_sig_key harry_sig_pub.jwk \
       --dump

# Unencrypted (device) claims and attestations.

## Claim set operations.

   # Create a new (unencrypted) foreign claim set for a device.
   # (The claim set is an unencrypted, non-standard JWE.)
   ../pykauriid/scripts/kauriid.py claims --operation new \
       --device \
       --subject "did:ssid:2omXXXvendingmachineXXX...DeG" \
       --claims device_claims.json \
       --claimset _claim_set_vendingmachine.jwe

   # Access the first element of a device claim set.
   ../pykauriid/scripts/kauriid.py claims --operation access \
       --device \
       --claimset _claim_set_vendingmachine.jwe \
       --index 0
        
## Attestation operations.

   # Attest an unencrypted device (foreign) claim set.
   ../pykauriid/scripts/kauriid.py attestations --operation attest \
       --device \
       --attester_data device_attester_data.json \
       --attestation_data device_attestation_data.json \
       --attester_sig_key dumbledore_sig_full.jwk \
       --attestation _attestation_vendingmachine.jwe \
       --claimset _claim_set_vendingmachine.jwe

   # Accept (counter-sign) an unencrypted device attestation.
   ../pykauriid/scripts/kauriid.py attestations --operation accept \
       --device \
       --attestation _attestation_vendingmachine.jwe \
       --attester_sig_key dumbledore_sig_pub.jwk \
       --subject_sig_key harry_sig_full.jwk

   # Access all elements of the unencrypted device attestation.
   ../pykauriid/scripts/kauriid.py attestations --operation access \
       --device \
       --attestation _attestation_vendingmachine.jwe \
       --attester_sig_key dumbledore_sig_pub.jwk \
       --subject_sig_key harry_sig_pub.jwk \
       --index -1

   # List the 'types' of all claim elements in the device attestation.
   ../pykauriid/scripts/kauriid.py attestations --operation access \
       --device \
       --attestation _attestation_vendingmachine.jwe \
       --claimset _claim_set_vendingmachine.json \
       --attester_sig_key dumbledore_sig_pub.jwk \
       --subject_sig_key harry_sig_pub.jwk \
       --list

   # Dump all content of an unencrypted device attestation (and claim set).
   ../pykauriid/scripts/kauriid.py attestations --operation access \
       --device \
       --attestation _attestation_vendingmachine.jwe \
       --attester_sig_key dumbledore_sig_pub.jwk \
       --subject_sig_key harry_sig_pub.jwk \
       --dump

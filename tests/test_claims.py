# -*- coding: utf-8 -*-
"""Tests for the claims module."""

# Created: 2018-08-13 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

import copy
import json
from typing import Union
from unittest import mock  # @UnusedImport
import unittest

import nacl.exceptions
from sspyjose.jwe import Jwe
from sspyjose.jwk import Jwk
from sspyjose.jws import Ed25519Jws

from pykauriid import utils
from pykauriid.claims import (_collect_claim_type_elements,
                              get_claim_type_hint,
                              validate_commitment,
                              ClaimSet,
                              ClaimSetKeys)
from tests.data import (HARRY_JSON_LD_CLAIMS,
                        DEVICE_CLAIMS,
                        HARRY_DID,
                        ALBUS_DID,
                        JWK_HARRY_FULL_ED25519,
                        JWK_HARRY_FULL_ED25519_DICT,
                        JWK_HARRY_PUB_ED25519,
                        JWK_ALBUS_FULL_ED25519,
                        CLAIM_SET,
                        CLAIM_SET_AES256GCM,
                        DEVICE_CLAIM_SET,
                        CLAIM_SET_KEY_DICT_AES256GCM,
                        DEVICE_CLAIM_SET_DICT,
                        CLAIM_SET_DICT,
                        CLAIM_SET_KEYS,
                        CLAIM_SET_UNCOMMITTED,
                        CLAIM_SET_DICT_UNCOMMITTED,
                        CLAIM_SET_KEYS_UNCOMMITTED)
from tests.reference_data import REFERENCE_DATA
from tests.utils import (random_mocker,
                         reset_random_mock_counter)


def _alter_byte(data: Union[bytes, str],
                index: int, *,
                replacement: bytes = None) -> Union[bytes, str]:
    """
    Alters a byte at position ``index`` in ``data``.

    If no ``replacement`` is given, the byte at the ``index`` will be XORed
    with 0x42.

    :param data: Data to alter.
    :param index: Index position to alter.
    :param replacement: Optional replacement byte (avoiding the XOR).
    :return: Altered sequence.
    """
    is_str = isinstance(data, str)
    if is_str:
        data = data.encode('utf-8')
    left = data[:index]
    centre = data[index]
    if replacement is not None:
        centre = replacement
    else:
        centre = [centre ^ 0x42]
    right = data[index + 1:]
    result = left + bytes(centre) + right
    if is_str:
        result = result.decode('utf-8')
    return result


class ModuleTest(unittest.TestCase):
    """Testing the claims module functionality."""

    def test_collect_claim_type_elements(self):
        """Collect claim type data on a single level."""
        tests = [
            {'@context': 'http://schema.org',
             '@type': 'Person',
             'givenName': 'Harry James'},
            {'@context': 'http://schema.org',
             '@type': 'Person',
             'address': {
                 '@type': 'PostalAddress',
                 'streetAddress': '4 Privet Drive'}
             },
            {'@context': 'http://schema.org',
             '@type': 'Person',
             'familyName': 'Potter',
             'givenName': 'Harry James',
             'address': {
                 '@type': 'PostalAddress',
                 'telephone': '+44 555 325 1464'}
             }
        ]
        expected = [
            ('Person', ['givenName']),
            ('Person', ['address']),
            ('Person', ['address', 'familyName', 'givenName'])
        ]
        for test, check in zip(tests, expected):
            the_type, fields = _collect_claim_type_elements(test)
            self.assertEqual(the_type, check[0])
            self.assertListEqual(fields, check[1])

    def test_get_claim_type(self):
        """Get the claim types representation."""
        tests = [
            {'@context': 'http://schema.org',
             '@type': 'Person',
             'givenName': 'Harry James'},
            {'@context': 'http://schema.org',
             '@type': 'Person',
             'address': {
                 '@type': 'PostalAddress',
                 'streetAddress': '4 Privet Drive'}
             },
            {'@context': 'http://schema.org',
             '@type': 'Person',
             'familyName': 'Potter',
             'givenName': 'Harry James',
             'address': {
                 '@type': 'PostalAddress',
                 'telephone': '+44 555 325 1464'}
             }
        ]
        expected = [
            'Person.givenName',
            'Person.address/PostalAddress.streetAddress',
            ('Person.address/PostalAddress.telephone,'
             'Person.familyName,'
             'Person.givenName')
        ]
        for test, check in zip(tests, expected):
            result = get_claim_type_hint(test)
            self.assertEqual(result, check)

    def test_validate_commitment(self):
        """Validate a commitment."""
        commitment = CLAIM_SET_DICT['commitment']
        sig_harry_full = Jwk.get_instance(
            crv='Ed25519', from_json=JWK_HARRY_FULL_ED25519)
        result = validate_commitment(commitment, sig_harry_full)
        self.assertIsInstance(result, Ed25519Jws)


class ClaimSetTest(unittest.TestCase):
    """Testing the ClaimSet class functionality."""

    def setUp(self):  # noqa: D102
        reset_random_mock_counter()
        self.sig_harry_full = Jwk.get_instance(
            from_json=JWK_HARRY_FULL_ED25519)
        self.sig_harry_pub = Jwk.get_instance(
            from_json=JWK_HARRY_PUB_ED25519)
        self.sig_albus_full = Jwk.get_instance(
            from_json=JWK_ALBUS_FULL_ED25519)

    def tearDown(self):  # noqa: D102
        pass

    def test_vanilla_constructor(self):
        """Making a vanilla claim set."""
        claims = ClaimSet()
        self.assertListEqual(claims.claims, [])
        self.assertListEqual(claims.commitment_hashes, [])
        self.assertEqual(claims.sub, None)
        self.assertEqual(claims.signing_key, None)
        self.assertListEqual(claims.commitment_salts, [])
        self.assertEqual(claims.unencrypted, False)

    def test_constructor_with_sig_key(self):
        """Making a claim set with a signing key."""
        claims = ClaimSet(signing_key=self.sig_harry_full)
        self.assertListEqual(claims.claims, [])
        self.assertListEqual(claims.commitment_hashes, [])
        self.assertIsNone(claims.sub)
        self.assertEqual(claims.signing_key.x,
                         JWK_HARRY_FULL_ED25519_DICT['x'])
        self.assertListEqual(claims.commitment_salts, [])
        self.assertEqual(claims.unencrypted, False)

    def test_constructor_with_sub_and_sig_key(self):
        """Making a claim set with a subject and signing key."""
        claims = ClaimSet(sub=HARRY_DID, signing_key=self.sig_harry_full)
        self.assertListEqual(claims.claims, [])
        self.assertListEqual(claims.commitment_hashes, [])
        self.assertEqual(claims.sub, HARRY_DID)
        self.assertListEqual(claims.commitment_salts, [])
        self.assertEqual(claims.unencrypted, False)

    def test_constructor_with_data(self):
        """Making a claim set from serialised/encrypted data."""
        object_key = Jwk.get_instance(
            from_dict=CLAIM_SET_KEYS['object_key'])
        claims = ClaimSet(from_binary=CLAIM_SET, object_key=object_key,
                          signing_key=self.sig_harry_pub)
        self.assertEqual(claims.sub, CLAIM_SET_DICT['sub'])
        self.assertEqual(claims.iat, CLAIM_SET_DICT['iat'])
        self.assertEqual(claims.exp, CLAIM_SET_DICT['exp'])
        no_claims = len(CLAIM_SET_DICT['claims'])
        self.assertEqual(len(claims.claims), no_claims)
        self.assertEqual(len(claims.commitment_hashes), no_claims)
        self.assertEqual(len(claims.commitment_salts), no_claims)
        self.assertEqual(claims.commitment, CLAIM_SET_DICT['commitment'])
        self.assertEqual(claims.unencrypted, False)

    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker())
    def test_make_salty_hash(self, random_mock):
        """Salt a payload and hash it."""
        claims = ClaimSet()
        salt, hash_value = claims.make_salty_hash(b'foo')
        self.assertEqual(random_mock.call_count, 1)
        self.assertEqual(utils.bytes_to_string(salt), 'AAAAAAAAAAAAAAAAAAAAAA')
        self.assertEqual(utils.bytes_to_string(hash_value),
                         'rnSs-yZlHcWKFM5KhxmwlCQ81WQ_TcyjrDtiO6oCHpc')

    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker())
    def test_add_claim_first(self, random_mock):
        """Add a first claim."""
        claims = ClaimSet()
        result = claims.add_claim(HARRY_JSON_LD_CLAIMS[0])
        self.assertEqual(random_mock.call_count, 3)
        self.assertEqual(result.k, bytes(32))
        self.assertEqual(len(claims.claims), 1)
        self.assertListEqual(claims.commitment_salts,
                             [bytes(15) + bytes([2])])
        self.assertEqual(len(claims.commitment_hashes), 1)

        decrypter = Jwe.get_instance(jwk=result)
        decrypter.load_compact(claims.claims[0])
        recovered = decrypter.decrypt()
        self.assertDictEqual(recovered, HARRY_JSON_LD_CLAIMS[0])

    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker())
    def test_add_claim_first_unencrypted(self, random_mock):
        """Add a first claim without encryption."""
        claims = ClaimSet(unencrypted=True)
        result = claims.add_claim(DEVICE_CLAIMS[0])
        self.assertEqual(random_mock.call_count, 1)
        self.assertIsNone(result)
        self.assertListEqual(claims.claims, DEVICE_CLAIM_SET_DICT['claims'])
        self.assertListEqual(claims.commitment_salts, [bytes(16)])

        decrypter = Jwe.get_instance(from_compact=claims.claims[0])
        decrypter.load_compact(claims.claims[0])
        recovered = decrypter.decrypt(allow_unsecured=True)
        self.assertDictEqual(recovered, DEVICE_CLAIMS[0])

    def test_add_claim_all(self):
        """Add all claims to the set."""
        claims = ClaimSet()
        for claim in HARRY_JSON_LD_CLAIMS:
            result = claims.add_claim(claim)
            self.assertEqual(len(result.k), 32)

        no_claims = len(HARRY_JSON_LD_CLAIMS)
        self.assertEqual(len(claims.claims), no_claims)
        self.assertEqual(len(claims.commitment_salts), no_claims)
        self.assertEqual(len(claims.commitment_hashes), no_claims)

    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker(increment=167))
    def test_get_commitment(self, _):
        """Computing the claim set commitment."""
        claims = ClaimSet(sub=HARRY_DID, signing_key=self.sig_harry_full)
        claims_keys = ClaimSetKeys()
        claims_keys.claim_set = claims
        for claim in HARRY_JSON_LD_CLAIMS:
            claims_keys.add_claim(claim)
        result = claims.get_commitment()
        elements = result.split('.')
        self.assertEqual(len(elements), 3)
        self.assertEqual(len(elements[0]), 23)
        self.assertEqual(len(elements[2]), 86)
        commitment_content = utils.base64_to_dict(result.split('.')[1])
        self.assertEqual(commitment_content['role'], 'subject')
        self.assertEqual(commitment_content['sub'], HARRY_DID)
        self.assertEqual(commitment_content['iss'], HARRY_DID)
        commitment_parts = commitment_content['commitment'].split('.')
        self.assertEqual(len(commitment_parts), 2 * len(HARRY_JSON_LD_CLAIMS))

    def test_get_commitment_alternate_key(self):
        """Computing the claim set commitment with alternate signing key."""
        claims = ClaimSet(sub=HARRY_DID, signing_key=self.sig_harry_full)
        claims_keys = ClaimSetKeys()
        claims_keys.claim_set = claims
        for claim in HARRY_JSON_LD_CLAIMS:
            claims_keys.add_claim(claim)
        result = claims.get_commitment(iss=ALBUS_DID,
                                       iss_key=self.sig_albus_full)
        commitment_content = utils.base64_to_dict(result.split('.')[1])
        self.assertEqual(commitment_content['role'], 'attester')
        self.assertEqual(commitment_content['sub'], HARRY_DID)
        self.assertEqual(commitment_content['iss'], ALBUS_DID)

    def test_get_commitment_no_key(self):
        """Computing the claim set commitment without a signing key."""
        claims = ClaimSet(sub=HARRY_DID)
        claims_keys = ClaimSetKeys()
        claims_keys.claim_set = claims
        for claim in HARRY_JSON_LD_CLAIMS:
            claims_keys.add_claim(claim)
        self.assertRaises(RuntimeError, claims.get_commitment)

    def test_get_commitment_pub_key_only(self):
        """Computing the claim set commitment without a private signing key."""
        claims = ClaimSet(sub=HARRY_DID, signing_key=self.sig_harry_pub)
        claims_keys = ClaimSetKeys()
        claims_keys.claim_set = claims
        for claim in HARRY_JSON_LD_CLAIMS:
            claims_keys.add_claim(claim)
        result = claims.get_commitment()
        self.assertIs(result, None)

    @mock.patch('time.time', autospec=True, return_value=1537086642.867628)
    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker(increment=167))
    @mock.patch('pykauriid.claims.ClaimSet.serialise', autospec=True,
                return_value=('secret', 'mumble jumble'))
    def test_finalise(self, serialise_mock, *_):
        """Finalise the claim set (sign & encrypt)."""
        claims = ClaimSet(sub=HARRY_DID, signing_key=self.sig_harry_full)
        claims_keys = ClaimSetKeys()
        claims_keys.claim_set = claims
        for claim in HARRY_JSON_LD_CLAIMS:
            claims_keys.add_claim(claim)
        key, serialised = claims.finalise(include_commitment=True)
        self.assertEqual(serialise_mock.call_count, 1)
        self.assertEqual(key, 'secret')
        self.assertEqual(serialised, 'mumble jumble')
        self.assertEqual(claims.iat, CLAIM_SET_DICT['iat'])
        self.assertEqual(claims.exp, CLAIM_SET_DICT['exp'])
        self.assertEqual(len(claims.commitment),
                         len(CLAIM_SET_DICT['commitment']))

    @mock.patch('time.time', autospec=True, return_value=1537086642.867628)
    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker(increment=167))
    @mock.patch('pykauriid.claims.ClaimSet.serialise', autospec=True,
                return_value=(None, 'mumble jumble'))
    def test_finalise_unencrypted(self, serialise_mock, *_):
        """Finalise the claim set (sign & encrypt)."""
        claims = ClaimSet(sub=HARRY_DID, signing_key=self.sig_harry_full,
                          unencrypted=True)
        for claim in DEVICE_CLAIMS:
            claims.add_claim(claim)
        key, serialised = claims.finalise(include_commitment=True)
        self.assertEqual(serialise_mock.call_count, 1)
        self.assertIsNone(key)
        self.assertEqual(serialised, 'mumble jumble')
        self.assertEqual(claims.iat, DEVICE_CLAIM_SET_DICT['iat'])
        self.assertEqual(claims.exp, DEVICE_CLAIM_SET_DICT['exp'])
        self.assertEqual(claims.commitment,
                         DEVICE_CLAIM_SET_DICT['commitment'])

    @mock.patch('time.time', autospec=True, return_value=1537086642.867628)
    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker(increment=167))
    @mock.patch('pykauriid.claims.ClaimSet.serialise', autospec=True,
                return_value=('secret', 'mumble jumble'))
    def test_finalise_no_commitment(self, serialise_mock, *_):
        """Finalise an encrypted claim set without sub commitment."""
        claims = ClaimSet(sub=HARRY_DID, signing_key=self.sig_harry_full)
        claims_keys = ClaimSetKeys()
        claims_keys.claim_set = claims
        for claim in HARRY_JSON_LD_CLAIMS:
            claims_keys.add_claim(claim)
        key, serialised = claims.finalise(include_commitment=False)
        self.assertEqual(serialise_mock.call_count, 1)
        self.assertEqual(key, 'secret')
        self.assertEqual(serialised, 'mumble jumble')
        self.assertEqual(claims.iat, CLAIM_SET_DICT['iat'])
        self.assertEqual(claims.exp, CLAIM_SET_DICT['exp'])
        self.assertEqual(claims.commitment, None)

    @mock.patch('time.time', autospec=True, return_value=1537086642.867628)
    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker(increment=167))
    def test_serialise(self, *_):
        """Serialise the claim set and encrypt it."""
        claims = ClaimSet(sub=HARRY_DID, signing_key=self.sig_harry_full)
        claims_keys = ClaimSetKeys()
        claims_keys.claim_set = claims
        for claim in HARRY_JSON_LD_CLAIMS:
            claims_keys.add_claim(claim)
        claims.iat = CLAIM_SET_DICT['iat']
        claims.exp = CLAIM_SET_DICT['exp']
        claims.commitment = CLAIM_SET_DICT['commitment']
        key, serialised = claims.serialise()
        self.assertEqual(key.alg, Jwe.DEFAULT_ENC)
        self.assertEqual(len(key.k), 32)
        elements = serialised.split('.')
        self.assertEqual(len(elements), 5)
        self.assertEqual(len(elements[0]), 35 if key.alg == 'C20P' else 39)
        self.assertEqual(len(elements[1]), 0)
        self.assertEqual(len(elements[2]), 16)
        self.assertEqual(len(elements[3]), 4122 if key.alg == 'C20P' else 4170)
        self.assertEqual(len(elements[4]), 22)

    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker(increment=167))
    @mock.patch('sspyjose.Jose.DEFAULT_ENC', 'A256GCM')
    def test_serialise_aes256gcm(self, *_):
        """Serialise the claim set and encrypt it using AES256-GCM."""
        claims = ClaimSet(sub=HARRY_DID, signing_key=self.sig_harry_full)
        claims_keys = ClaimSetKeys()
        claims_keys.claim_set = claims
        for claim in HARRY_JSON_LD_CLAIMS:
            claims_keys.add_claim(claim)
        claims.iat = CLAIM_SET_DICT['iat']
        claims.exp = CLAIM_SET_DICT['exp']
        claims.commitment = CLAIM_SET_DICT['commitment']
        key, serialised = claims.serialise()
        self.assertEqual(key.alg, 'A256GCM')
        self.assertEqual(len(key.k), 32)
        elements = serialised.split('.')
        self.assertEqual(len(elements), 5)
        self.assertEqual(utils.base64_to_dict(elements[0])['enc'], 'A256GCM')

    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker(increment=167))
    @mock.patch('sspyjose.Jose.DEFAULT_ENC', 'A256GCM')
    def test_serialise_unencrypted(self, *_):
        """Serialise the claim set and get it as an unencrypted JWE."""
        claims = ClaimSet(sub=HARRY_DID, signing_key=self.sig_harry_full,
                          unencrypted=True)
        for claim in DEVICE_CLAIMS:
            claims.add_claim(claim)
        claims.iat = DEVICE_CLAIM_SET_DICT['iat']
        claims.exp = DEVICE_CLAIM_SET_DICT['exp']
        claims.commitment = DEVICE_CLAIM_SET_DICT['commitment']
        key, serialised = claims.serialise()
        self.assertIsNone(key)
        self.assertEqual(serialised, DEVICE_CLAIM_SET)
        elements = serialised.split('.')
        self.assertEqual(len(elements), 5)
        self.assertEqual(utils.base64_to_dict(elements[0])['alg'], 'unsecured')

    def test_load(self):
        """Successful load a serialised claim set."""
        claims = ClaimSet(signing_key=self.sig_harry_pub)
        object_key = Jwk.get_instance(from_dict=CLAIM_SET_KEYS['object_key'])
        claims.load(CLAIM_SET, object_key, signing_key=self.sig_harry_pub)
        no_claims = len(CLAIM_SET_DICT['claims'])
        self.assertEqual(len(claims.claims), no_claims)
        self.assertEqual(len(claims.commitment_hashes), no_claims)
        self.assertEqual(len(claims.commitment_salts), no_claims)
        self.assertEqual(claims.sub, CLAIM_SET_DICT['sub'])
        self.assertEqual(claims.commitment, CLAIM_SET_DICT['commitment'])
        self.assertEqual(claims.iat, CLAIM_SET_DICT['iat'])
        self.assertEqual(claims.exp, CLAIM_SET_DICT['exp'])

    def test_load_aes256gcm(self):
        """Successful load a serialised claim set encrypted with AES256-GCM."""
        claims = ClaimSet(signing_key=self.sig_harry_pub)
        object_key = Jwk.get_instance(
            alg='A256GCM',
            from_dict=CLAIM_SET_KEY_DICT_AES256GCM['object_key'])
        claims.load(CLAIM_SET_AES256GCM, object_key)
        no_claims = len(CLAIM_SET_DICT['claims'])
        self.assertEqual(len(claims.claims), no_claims)
        self.assertEqual(len(claims.commitment_hashes), no_claims)
        self.assertEqual(len(claims.commitment_salts), no_claims)
        self.assertEqual(claims.sub, CLAIM_SET_DICT['sub'])
        self.assertEqual(claims.commitment, CLAIM_SET_DICT['commitment'])
        self.assertEqual(claims.iat, CLAIM_SET_DICT['iat'])
        self.assertEqual(claims.exp, CLAIM_SET_DICT['exp'])

    def test_load_unencrypted(self):
        """Successful load an unencrypted serialised claim set."""
        claims = ClaimSet(signing_key=self.sig_harry_pub)
        claims.load(DEVICE_CLAIM_SET, object_key=None, allow_unencrypted=True)
        no_claims = len(DEVICE_CLAIM_SET_DICT['claims'])
        self.assertTrue(claims.unencrypted)
        self.assertListEqual(claims.claims, DEVICE_CLAIM_SET_DICT['claims'])
        self.assertEqual(len(claims.commitment_hashes), no_claims)
        self.assertEqual(len(claims.commitment_salts), no_claims)
        self.assertEqual(claims.sub, DEVICE_CLAIM_SET_DICT['sub'])
        self.assertEqual(claims.commitment,
                         DEVICE_CLAIM_SET_DICT['commitment'])
        self.assertEqual(claims.iat, DEVICE_CLAIM_SET_DICT['iat'])
        self.assertEqual(claims.exp, DEVICE_CLAIM_SET_DICT['exp'])

    def test_load_no_commitment(self):
        """Successful load serialised claim set without subject commitment."""
        claims = ClaimSet(signing_key=self.sig_harry_pub)
        object_key = Jwk.get_instance(
            from_dict=CLAIM_SET_KEYS_UNCOMMITTED['object_key'])
        claims.load(CLAIM_SET_UNCOMMITTED, object_key)
        no_claims = len(CLAIM_SET_DICT_UNCOMMITTED['claims'])
        self.assertEqual(len(claims.claims), no_claims)
        self.assertEqual(claims.commitment, None)
        self.assertListEqual(claims.commitment_hashes, [])
        self.assertListEqual(claims.commitment_salts, [])
        self.assertEqual(claims.sub, CLAIM_SET_DICT_UNCOMMITTED['sub'])
        self.assertEqual(claims.iat, CLAIM_SET_DICT_UNCOMMITTED['iat'])
        self.assertEqual(claims.exp, CLAIM_SET_DICT_UNCOMMITTED['exp'])

    @mock.patch('pykauriid.claims.Jwe', autospec=True)
    def test_load_subject_bad_signature(self, jwe_mock):
        """Load a serialised claim set with a bad signature."""
        jwe_object = mock.create_autospec(Jwe)
        jwe_mock.get_instance.return_value = jwe_object
        altered_commitment = _alter_byte(CLAIM_SET_DICT['commitment'], 1050,
                                         replacement=b'X')
        bad_data = copy.deepcopy(CLAIM_SET_DICT)
        bad_data['commitment'] = altered_commitment
        jwe_object.decrypt.return_value = bad_data
        claims = ClaimSet(signing_key=self.sig_harry_pub)
        object_key = Jwk.get_instance(from_dict=CLAIM_SET_KEYS['object_key'])
        self.assertRaises(nacl.exceptions.BadSignatureError,
                          claims.load, CLAIM_SET, object_key)

    @mock.patch('pykauriid.claims.ClaimSet.validate_commitment',
                autospec=True, side_effect=RuntimeError(
                    'Expected signature of commitment does not match claim'
                    ' set owner.'))
    def test_load_commitment_signer_mismatch(self, validate_commitment_mock):
        """Load a serialised claim set with a signer key mismatch."""
        claims = ClaimSet()
        claims.claims = CLAIM_SET_DICT['claims']
        claims.sub = CLAIM_SET_DICT['sub']
        object_key = Jwk.get_instance(from_dict=CLAIM_SET_KEYS['object_key'])
        self.assertRaises(RuntimeError, claims.load, CLAIM_SET, object_key)
        self.assertEqual(validate_commitment_mock.call_count, 1)

    def test_unpack_commitment_elements_incompatible_number_elements(self):
        """Load a serialised claim set with incompatible number of elements."""
        commitment = CLAIM_SET_DICT['commitment']
        bad_data = utils.base64_to_dict(commitment.split('.')[1])
        commitment_parts = bad_data['commitment'].split('.')
        bad_data['commitment'] = '.'.join(commitment_parts[2:])
        claims = ClaimSet()
        claims.claims = CLAIM_SET_DICT['claims']
        claims.sub = CLAIM_SET_DICT['sub']
        self.assertRaises(RuntimeError, claims.unpack_commitment_elements,
                          bad_data)

    def test_access_claim(self):
        """Access a claim from a serialised claim set."""
        object_key = Jwk.get_instance(from_dict=CLAIM_SET_KEYS['object_key'])
        claims = ClaimSet(from_binary=CLAIM_SET, object_key=object_key,
                          signing_key=self.sig_harry_pub)
        claim_key = Jwk.get_instance(
            from_dict=CLAIM_SET_KEYS['claim_keys']['1'])
        result = claims.access_claim(1, claim_key)
        self.assertDictEqual(result, HARRY_JSON_LD_CLAIMS[7])

    def test_access_claim_unencrypted(self):
        """Access an unencrypted claim from a serialised claim set."""
        claims = ClaimSet(from_binary=DEVICE_CLAIM_SET, object_key=None,
                          signing_key=self.sig_harry_pub, unencrypted=True)
        result = claims.access_claim(0, claim_key=None)
        self.assertDictEqual(result, DEVICE_CLAIMS[0])

    def test_access_claim_hash_mismatch(self):
        """Access a claim from a serialised claim set, hash mismatch."""
        object_key = Jwk.get_instance(from_dict=CLAIM_SET_KEYS['object_key'])
        claims = ClaimSet(from_binary=CLAIM_SET, object_key=object_key,
                          signing_key=self.sig_harry_pub)
        claim_key = Jwk.get_instance(
            from_dict=CLAIM_SET_KEYS['claim_keys']['1'])
        claims.commitment_hashes[1] = b'booboo'
        self.assertRaises(RuntimeError, claims.access_claim, 1, claim_key)

    def test_access_claim_no_commitment(self):
        """Access a claim from a serialised claim set without a commitment."""
        object_key = Jwk.get_instance(
            from_dict=CLAIM_SET_KEYS_UNCOMMITTED['object_key'])
        claims = ClaimSet(from_binary=CLAIM_SET_UNCOMMITTED,
                          object_key=object_key)
        claim_key = Jwk.get_instance(
            from_dict=CLAIM_SET_KEYS_UNCOMMITTED['claim_keys']['1'])
        result = claims.access_claim(1, claim_key)
        self.assertDictEqual(result, HARRY_JSON_LD_CLAIMS[7])

    def test_access_claim_raw_bytes(self):
        """Access a claim from a serialised claim set in binary form."""
        object_key = Jwk.get_instance(from_dict=CLAIM_SET_KEYS['object_key'])
        claims = ClaimSet(from_binary=CLAIM_SET, object_key=object_key,
                          signing_key=self.sig_harry_pub)
        claim_key = Jwk.get_instance(
            from_dict=CLAIM_SET_KEYS['claim_keys']['1'])
        result = claims.access_claim(1, claim_key, raw_bytes=True)
        self.assertIsInstance(result, bytes)
        self.assertDictEqual(json.loads(result), HARRY_JSON_LD_CLAIMS[7])


class ClaimSetKeysTest(unittest.TestCase):
    """Testing the ClaimSetKeys class functionality."""

    def setUp(self):  # noqa: D102
        self.claims = ClaimSet(sub=HARRY_DID)
        self.claims_keys = ClaimSetKeys()
        self.sig_harry_full = Jwk.get_instance(
            crv='Ed25519', from_json=JWK_HARRY_FULL_ED25519)
        self.sig_harry_pub = Jwk.get_instance(
            crv='Ed25519', from_json=JWK_HARRY_PUB_ED25519)

    def tearDown(self):  # noqa: D102
        pass

    def test_vanilla_constructor(self):
        """Making a vanilla claim set keys set."""
        claims_keys = ClaimSetKeys()
        self.assertEqual(claims_keys.claim_set, None)
        self.assertListEqual(claims_keys.claim_keys, [])
        self.assertEqual(claims_keys.object_key, None)
        self.assertEqual(claims_keys.trace_key, None)

    @mock.patch('pykauriid.claims.ClaimSetKeys.load', autospec=True)
    def test_constructor_from_json(self, load_mock):
        """Making claim set keys from JSON data."""
        ClaimSetKeys(data=json.dumps(CLAIM_SET_KEYS),
                     signing_key=self.sig_harry_pub)
        self.assertEqual(load_mock.call_count, 1)

    @mock.patch('pykauriid.claims.ClaimSetKeys.load', autospec=True)
    def test_constructor_from_dict(self, load_mock):
        """Making claim set keys from dictionary data."""
        ClaimSetKeys(data=CLAIM_SET_KEYS, signing_key=self.sig_harry_pub)
        self.assertEqual(load_mock.call_count, 1)

    @mock.patch('pykauriid.claims.ClaimSet.add_claim',
                autospec=True, return_value=b'foo')
    def test_add_claim(self, add_claim_mock):
        """Add claim and store key."""
        claims_keys = ClaimSetKeys()
        claims_keys.claim_set = self.claims
        claims_keys.add_claim(HARRY_JSON_LD_CLAIMS[0])
        self.assertEqual(add_claim_mock.call_count, 1)
        self.assertEqual(claims_keys.claim_keys[0], b'foo')

    @mock.patch('pykauriid.claims.ClaimSet.finalise',
                autospec=True, return_value=(b'key', b'serialised'))
    def test_finalise_claim_set(self, finalise_mock):
        """Add claim and store key."""
        claims_keys = ClaimSetKeys()
        claims_keys.claim_set = self.claims
        for claim in HARRY_JSON_LD_CLAIMS:
            claims_keys.add_claim(claim)
        result = claims_keys.finalise_claim_set()
        self.assertEqual(finalise_mock.call_count, 1)
        self.assertEqual(result, b'serialised')
        self.assertEqual(claims_keys.object_key, b'key')

    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker())
    def test_to_dict(self, _):
        """Convert the claim set keys object to a dict."""
        claims_keys = ClaimSetKeys()
        self.claims.signing_key = self.sig_harry_full
        claims_keys.claim_set = self.claims
        for claim in HARRY_JSON_LD_CLAIMS:
            claims_keys.add_claim(claim)
        claims_keys.attestation_id = '08/15'
        result = claims_keys.to_dict()
        self.assert_(len(result['claim_set']) > 2000)
        self.assertEqual(len(result['object_key']['k']), 43)
        no_claims = len(HARRY_JSON_LD_CLAIMS)
        self.assertEqual(len(result['claim_keys']), no_claims)
        self.assertNotIn('claim_type_hints', result)
        self.assertEqual(len(result['claim_keys']['0']['k']), 43)
        self.assertEqual(result['trace_key'], None)
        self.assertEqual(claims_keys.encrypted_claim_set, result['claim_set'])
        self.assertEqual(result['attestation_id'], '08/15')
        # With added trace key.
        claims_keys.trace_key = Jwk.get_instance(alg='C20P', generate=True)
        result = claims_keys.to_dict()
        self.assertEqual(len(result['trace_key']['k']), 43)

    @mock.patch('time.time', autospec=True, return_value=1537086642.867628)
    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker(increment=167))
    def test_to_dict_with_type_hints(self, *_):
        """Convert claim set keys object to a dict with claim type hints."""
        claims_keys = ClaimSetKeys()
        self.claims.signing_key = self.sig_harry_full
        claims_keys.claim_set = self.claims
        for claim in HARRY_JSON_LD_CLAIMS:
            claims_keys.add_claim(claim)
        result = claims_keys.to_dict(claim_type_hints=True)
        self.assert_(len(result['claim_set']) > 2000)
        self.assertEqual(len(result['object_key']['k']), 43)
        no_claims = len(HARRY_JSON_LD_CLAIMS)
        self.assertEqual(len(result['claim_keys']), no_claims)
        self.assertEqual(len(result['claim_type_hints']), no_claims)
        self.assertEqual(result['claim_type_hints']['1'], 'Person.givenName')
        self.assertEqual(len(result['claim_keys']['0']['k']), 43)
        self.assertEqual(result['trace_key'], None)
        self.assertEqual(claims_keys.encrypted_claim_set, result['claim_set'])

    @mock.patch('pykauriid.claims.ClaimSetKeys.to_dict', autospec=True,
                return_value={'answer': 42})
    def test_serialise(self, _):
        """Serialise the claim set keys object to JSON."""
        claims_keys = ClaimSetKeys()
        self.assertEqual(claims_keys.serialise(), '{"answer":42}')

    def test_load_self_claim_set_keys(self):
        """Load the (self) claim set keys object from JSON."""
        case_0 = REFERENCE_DATA['cases']['0']
        step0_output = case_0['steps'][0]['output']
        claim_set_keys_dict = step0_output['claim_set_keys']
        harry_claims = case_0['data']['harry_claims']
        claim_set_check = step0_output['claim_set_keys']['claim_set']
        claims_keys = ClaimSetKeys()
        claims_keys.load(json.dumps(claim_set_keys_dict), self.sig_harry_pub)
        self.assertEqual(
            claims_keys.object_key.k,
            utils.string_to_bytes(claim_set_keys_dict['object_key']['k']))
        no_claims = len(harry_claims)
        self.assertEqual(len(claims_keys.claim_set.claims),
                         len(claims_keys.claim_keys))
        self.assertEqual(len(claims_keys.claim_set.commitment_salts),
                         len(claims_keys.claim_keys))
        self.assertEqual(len(claims_keys.claim_set.commitment_hashes),
                         len(claims_keys.claim_keys))
        for i, claim_key in zip(range(no_claims), claims_keys.claim_keys):
            claims_keys.claim_set.access_claim(i, claim_key)
            self.assertEqual(
                utils.bytes_to_string(claim_key.k),
                claim_set_keys_dict['claim_keys'][str(i)]['k'])
        self.assertEqual(claims_keys.encrypted_claim_set, claim_set_check)
        self.assertEqual(
            claims_keys.object_key.k,
            utils.string_to_bytes(claim_set_keys_dict['object_key']['k']))
        self.assertDictEqual(
            {str(i): value
             for i, value in enumerate(claims_keys.claim_type_hints)},
            claim_set_keys_dict['claim_type_hints'])
        self.assertEqual(claims_keys.trace_key,
                         claim_set_keys_dict['trace_key'])

    def test_load_attestation_claim_set_keys(self):
        """Load the (attestation's) claim set keys object from JSON."""
        case_0 = REFERENCE_DATA['cases']['0']
        step1_output = case_0['steps'][1]['output']
        claim_set_keys_dict = step1_output['claim_set_keys']
        harry_claims = case_0['data']['harry_claims']
        claim_set_check = step1_output['claim_set_keys']['claim_set']
        claims_keys = ClaimSetKeys()
        claims_keys.load(json.dumps(claim_set_keys_dict), self.sig_harry_pub)
        self.assertEqual(
            claims_keys.object_key.k,
            utils.string_to_bytes(claim_set_keys_dict['object_key']['k']))
        no_claims = len(harry_claims)
        self.assertEqual(len(claims_keys.claim_set.claims),
                         len(claims_keys.claim_keys))
        self.assertListEqual(claims_keys.claim_set.commitment_salts, [])
        self.assertListEqual(claims_keys.claim_set.commitment_hashes, [])
        for i, claim_key in zip(range(no_claims), claims_keys.claim_keys):
            claims_keys.claim_set.access_claim(i, claim_key)
            self.assertEqual(
                utils.bytes_to_string(claim_key.k),
                claim_set_keys_dict['claim_keys'][str(i)]['k'])
        self.assertEqual(claims_keys.encrypted_claim_set, claim_set_check)
        self.assertEqual(
            claims_keys.object_key.k,
            utils.string_to_bytes(claim_set_keys_dict['object_key']['k']))
        self.assertDictEqual(
            {str(i): value
             for i, value in enumerate(claims_keys.claim_type_hints)},
            claim_set_keys_dict['claim_type_hints'])
        self.assertDictEqual(claims_keys.trace_key.to_dict(),
                             claim_set_keys_dict['trace_key'])

    def test_load_dict_with_trace_key(self):
        """Load the claim set keys object from dict with trace key."""
        claims_keys = ClaimSetKeys()
        claims_keys.load(CLAIM_SET_KEYS_UNCOMMITTED, self.sig_harry_pub)
        self.assertDictEqual(claims_keys.object_key.to_dict(),
                             CLAIM_SET_KEYS_UNCOMMITTED['object_key'])
        self.assertDictEqual(claims_keys.trace_key.to_dict(),
                             CLAIM_SET_KEYS_UNCOMMITTED['trace_key'])
        no_claims = len(HARRY_JSON_LD_CLAIMS)
        for i, claim, claim_key in zip(range(no_claims),
                                       claims_keys.claim_set.claims,
                                       claims_keys.claim_keys):
            self.assertIn(claim, CLAIM_SET_DICT_UNCOMMITTED['claims'])
            self.assertEqual(
                claim_key.k,
                utils.string_to_bytes(
                    CLAIM_SET_KEYS_UNCOMMITTED['claim_keys'][str(i)]['k']))
        self.assertEqual(claims_keys.encrypted_claim_set,
                         CLAIM_SET_UNCOMMITTED)
        self.assertDictEqual(
            {str(i): value
             for i, value in enumerate(claims_keys.claim_type_hints)},
            CLAIM_SET_KEYS_UNCOMMITTED['claim_type_hints'])


class ClaimSetSanityTest(unittest.TestCase):
    """Sanity testing of the claims module."""

    def setUp(self):  # noqa: D102
        self.sig_harry_full = Jwk.get_instance(
            crv='Ed25519', from_json=JWK_HARRY_FULL_ED25519)
        self.sig_harry_pub = Jwk.get_instance(
            crv='Ed25519', from_json=JWK_HARRY_PUB_ED25519)

    def tearDown(self):  # noqa: D102
        pass

    def test_round_trip(self):
        """Round trip of an entire claim set serialisation and access."""
        # Make the original serialised claim set and claim set keys objects.
        claims = ClaimSet(sub=HARRY_DID, signing_key=self.sig_harry_full)
        claims_keys = ClaimSetKeys()
        claims_keys.claim_set = claims
        for claim in HARRY_JSON_LD_CLAIMS:
            claims_keys.add_claim(claim)
        serialised_claim_set_keys = claims_keys.serialise()
        serialised_claim_set = claims_keys.encrypted_claim_set

        # Receiver unpicking and accessing the claim set and keys.
        receiver_claim_set_keys = ClaimSetKeys(data=serialised_claim_set_keys,
                                               signing_key=self.sig_harry_pub)
        object_key = receiver_claim_set_keys.object_key
        receiver_claims = ClaimSet(from_binary=serialised_claim_set,
                                   object_key=object_key,
                                   signing_key=self.sig_harry_pub)
        for i in range(len(receiver_claims.claims)):
            claim_key = receiver_claim_set_keys.claim_keys[i]
            receiver_claims.access_claim(i, claim_key)

    def test_load_serialise_round_trip(self):
        """Serialise the claim set keys object to JSON."""
        claims_keys = ClaimSetKeys(data=CLAIM_SET_KEYS,
                                   signing_key=self.sig_harry_pub)
        and_back = claims_keys.serialise(claim_type_hints=True)
        reference = copy.deepcopy(CLAIM_SET_KEYS)
        del reference['claim_set']
        and_back = json.loads(and_back)
        and_back_cs = and_back['claim_set']
        del and_back['claim_set']
        self.assertDictEqual(and_back, reference)
        self.assertEqual(len(and_back_cs), len(CLAIM_SET_KEYS['claim_set']))


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

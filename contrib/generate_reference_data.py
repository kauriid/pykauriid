#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Simple script to ease the generation of a reference data JSON file.
"""

# Created: 2018-11-07 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

import json
import logging
import os
import subprocess
import tempfile


ROOT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
KAURIID = os.path.join(ROOT_PATH, 'pykauriid', 'scripts', 'kauriid.py')
EXAMPLE_PATH = os.path.join(ROOT_PATH, 'examples')
REFERENCE_FILE = os.path.join(ROOT_PATH, 'tests', 'reference_data.json')


class Case:
    """Base case."""

    case_data = None
    envelope = None
    tempdir = None
    CASE_NO = None
    _ENVELOPE_ELEMENTS = ['__reference_data_version__',
                          'cases']
    _JSON_EXAMPLE_DATA = ['harry_claims',
                          'attester_data',
                          'attestation_data']

    def __init__(self, data: dict, tempdir: str):
        """Initialise the reference case."""
        self.envelope = {key: data[key] for key in self._ENVELOPE_ELEMENTS}
        self.tempdir = tempdir
        self.case_data = data['cases'][self.CASE_NO]
        self.case_data['description'] = self.__doc__
        logging.info('Initialising case {}: {}'
                     .format(self.CASE_NO, self.case_data['description']))
        self.make_example_files()

    def make_example_files(self):
        """Make test files in the examples/ directory."""
        logging.info('Placing data files in examples/ directory.')
        for name, jwk in self.case_data['data']['jwks'].items():
            with open(os.path.join(EXAMPLE_PATH,
                                   '{}.jwk'.format(name)), 'wt') as fd:
                json.dump(jwk, fd, indent=4)
        for name in self._JSON_EXAMPLE_DATA:
            with open(os.path.join(EXAMPLE_PATH,
                                   '{}.json'.format(name)), 'wt') as fd:
                json.dump(self.case_data['data'][name], fd, indent=4)

    def add_case_result(self, data: dict = None):
        """Add result data set for this case."""
        if not data:
            data = self.envelope.copy()
        data['cases'][self.CASE_NO] = self.case_data
        return data

    def _add_step_data(self, step: int, step_data: dict):
        """Add step result to case data."""
        if len(self.case_data['steps']) > step:
            self.case_data['steps'][step] = step_data
        else:
            self.case_data['steps'].append(step_data)


class Case0(Case):
    """Attestation flow of a self claim set, encrypting with AES256-GCM."""

    CASE_NO = '0'

    def run(self):
        """Run the sequence of steps for this case."""
        self.step0()
        self.step1()

    def step0(self):
        """Create a new self claim set and claim set keys object."""
        step = 0
        description = self.step0.__doc__
        logging.info('Running case {}, step {}: {}'
                     .format(self.CASE_NO, step, description))
        input_data = ['data/harry_did',
                      'data/jwks/harry_sig_full',
                      'data/harry_claims']
        # Execute the command for this step with the required input data.
        claim_set_keys_file = os.path.join(self.tempdir,
                                           '_claim_set_keys.json')
        command = [
            KAURIID, 'claims', '--operation', 'new',
            '--subject', self.case_data['data']['harry_did'],
            '--subject_sig_key', os.path.join(EXAMPLE_PATH,
                                              'harry_sig_full.jwk'),
            '--claims', os.path.join(EXAMPLE_PATH,
                                     'harry_claims.json'),
            '--claimset', os.path.join(self.tempdir,
                                       '_claim_set_harry.jwe'),
            '--claimsetkeys', claim_set_keys_file,
        ]
        subprocess.run(command, check=True)
        # Extract the output for the reference.
        output = {}
        output['claim_set_keys'] = json.load(open(claim_set_keys_file, 'rt'))
        # Record this step.
        step_data = {
            'step': step,
            'description': description,
            'input': input_data,
            'output': output
        }
        self._add_step_data(step, step_data)

    def step1(self):
        """Attest a self claim set object."""
        step = 1
        description = self.step1.__doc__
        logging.info('Running case {}, step {}: {}'
                     .format(self.CASE_NO, step, description))
        input_data = ['data/jwks/harry_sig_pub',
                      'data/jwks/dumbledore_sig_full',
                      'data/attester_data',
                      'data/attestation_data',
                      'output/0/claim_set_keys']
        # Execute the command for this step with the required input data.
        claim_set_keys_file = os.path.join(self.tempdir,
                                           '_claim_set_keys.json')
        attestation_file = os.path.join(self.tempdir, '_attestation.jwe')
        command = [
            KAURIID, 'attestations', '--operation', 'attest',
            '--attester_sig_key', os.path.join(EXAMPLE_PATH,
                                               'dumbledore_sig_full.jwk'),
            '--subject_sig_key', os.path.join(EXAMPLE_PATH,
                                              'harry_sig_pub.jwk'),
            '--attester_data', os.path.join(EXAMPLE_PATH,
                                            'attester_data.json'),
            '--attestation_data', os.path.join(EXAMPLE_PATH,
                                               'attestation_data.json'),
            '--attestation', attestation_file,
            '--claimsetkeys', claim_set_keys_file,
        ]
        subprocess.run(command, check=True)
        # Extract the output for the reference.
        output = {}
        output['claim_set_keys'] = json.load(open(claim_set_keys_file, 'rt'))
        output['attestation'] = open(attestation_file, 'rt').read()
        # Record this step.
        step_data = {
            'step': step,
            'description': description,
            'input': input_data,
            'output': output
        }
        self._add_step_data(step, step_data)


class Case1(Case):
    """Attestation flow for a foreign claim set, encrypting with AES256-GCM."""

    CASE_NO = '1'

    def run(self):
        """Run the sequence of steps for this case."""
        self.step0()
        self.step1()
        self.step2()

    def step0(self):
        """Create a new foreign claim set and claim set keys object."""
        step = 0
        description = self.step0.__doc__
        logging.info('Running case {}, step {}: {}'
                     .format(self.CASE_NO, step, description))
        input_data = ['data/harry_did',
                      'data/harry_claims']
        # Execute the command for this step with the required input data.
        claim_set_keys_file = os.path.join(self.tempdir,
                                           '_claim_set_keys.json')
        command = [
            KAURIID, 'claims', '--operation', 'new',
            '--subject', self.case_data['data']['harry_did'],
            '--claims', os.path.join(EXAMPLE_PATH,
                                     'harry_claims.json'),
            '--claimset', os.path.join(self.tempdir,
                                       '_claim_set_harry.jwe'),
            '--claimsetkeys', claim_set_keys_file,
        ]
        subprocess.run(command, check=True)
        # Extract the output for the reference.
        output = {}
        output['claim_set_keys'] = json.load(open(claim_set_keys_file, 'rt'))
        # Record this step.
        step_data = {
            'step': step,
            'description': description,
            'input': input_data,
            'output': output
        }
        self._add_step_data(step, step_data)

    def step1(self):
        """Attest a foreign claim set object."""
        step = 1
        description = self.step1.__doc__
        logging.info('Running case {}, step {}: {}'
                     .format(self.CASE_NO, step, description))
        input_data = ['data/jwks/dumbledore_sig_full',
                      'data/attester_data',
                      'data/attestation_data',
                      'output/0/claim_set_keys']
        # Execute the command for this step with the required input data.
        claim_set_keys_file = os.path.join(self.tempdir,
                                           '_claim_set_keys.json')
        attestation_file = os.path.join(self.tempdir, '_attestation.jwe')
        command = [
            KAURIID, 'attestations', '--operation', 'attest',
            '--attester_sig_key', os.path.join(EXAMPLE_PATH,
                                               'dumbledore_sig_full.jwk'),
            '--attester_data', os.path.join(EXAMPLE_PATH,
                                            'attester_data.json'),
            '--attestation_data', os.path.join(EXAMPLE_PATH,
                                               'attestation_data.json'),
            '--attestation', attestation_file,
            '--claimsetkeys', claim_set_keys_file,
        ]
        subprocess.run(command, check=True)
        # Extract the output for the reference.
        output = {}
        output['claim_set_keys'] = json.load(open(claim_set_keys_file, 'rt'))
        output['attestation'] = open(attestation_file, 'rt').read()
        # Record this step.
        step_data = {
            'step': step,
            'description': description,
            'input': input_data,
            'output': output
        }
        self._add_step_data(step, step_data)

    def step2(self):
        """Accept (counter-sign) a foreign claim set attestation."""
        step = 2
        description = self.step2.__doc__
        logging.info('Running case {}, step {}: {}'
                     .format(self.CASE_NO, step, description))
        input_data = ['data/jwks/dumbledore_sig_pub',
                      'data/jwks/harry_sig_full',
                      'output/1/claim_set_keys',
                      'output/1/attestation']
        # Execute the command for this step with the required input data.
        claim_set_keys_file = os.path.join(self.tempdir,
                                           '_claim_set_keys.json')
        attestation_file = os.path.join(self.tempdir, '_attestation.jwe')
        command = [
            KAURIID, 'attestations', '--operation', 'accept',
            '--attester_sig_key', os.path.join(EXAMPLE_PATH,
                                               'dumbledore_sig_pub.jwk'),
            '--subject_sig_key', os.path.join(EXAMPLE_PATH,
                                              'harry_sig_full.jwk'),
            '--attestation', attestation_file,
            '--claimsetkeys', claim_set_keys_file,
        ]
        subprocess.run(command, check=True)
        # Extract the output for the reference.
        output = {}
        output['attestation'] = open(attestation_file, 'rt').read()
        # Record this step.
        step_data = {
            'step': step,
            'description': description,
            'input': input_data,
            'output': output
        }
        self._add_step_data(step, step_data)


def main():
    """Execute the generation of reference data."""
    data = json.load(open(REFERENCE_FILE, 'rt'))
    result = None
    with tempfile.TemporaryDirectory() as tmpdirname:
        cases = [Case0(data, tmpdirname),
                 Case1(data, tmpdirname)]
        for case in cases:
            case.run()
            result = case.add_case_result(result)
    logging.info('Saving reference data to file {}'.format(REFERENCE_FILE))
    json.dump(result, open(REFERENCE_FILE, 'wt'), indent=4)
    logging.info('Done.')


if __name__ == '__main__':
    # Set up logger.
    logging.basicConfig(
        level=logging.INFO,
        format='%(levelname)s\t%(name)s\t%(asctime)s %(message)s')

    main()

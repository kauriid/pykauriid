Contributed Artifacts
=====================

`generate_reference_data.py`
----------------------------

Script to consistently generate reference data JSON and example files
file using the `kauriid.py` CLI.

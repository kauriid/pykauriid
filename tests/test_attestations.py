# -*- coding: utf-8 -*-
"""Tests for the attestations module."""

# Created: 2018-09-12 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

# flake8: --ignore E501

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

import copy
import json
from unittest import mock  # @UnusedImport
import unittest

import prov.model
import sspyjose

from pykauriid import utils
from pykauriid.attestations import (make_utc_datetime,
                                    _delegation_exists,
                                    Attestation,
                                    AttesterData,
                                    AttestationData,
                                    AttestationStatement)
from pykauriid.claims import ClaimSet, ClaimSetKeys
from tests.data import (CLAIM_SET,
                        CLAIM_SET_STRIPPED_COMMITMENT,
                        CLAIM_SET_KEYS,
                        CLAIM_SET_TRACE_KEY,
                        DEVICE_CLAIM_SET,
                        DEVICE_CLAIM_SET_STRIPPED_COMMITMENT,
                        DEVICE_ATTESTATION,
                        JWK_HARRY_FULL_ED25519,
                        JWK_HARRY_PUB_ED25519,
                        JWK_ALBUS_FULL_ED25519,
                        HARRY_DID,
                        ADDITIONAL_PROV_NAMESPACES,
                        CURRENT_MANUAL_VERIFICATION,
                        PROVN_DATA,
                        HARRY_JSON_LD_CLAIMS,
                        ANCESTOR_ID,
                        ANCESTOR_OBJECT_KEY,
                        ANCESTOR_TRACE_KEY,
                        ATTESTATION_TRACE_KEY,
                        ATTESTATION,
                        ATTESTATION_ID,
                        CLAIM_SET_COMMITMENT,
                        FOREIGN_CLAIM_SET,
                        FOREIGN_CLAIM_SET_COMMITMENT,
                        FOREIGN_CLAIM_SET_KEYS,
                        FOREIGN_ATTESTATION)
from tests.reference_data import REFERENCE_DATA
from tests.utils import (deep_sort,
                         random_mocker,
                         reset_random_mock_counter)


class ModuleTest(unittest.TestCase):
    """Testing the attestations module functionality."""

    def test_make_utc_datetime(self):
        """Create an ISO compliant UTC time."""
        now = make_utc_datetime()
        self.assert_(now.isoformat().endswith('+00:00')
                     or now.isoformat().endswith('Z'))

    def test_make_utc_datetime_with_ts(self):
        """Create an ISO compliant UTC time with a given iat."""
        ts = make_utc_datetime(1537086648)
        self.assertIn(ts.isoformat(),
                      ('2018-09-16T08:30:48+00:00', '2018-09-16T08:30:48Z'))

    def test_delegation_exists_true(self):
        """Check for an existing delegation in the PROV document."""
        document = prov.model.ProvDocument()
        document.add_namespace('ex', 'https://example.org/prov/')
        foo = document.agent('ex:foo')
        bar = document.agent('ex:bar')
        document.delegation('ex:foo', 'ex:bar')
        self.assertIs(_delegation_exists(document, 'ex:foo', 'ex:bar'), True)
        self.assertIs(_delegation_exists(document, foo, bar), True)

    def test_delegation_exists_false(self):
        """Check for a non-existent delegation in the PROV document."""
        document = prov.model.ProvDocument()
        document.add_namespace('ex', 'https://example.org/prov/')
        document.agent('ex:foo')
        document.agent('ex:bar')
        document.delegation('ex:foo', 'ex:bar')
        tests = [
            ('ex:foo', 'ex:baz'),
            ('ex:bar', 'ex:foo'),
            ('prov:foo', 'ex:bar'),
            ('ex:foo', 'prov:bar')
        ]
        for delegate, responsible in tests:
            self.assertIs(
                _delegation_exists(document, delegate, responsible), False)


class AttestationTest(unittest.TestCase):
    """Testing the Attestation class functionality."""

    def setUp(self):  # noqa: D102
        reset_random_mock_counter()
        self.sig_harry_full = sspyjose.jwk.Jwk.get_instance(
            from_json=JWK_HARRY_FULL_ED25519)
        self.sig_harry_pub = sspyjose.jwk.Jwk.get_instance(
            from_json=JWK_HARRY_PUB_ED25519)
        self.sig_albus_full = sspyjose.jwk.Jwk.get_instance(
            from_json=JWK_ALBUS_FULL_ED25519)
        self.sig_albus_pub = sspyjose.jwk.Jwk.get_instance(
            from_json=JWK_ALBUS_FULL_ED25519)

    def tearDown(self):  # noqa: D102
        pass

    def test_constructor_with_claim_set(self):
        """Constructor with claim set and keys."""
        my_attestation = Attestation(claim_set_keys=CLAIM_SET_KEYS,
                                     subject_signing_key=self.sig_harry_pub)
        self.assertIsInstance(my_attestation.claim_set_keys.claim_set,
                              ClaimSet)
        self.assertEquals(my_attestation.claim_set_keys.encrypted_claim_set,
                          CLAIM_SET)
        self.assertIsInstance(my_attestation.claim_set_keys, ClaimSetKeys)
        self.assertIsNone(my_attestation.attester_data)
        self.assertIsNone(my_attestation.attestation_data)
        self.assertIsInstance(my_attestation._prov_document,
                              prov.model.ProvDocument)
        self.assertIs(my_attestation.unencrypted, False)

    @mock.patch('pykauriid.attestations.Attestation.load', autospec=True)
    def test_constructor_from_binary(self, load_mock):
        """Constructor with claim set, JSON keys and attestation."""
        claim_set_keys = copy.deepcopy(CLAIM_SET_KEYS)
        claim_set_keys['trace_key'] = ATTESTATION_TRACE_KEY
        my_attestation = Attestation(claim_set_keys=claim_set_keys,
                                     from_binary=ATTESTATION,
                                     subject_signing_key=self.sig_harry_pub)
        self.assertEqual(load_mock.call_count, 1)
        self.assertDictEqual(
            my_attestation.claim_set_keys.trace_key.to_dict(),
            ATTESTATION_TRACE_KEY)
        self.assertIs(my_attestation.unencrypted, False)

    def test_constructor_with_claim_set_json_keys(self):
        """Constructor with claim set and JSON keys."""
        my_attestation = Attestation(claim_set_keys=json.dumps(CLAIM_SET_KEYS),
                                     subject_signing_key=self.sig_harry_pub)
        self.assertIsInstance(my_attestation.claim_set_keys, ClaimSetKeys)
        self.assertIs(my_attestation.unencrypted, False)

    @mock.patch('time.time', autospec=True, return_value=1537086642.867628)
    @mock.patch('uuid.uuid4', autospec=True,
                side_effect=('00000000-1111-2222-3333-444444444444',
                             '11111111-2222-3333-4444-555555555555',
                             '22222222-3333-4444-5555-666666666666'))
    def test_add_attestation_prov(self, uuid_mock, time_mock):
        """Adding PROV to the attestation."""
        my_attestation = Attestation(claim_set_keys=CLAIM_SET_KEYS,
                                     subject_signing_key=self.sig_harry_pub)
        ancestors = [{
            'wrapper_uri':
                'kauriid:attestations/ipns/QmTXXXattestation00XXX...Ftu',
            'id': 'kauriid:kauriid:attestations/00000000-...-444444444444',
            'object_key': None,
            'trace_key': None}]
        # Name spaces and actors.
        provenance_namespaces = {
            'aa': ADDITIONAL_PROV_NAMESPACES['aa'],
            'nzta': ADDITIONAL_PROV_NAMESPACES['nzta'],
            'watercare': ADDITIONAL_PROV_NAMESPACES['watercare']}
        attester_data = AttesterData(
            iss='aa:staff/2omXXXbobXXX...WEG',
            delegation_chain=['aa:Organisation'],
            provenance_namespaces=provenance_namespaces)
        my_attestation.attester_data = attester_data
        # PROV trail with evidence and plan.
        evidence_elements = {
            'nzta:dl/DJ034005-v193': 'original document',
            'watercare:customers/4711/invoices/201809.pdf': 'digital scan'}
        my_attestation.attestation_data = AttestationData(
            evidence_elements=evidence_elements,
            evidence_verification=CURRENT_MANUAL_VERIFICATION,
            ancestors=ancestors,
            content=('foo', 'bar'))
        target_id = my_attestation._add_attestation_prov()
        self.assertEqual(time_mock.call_count, 1)
        self.assertEqual(uuid_mock.call_count, 3)
        self.assertEqual(my_attestation.provenance, PROVN_DATA)
        self.assertEqual(
            target_id,
            'kauriid:attestations/00000000-1111-2222-3333-444444444444')

    @mock.patch('time.time', autospec=True, return_value=1537086642.867628)
    @mock.patch('uuid.uuid4', autospec=True,
                return_value='00000000-1111-2222-3333-444444444444')
    def test_add_attestation_prov_stripped(self, *_):
        """Adding PROV to the attestation stripped to the min."""
        my_attestation = Attestation(claim_set_keys=CLAIM_SET_KEYS,
                                     subject_signing_key=self.sig_harry_pub)
        # Name spaces and actors.
        provenance_namespaces = {
            'aa': ADDITIONAL_PROV_NAMESPACES['aa']}
        attester_data = AttesterData(
            iss='aa:staff/2omXXXbobXXX...WEG',
            provenance_namespaces=provenance_namespaces)
        my_attestation.attester_data = attester_data
        my_attestation.attestation_data = AttestationData()
        # PROV trail with evidence and plan.
        my_attestation._add_attestation_prov()

    def test_serialise(self):
        """Serialising an attestation."""
        # Prepare all the preliminaries.
        my_attestation = Attestation(claim_set_keys=CLAIM_SET_KEYS,
                                     subject_signing_key=self.sig_harry_pub)
        object_key = sspyjose.jwk.Jwk.get_instance(
            from_dict=ANCESTOR_OBJECT_KEY)
        my_attestation.claim_set_keys.object_key = object_key
        my_attestation.claim_set_keys.encrypted_claim_set = 'garble'
        my_attestation.commitments = {'attester': 'I do!'}
        my_attestation.content = 'stuff'
        my_attestation.id = '08/15'
        result = my_attestation.serialise()
        decrypter = sspyjose.jwe.Jwe.get_instance(from_compact=result,
                                                  jwk=object_key)
        content = decrypter.decrypt()
        self.assertDictEqual(deep_sort(content),
                             {'claim_set': 'garble',
                              'commitments': {'attester': 'I do!'},
                              'content': 'stuff',
                              'v': Attestation.version,
                              'id': '08/15'})

    def test_serialise_unencrypted(self):
        """Serialising an attestation without encryption."""
        # Prepare all the preliminaries.
        my_attestation = Attestation(subject_signing_key=self.sig_harry_pub,
                                     allow_unencrypted=True)
        claim_set = ClaimSet(
            from_binary=DEVICE_CLAIM_SET, object_key=None,
            signing_key=self.sig_harry_pub, unencrypted=True)
        my_attestation.claim_set_keys.claim_set = claim_set
        my_attestation.claim_set_keys.encrypted_claim_set = 'garble'
        my_attestation.commitments = {'attester': 'I do!'}
        my_attestation.content = 'stuff'
        my_attestation.id = '08/15'
        result = my_attestation.serialise()
        decrypter = sspyjose.jwe.Jwe.get_instance(alg='unsecured',
                                                  from_compact=result)
        content = decrypter.decrypt(allow_unsecured=True)
        self.assertDictEqual(deep_sort(content),
                             {'claim_set': 'garble',
                              'commitments': {'attester': 'I do!'},
                              'content': 'stuff',
                              'v': Attestation.version,
                              'id': '08/15'})

    def test__make_salty_hashes(self):
        """Make salty hashes on loaded, uncommitted claim set."""
        claims = ClaimSet(from_binary=DEVICE_CLAIM_SET_STRIPPED_COMMITMENT,
                          unencrypted=True)
        my_attestation = Attestation(allow_unencrypted=True)
        my_attestation.claim_set_keys.claim_set = claims
        my_attestation._make_salty_hashes()
        self.assertEqual(
            len(my_attestation.claim_set_keys.claim_set.commitment_salts),
            len(claims.claims))
        self.assertEqual(
            len(my_attestation.claim_set_keys.claim_set.commitment_hashes),
            len(claims.claims))

    @mock.patch('sspyjose.Jose.DEFAULT_ENC', 'C20P')
    @mock.patch('time.time', autospec=True, return_value=1537086642.867628)
    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker(increment=167))
    @mock.patch('uuid.uuid4', autospec=True,
                side_effect=('00000000-1111-2222-3333-444444444444',
                             '11111111-2222-3333-4444-555555555555',
                             '22222222-3333-4444-5555-666666666666'))
    def test_finalise_complete_flow(self, *_):
        """Finalising an attestation with a complete flow."""
        # Prepare all the preliminaries.
        my_attestation = Attestation(claim_set_keys=CLAIM_SET_KEYS,
                                     subject_signing_key=self.sig_harry_pub)
        # Attester and attestation data prep.
        provenance_namespaces = {
            'aa': ADDITIONAL_PROV_NAMESPACES['aa'],
            'nzta': ADDITIONAL_PROV_NAMESPACES['nzta'],
            'watercare': ADDITIONAL_PROV_NAMESPACES['watercare']}
        attester_data = AttesterData(
            iss='aa:staff/2omXXXbobXXX...WEG',
            provenance_namespaces=provenance_namespaces,
            delegation_chain=['aa:Organisation'])
        my_attestation.attester_data = attester_data
        evidence_elements = {
            'nzta:dl/DJ034005-v193': 'original document',
            'watercare:customers/4711/invoices/201809.pdf': 'digital scan'}
        statements = [
            AttestationStatement(
                ttl=3600 * 24 * 90,  # 90 days.
                metadata={'kyc_compliance': 'type 3',
                          'governance': 'NZ, non-banking'}),
            AttestationStatement(
                exp=1568622642,
                metadata={'kyc_compliance': None,
                          'governance': 'NZ, non-banking'})]
        ancestors = [{'id': ANCESTOR_ID,
                      'object_key': ANCESTOR_OBJECT_KEY,
                      'trace_key': ANCESTOR_TRACE_KEY}]
        attestation_data = AttestationData(
            provenance_namespaces=provenance_namespaces,
            evidence_elements=evidence_elements,
            evidence_verification=CURRENT_MANUAL_VERIFICATION,
            ancestors=ancestors,
            statements=statements)
        my_attestation.attestation_data = attestation_data
        attester_signing_key = sspyjose.jwk.Jwk.get_instance(
            from_json=JWK_ALBUS_FULL_ED25519)

        # Try it.
        attestation_object = my_attestation.finalise(attester_signing_key)
        self.assertEqual(
            my_attestation.claim_set_keys.trace_key.to_dict()['k'],
            ATTESTATION_TRACE_KEY['k'])
        self.assertIn('k', my_attestation.claim_set_keys.object_key.to_dict())

        # Check it.
        self.assertEqual(my_attestation.id, ATTESTATION_ID)
        self.assertEqual(my_attestation.claim_set_keys.attestation_id,
                         ATTESTATION_ID)
        decrypter = sspyjose.jwe.Jwe.get_instance(
            from_compact=attestation_object,
            jwk=sspyjose.jwk.Jwk.get_instance(
                from_dict=CLAIM_SET_KEYS['object_key']))
        decrypted_attestation_object = decrypter.decrypt()
        subject_commitment = (
            decrypted_attestation_object['commitments']['subject'])
        self.assertEqual(subject_commitment,
                         my_attestation.commitments['subject'])
        attester_commitment = (
            decrypted_attestation_object['commitments']['attester'])
        self.assertEqual(attester_commitment,
                         my_attestation.commitments['attester'])
        self.assertEqual(decrypted_attestation_object['claim_set'],
                         CLAIM_SET_STRIPPED_COMMITMENT)
        signed_attestation_content = (
            decrypted_attestation_object['content'])
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=attester_commitment,
            jwk=attester_signing_key)
        verifier.verify()
        attester_commitment_payload = verifier.payload
        self.assertEqual(attester_commitment_payload['sub'], HARRY_DID)
        self.assertEqual(attester_commitment_payload['role'], 'attester')
        self.assertEqual(attester_commitment_payload['iss'],
                         'aa:staff/2omXXXbobXXX...WEG')
        subject_signing_key = sspyjose.jwk.Jwk.get_instance(
            from_json=JWK_HARRY_PUB_ED25519)
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=subject_commitment, jwk=subject_signing_key)
        verifier.verify()
        subject_commitment_payload = verifier.payload
        self.assertEqual(subject_commitment_payload['sub'], HARRY_DID)
        self.assertEqual(subject_commitment_payload['role'], 'subject')
        self.assertEqual(subject_commitment_payload['iss'], HARRY_DID)
        self.assertEqual(attester_commitment_payload['commitment'],
                         subject_commitment_payload['commitment'])
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=signed_attestation_content, jwk=attester_signing_key)
        verifier.verify()
        content_payload = verifier.payload
        self.assertEqual(content_payload['iss'],
                         my_attestation.attester_data.iss)
        self.assertEqual(content_payload['iat'], 1537086642)
        self.assertEqual(len(content_payload['statements']), 2)
        self.assertDictEqual(
            deep_sort(content_payload['statements'][0]),
            {'exp': 1544862642,
             'metadata': {'kyc_compliance': 'type 3',
                          'governance': 'NZ, non-banking'}})
        self.assertDictEqual(
            deep_sort(content_payload['statements'][1]),
            {'exp': 1568622642,
             'metadata': {'kyc_compliance': None,
                          'governance': 'NZ, non-banking'}})
        self.assert_(len(content_payload['provenance']) > 1000)
        self.assertEqual(len(content_payload['ancestor_keys']), 1)
        ancestor_id = list(content_payload['ancestor_keys'].keys())[0]
        self.assertEqual(ancestor_id,
                         my_attestation.attestation_data.ancestors[0]['id'])
        trace_key = sspyjose.jwk.Jwk.get_instance(
            from_dict=ATTESTATION_TRACE_KEY)
        decrypter = sspyjose.jwe.Jwe.get_instance(jwk=trace_key)
        ancestor = content_payload['ancestor_keys'][ancestor_id]
        object_key_cipher = ancestor['object_key']
        trace_key_cipher = ancestor['trace_key']
        decrypter.load_compact(object_key_cipher)
        object_key_recovered = decrypter.decrypt()
        self.assertDictEqual(deep_sort(object_key_recovered),
                             ANCESTOR_OBJECT_KEY)
        decrypter.load_compact(trace_key_cipher)
        trace_key_recovered = decrypter.decrypt()
        self.assertDictEqual(deep_sort(trace_key_recovered),
                             ANCESTOR_TRACE_KEY)

    @mock.patch('sspyjose.Jose.DEFAULT_ENC', 'A256GCM')
    @mock.patch('time.time', autospec=True, return_value=1537086642.867628)
    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker(increment=167))
    @mock.patch('uuid.uuid4', autospec=True,
                return_value='00000000-1111-2222-3333-444444444444')
    def test_finalise_complete_flow_unencrypted(self, *_):
        """
        Finalising an attestation with a complete flow, unencrypted content.
        """
        # Prepare all the preliminaries.
        my_attestation = Attestation(subject_signing_key=self.sig_harry_pub,
                                     allow_unencrypted=True)
        claim_set = ClaimSet(
            from_binary=DEVICE_CLAIM_SET,
            signing_key=self.sig_harry_pub, unencrypted=True)
        my_attestation.claim_set_keys.claim_set = claim_set
        # Attester and attestation data prep.
        attester_data = AttesterData(
            iss='did:ssid:5FuuQKEtXvHXpMdQFMC4Fq4zrqjowGhQKQwygsrbv87Jr5P6',
            delegation_chain=[
                'did:ssid:5E2J5Uqaf73CoKyD6itv4FcQvgVZ7nZR9rmmS3cCGbYNxEv7'])
        my_attestation.attester_data = attester_data
        statements = [
            AttestationStatement(
                ttl=10 * 365 * 24 * 3600,  # 10 years
                metadata={'kym_compliance': 'device identity, level 3',
                          'governance': 'IoT association'})]
        attestation_data = AttestationData(
            evidence_verification=CURRENT_MANUAL_VERIFICATION,
            statements=statements)
        my_attestation.attestation_data = attestation_data
        attester_signing_key = sspyjose.jwk.Jwk.get_instance(
            from_json=JWK_ALBUS_FULL_ED25519)

        # Try it.
        attestation_object = my_attestation.finalise(attester_signing_key)
        self.assertIsNone(my_attestation.claim_set_keys.object_key)
        self.assertIsNone(my_attestation.claim_set_keys.trace_key)

        # Check it.
        self.assertEqual(attestation_object, DEVICE_ATTESTATION)
        self.assertEqual(my_attestation.id, ATTESTATION_ID)
        self.assertEqual(my_attestation.claim_set_keys.attestation_id,
                         ATTESTATION_ID)
        decrypter = sspyjose.jwe.Jwe.get_instance(
            alg='unsecured', from_compact=attestation_object)
        decrypted_attestation_object = decrypter.decrypt(allow_unsecured=True)
        subject_commitment = (
            decrypted_attestation_object['commitments']['subject'])
        self.assertEqual(subject_commitment,
                         my_attestation.commitments['subject'])
        attester_commitment = (
            decrypted_attestation_object['commitments']['attester'])
        self.assertEqual(attester_commitment,
                         my_attestation.commitments['attester'])
        self.assertEqual(decrypted_attestation_object['claim_set'],
                         DEVICE_CLAIM_SET_STRIPPED_COMMITMENT)
        signed_attestation_content = (
            decrypted_attestation_object['content'])
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=attester_commitment,
            jwk=attester_signing_key)
        verifier.verify()
        attester_commitment_payload = verifier.payload
        self.assertEqual(attester_commitment_payload['sub'], HARRY_DID)
        self.assertEqual(attester_commitment_payload['role'], 'attester')
        self.assertEqual(
            attester_commitment_payload['iss'],
            'did:ssid:5FuuQKEtXvHXpMdQFMC4Fq4zrqjowGhQKQwygsrbv87Jr5P6')
        subject_signing_key = sspyjose.jwk.Jwk.get_instance(
            from_json=JWK_HARRY_PUB_ED25519)
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=subject_commitment, jwk=subject_signing_key)
        verifier.verify()
        subject_commitment_payload = verifier.payload
        self.assertEqual(subject_commitment_payload['sub'], HARRY_DID)
        self.assertEqual(subject_commitment_payload['role'], 'subject')
        self.assertEqual(subject_commitment_payload['iss'], HARRY_DID)
        self.assertEqual(attester_commitment_payload['commitment'],
                         subject_commitment_payload['commitment'])
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=signed_attestation_content, jwk=attester_signing_key)
        verifier.verify()
        content_payload = verifier.payload
        self.assertEqual(content_payload['iss'],
                         my_attestation.attester_data.iss)
        self.assertEqual(content_payload['iat'], 1537086642)
        self.assertEqual(len(content_payload['statements']), 1)
        self.assertDictEqual(
            deep_sort(content_payload['statements'][0]),
            {'exp': 1852446642,
             'metadata': {'kym_compliance': 'device identity, level 3',
                          'governance': 'IoT association'}})
        self.assert_(len(content_payload['provenance']) > 1000)
        self.assertEqual(len(content_payload['ancestor_keys']), 0)

    @mock.patch('time.time', autospec=True, return_value=1537086642.867628)
    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker(increment=167))
    def test_finalise_foreign_cs_complete_flow(self, *_):
        """Finalising an attestation for foreign claims with complete flow."""
        claims = ClaimSet(sub=HARRY_DID)
        claim_keys = ClaimSetKeys()
        claim_keys.claim_set = claims

        # Populate claim set.
        for item in HARRY_JSON_LD_CLAIMS:
            claim_keys.add_claim(item)

        claim_keys.finalise_claim_set(include_commitment=False)
        claim_keys.serialise(claim_type_hints=True)
        serialised_claim_set = claim_keys.encrypted_claim_set

        # Prepare all the preliminaries.
        my_attestation = Attestation(claim_set_keys=claim_keys)
        # Attester and attestation data prep.
        provenance_namespaces = {
            'aa': ADDITIONAL_PROV_NAMESPACES['aa'],
            'nzta': ADDITIONAL_PROV_NAMESPACES['nzta'],
            'watercare': ADDITIONAL_PROV_NAMESPACES['watercare']}
        attester_data = AttesterData(
            iss='aa:staff/2omXXXbobXXX...WEG',
            provenance_namespaces=provenance_namespaces,
            delegation_chain=['aa:Organisation'])
        my_attestation.attester_data = attester_data
        evidence_elements = {
            'nzta:dl/DJ034005-v193': 'original document',
            'watercare:customers/4711/invoices/201809.pdf': 'digital scan'}
        statements = [
            AttestationStatement(
                ttl=3600 * 24 * 90,  # 90 days.
                metadata={'kyc_compliance': 'type 3',
                          'governance': 'NZ, non-banking'}),
            AttestationStatement(
                exp=1568622642,
                metadata={'kyc_compliance': None,
                          'governance': 'NZ, non-banking'})]
        ancestors = [{'id': ANCESTOR_ID,
                      'object_key': ANCESTOR_OBJECT_KEY,
                      'trace_key': ANCESTOR_TRACE_KEY}]
        attestation_data = AttestationData(
            provenance_namespaces=provenance_namespaces,
            evidence_elements=evidence_elements,
            evidence_verification=CURRENT_MANUAL_VERIFICATION,
            ancestors=ancestors,
            statements=statements)
        my_attestation.attestation_data = attestation_data
        attester_signing_key = sspyjose.jwk.Jwk.get_instance(
            from_json=JWK_ALBUS_FULL_ED25519)

        # Try it.
        attestation_object = my_attestation.finalise(attester_signing_key)

        self.assertEqual(claim_keys.trace_key.to_dict()['k'],
                         CLAIM_SET_TRACE_KEY['k'])
        self.assertIn('k', claim_keys.object_key.to_dict())

        # Check it.
        # Unpick it first.
        self.assertIsNotNone(my_attestation.id)
        self.assertEqual(my_attestation.claim_set_keys.attestation_id,
                         my_attestation.id)
        decrypter = sspyjose.jwe.Jwe.get_instance(
            from_compact=attestation_object, jwk=claim_keys.object_key)
        decrypted_attestation_object = decrypter.decrypt()
        attester_commitment = (
            decrypted_attestation_object['commitments']['attester'])
        self.assertEqual(attester_commitment,
                         my_attestation.commitments['attester'])
        self.assertNotIn('subject',
                         decrypted_attestation_object['commitments'])
        self.assertEqual(decrypted_attestation_object['claim_set'],
                         serialised_claim_set)
        # Attester commitment.
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=attester_commitment, jwk=attester_signing_key)
        verifier.verify()
        commitment_payload = verifier.payload
        self.assertEqual(commitment_payload['sub'], HARRY_DID)
        self.assertEqual(commitment_payload['role'], 'attester')
        self.assertEqual(commitment_payload['iss'],
                         'aa:staff/2omXXXbobXXX...WEG')

        # Attestation payload.
        signed_attestation_content = (
            decrypted_attestation_object['content'])
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=signed_attestation_content, jwk=attester_signing_key)
        verifier.verify()
        content_payload = verifier.payload
        self.assertEqual(content_payload['iss'],
                         my_attestation.attester_data.iss)
        self.assertEqual(content_payload['iat'], 1537086642)
        self.assertEqual(len(content_payload['statements']), 2)
        self.assertDictEqual(
            deep_sort(content_payload['statements'][0]),
            {'exp': 1544862642,
             'metadata': {'kyc_compliance': 'type 3',
                          'governance': 'NZ, non-banking'}})
        self.assertDictEqual(
            deep_sort(content_payload['statements'][1]),
            {'exp': 1568622642,
             'metadata': {'kyc_compliance': None,
                          'governance': 'NZ, non-banking'}})
        self.assert_(len(content_payload['provenance']) > 1000)
        self.assertEqual(len(content_payload['ancestor_keys']), 1)
        ancestor_id = list(content_payload['ancestor_keys'].keys())[0]
        self.assertEqual(ancestor_id,
                         my_attestation.attestation_data.ancestors[0]['id'])
        decrypter = sspyjose.jwe.Jwe.get_instance(jwk=claim_keys.trace_key)
        ancestor = content_payload['ancestor_keys'][ancestor_id]
        object_key_cipher = ancestor['object_key']
        trace_key_cipher = ancestor['trace_key']
        decrypter.load_compact(object_key_cipher)
        object_key_recovered = decrypter.decrypt()
        self.assertDictEqual(deep_sort(object_key_recovered),
                             ANCESTOR_OBJECT_KEY)
        decrypter.load_compact(trace_key_cipher)
        trace_key_recovered = decrypter.decrypt()
        self.assertDictEqual(deep_sort(trace_key_recovered),
                             ANCESTOR_TRACE_KEY)

    @mock.patch('time.time', autospec=True, return_value=1537086642.867628)
    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker(increment=167))
    @mock.patch('uuid.uuid4', autospec=True,
                return_value='00000000-1111-2222-3333-444444444444')
    def test_finalise_foreign_cs_complete_flow_unencrypted(self, *_):
        """
        Finalising an attestation, foreign claims, complete flow, unencrypted.
        """
        claims = ClaimSet(from_binary=DEVICE_CLAIM_SET_STRIPPED_COMMITMENT,
                          unencrypted=True)

        # Prepare all the preliminaries.
        my_attestation = Attestation(allow_unencrypted=True)
        my_attestation.claim_set_keys.claim_set = claims
        # Make the 'encrypted' claim set the attestation finalisation needs.
        my_attestation.claim_set_keys.finalise_claim_set(
            include_commitment=False, retain_order=True)
        # Attester and attestation data prep.
        attester_data = AttesterData(
            iss='did:ssid:5FuuQKEtXvHXpMdQFMC4Fq4zrqjowGhQKQwygsrbv87Jr5P6',
            delegation_chain=[
                'did:ssid:5E2J5Uqaf73CoKyD6itv4FcQvgVZ7nZR9rmmS3cCGbYNxEv7'])
        my_attestation.attester_data = attester_data
        statements = [
            AttestationStatement(
                ttl=10 * 365 * 24 * 3600,  # 10 years
                metadata={'kym_compliance': 'device identity, level 3',
                          'governance': 'IoT association'})]
        attestation_data = AttestationData(
            evidence_verification=CURRENT_MANUAL_VERIFICATION,
            statements=statements)
        my_attestation.attestation_data = attestation_data
        attester_signing_key = sspyjose.jwk.Jwk.get_instance(
            from_json=JWK_ALBUS_FULL_ED25519)

        # Try it.
        attestation_object = my_attestation.finalise(attester_signing_key)
        self.assertIsNone(my_attestation.claim_set_keys.object_key)
        self.assertIsNone(my_attestation.claim_set_keys.trace_key)

        # Check it.
        # Unpick it first.
        self.assertEqual(my_attestation.id, ATTESTATION_ID)
        self.assertEqual(my_attestation.claim_set_keys.attestation_id,
                         ATTESTATION_ID)
        decrypter = sspyjose.jwe.Jwe.get_instance(
            alg='unsecured', from_compact=attestation_object)
        decrypted_attestation_object = decrypter.decrypt(allow_unsecured=True)
        self.assertNotIn('subject',
                         decrypted_attestation_object['commitments'])
        attester_commitment = (
            decrypted_attestation_object['commitments']['attester'])
        self.assertEqual(attester_commitment,
                         my_attestation.commitments['attester'])
        self.assertEqual(decrypted_attestation_object['claim_set'],
                         DEVICE_CLAIM_SET_STRIPPED_COMMITMENT)
        signed_attestation_content = (
            decrypted_attestation_object['content'])
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=attester_commitment,
            jwk=attester_signing_key)
        verifier.verify()
        attester_commitment_payload = verifier.payload
        self.assertEqual(attester_commitment_payload['sub'], HARRY_DID)
        self.assertEqual(attester_commitment_payload['role'], 'attester')
        self.assertEqual(
            attester_commitment_payload['iss'],
            'did:ssid:5FuuQKEtXvHXpMdQFMC4Fq4zrqjowGhQKQwygsrbv87Jr5P6')
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=signed_attestation_content, jwk=attester_signing_key)
        verifier.verify()
        content_payload = verifier.payload
        self.assertEqual(content_payload['iss'],
                         my_attestation.attester_data.iss)
        self.assertEqual(content_payload['iat'], 1537086642)
        self.assertEqual(len(content_payload['statements']), 1)
        self.assertDictEqual(
            deep_sort(content_payload['statements'][0]),
            {'exp': 1852446642,
             'metadata': {'kym_compliance': 'device identity, level 3',
                          'governance': 'IoT association'}})
        self.assert_(len(content_payload['provenance']) > 1000)
        self.assertEqual(len(content_payload['ancestor_keys']), 0)

    @mock.patch('sspyjose.Jose.DEFAULT_ENC', 'A256GCM')
    @mock.patch('time.time', autospec=True, return_value=1537086642.867628)
    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=random_mocker(increment=167))
    def test_finalise_foreign_cs_complete_flow_aes256gcm(self, *_):
        """Finalising attestation foreign claims complete flow (AES256-GCM)."""
        subject_signing_key = sspyjose.jwk.Jwk.get_instance(
            from_json=JWK_HARRY_PUB_ED25519)
        claims = ClaimSet(sub=HARRY_DID, signing_key=subject_signing_key)
        claim_keys = ClaimSetKeys()
        claim_keys.claim_set = claims

        # Populate claim set.
        for item in HARRY_JSON_LD_CLAIMS:
            claim_keys.add_claim(item)

        claim_keys.finalise_claim_set(include_commitment=False)
        claim_keys.serialise(claim_type_hints=True)
        serialised_claim_set = claim_keys.encrypted_claim_set

        # Prepare all the preliminaries.
        my_attestation = Attestation(claim_set_keys=claim_keys)
        # Attester and attestation data prep.
        provenance_namespaces = {
            'aa': ADDITIONAL_PROV_NAMESPACES['aa'],
            'nzta': ADDITIONAL_PROV_NAMESPACES['nzta'],
            'watercare': ADDITIONAL_PROV_NAMESPACES['watercare']}
        attester_data = AttesterData(
            iss='aa:staff/2omXXXbobXXX...WEG',
            provenance_namespaces=provenance_namespaces,
            delegation_chain=['aa:Organisation'])
        my_attestation.attester_data = attester_data
        evidence_elements = {
            'nzta:dl/DJ034005-v193': 'original document',
            'watercare:customers/4711/invoices/201809.pdf': 'digital scan'}
        statements = [
            AttestationStatement(
                ttl=3600 * 24 * 90,  # 90 days.
                metadata={'kyc_compliance': 'type 3',
                          'governance': 'NZ, non-banking'}),
            AttestationStatement(
                exp=1568622642,
                metadata={'kyc_compliance': None,
                          'governance': 'NZ, non-banking'})]
        ancestors = [{'id': ANCESTOR_ID,
                      'object_key': ANCESTOR_OBJECT_KEY,
                      'trace_key': ANCESTOR_TRACE_KEY}]
        attestation_data = AttestationData(
            provenance_namespaces=provenance_namespaces,
            evidence_elements=evidence_elements,
            evidence_verification=CURRENT_MANUAL_VERIFICATION,
            ancestors=ancestors,
            statements=statements)
        my_attestation.attestation_data = attestation_data
        attester_signing_key = sspyjose.jwk.Jwk.get_instance(
            from_json=JWK_ALBUS_FULL_ED25519)

        # Try it.
        attestation_object = my_attestation.finalise(attester_signing_key)

        self.assertEqual(claim_keys.trace_key.to_dict()['k'],
                         CLAIM_SET_TRACE_KEY['k'])
        self.assertIn('k', claim_keys.object_key.to_dict())

        # Check it.
        self.assertIsNotNone(my_attestation.id)
        self.assertEqual(my_attestation.claim_set_keys.attestation_id,
                         my_attestation.id)
        decrypter = sspyjose.jwe.Jwe.get_instance(
            from_compact=attestation_object,
            jwk=claim_keys.object_key)
        decrypted_attestation_object = decrypter.decrypt()
        attester_commitment = (
            decrypted_attestation_object['commitments']['attester'])
        self.assertEqual(attester_commitment,
                         my_attestation.commitments['attester'])
        self.assertNotIn('subject',
                         decrypted_attestation_object['commitments'])
        self.assertEqual(decrypted_attestation_object['claim_set'],
                         serialised_claim_set)
        # Attester commitment.
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=attester_commitment, jwk=attester_signing_key)
        verifier.verify()
        commitment_payload = verifier.payload
        self.assertEqual(commitment_payload['sub'], HARRY_DID)
        self.assertEqual(commitment_payload['role'], 'attester')
        self.assertEqual(commitment_payload['iss'],
                         'aa:staff/2omXXXbobXXX...WEG')

        # Attestation payload.
        signed_attestation_content = (
            decrypted_attestation_object['content'])
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=signed_attestation_content, jwk=attester_signing_key)
        verifier.verify()
        content_payload = verifier.payload
        self.assertEqual(content_payload['iss'],
                         my_attestation.attester_data.iss)
        self.assertEqual(content_payload['iat'], 1537086642)
        self.assertEqual(len(content_payload['statements']), 2)
        self.assertDictEqual(
            deep_sort(content_payload['statements'][0]),
            {'exp': 1544862642,
             'metadata': {'kyc_compliance': 'type 3',
                          'governance': 'NZ, non-banking'}})
        self.assertDictEqual(
            deep_sort(content_payload['statements'][1]),
            {'exp': 1568622642,
             'metadata': {'kyc_compliance': None,
                          'governance': 'NZ, non-banking'}})
        self.assert_(len(content_payload['provenance']) > 1000)
        self.assertEqual(len(content_payload['ancestor_keys']), 1)
        ancestor_id = list(content_payload['ancestor_keys'].keys())[0]
        self.assertEqual(ancestor_id,
                         my_attestation.attestation_data.ancestors[0]['id'])
        decrypter = sspyjose.jwe.Jwe.get_instance(jwk=claim_keys.trace_key)
        ancestor = content_payload['ancestor_keys'][ancestor_id]
        object_key_cipher = ancestor['object_key']
        trace_key_cipher = ancestor['trace_key']
        decrypter.load_compact(object_key_cipher)
        object_key_recovered = decrypter.decrypt()
        self.assertDictEqual(deep_sort(object_key_recovered),
                             ANCESTOR_OBJECT_KEY)
        decrypter.load_compact(trace_key_cipher)
        trace_key_recovered = decrypter.decrypt()
        self.assertDictEqual(deep_sort(trace_key_recovered),
                             ANCESTOR_TRACE_KEY)

    def test_load(self):
        """Load an attestation."""
        case_0 = REFERENCE_DATA['cases']['0']
        step1_output = case_0['steps'][1]['output']
        claim_set_keys = step1_output['claim_set_keys']
        ancestors = case_0['data']['attestation_data']['ancestors']

        my_attestation = Attestation(claim_set_keys=claim_set_keys,
                                     subject_signing_key=self.sig_harry_pub,
                                     attester_signing_key=self.sig_albus_pub)
        my_attestation.load(step1_output['attestation'])
        self.assertEqual(len(my_attestation.commitment_payloads), 2)
        subject_commitment = my_attestation.commitment_payloads['subject']
        self.assertEqual(subject_commitment['iss'],
                         case_0['data']['harry_did'])
        self.assertEqual(subject_commitment['role'], 'subject')
        self.assertEqual(subject_commitment['sub'],
                         case_0['data']['harry_did'])
        attester_commitment = my_attestation.commitment_payloads['attester']
        self.assertEqual(attester_commitment['iss'],
                         case_0['data']['attester_data']['attester'])
        self.assertEqual(attester_commitment['role'], 'attester')
        self.assertEqual(attester_commitment['sub'],
                         case_0['data']['harry_did'])
        self.assertEqual(my_attestation.attester_data.iss,
                         case_0['data']['attester_data']['attester'])
        self.assertEqual(my_attestation.attestation_data.iat, 1553836603)
        self.assertEqual(my_attestation.claim_set_keys.encrypted_claim_set,
                         claim_set_keys['claim_set'])
        self.assertEqual(len(my_attestation.provenance), 3064)
        statement1 = {'exp': 1561612603,
                      'metadata': {'kyc_compliance': 'type 3',
                                   'governance': 'NZ, non-banking'}}
        statement2 = {'exp': 1568622642,
                      'metadata': {'kyc_compliance': None,
                                   'governance': 'NZ, non-banking'}}
        self.assertEqual(len(my_attestation.attestation_data.statements), 2)
        self.assertDictEqual(
            deep_sort(my_attestation.attestation_data.statements[0]),
            statement1)
        self.assertDictEqual(
            deep_sort(my_attestation.attestation_data.statements[1]),
            statement2)
        self.assertEqual(len(my_attestation.attestation_data.ancestors), 1)
        ancestor = my_attestation.attestation_data.ancestors[0]
        self.assertEqual(ancestor['id'], ancestors[0]['id'])
        self.assertDictEqual(deep_sort(ancestor['object_key']),
                             ancestors[0]['object_key'])
        self.assertDictEqual(deep_sort(ancestor['trace_key']),
                             ancestors[0]['trace_key'])
        self.assertIsNone(ancestor.get('wrapper_uri'))
        claim_set = my_attestation.claim_set_keys.claim_set
        no_claims = len(case_0['data']['harry_claims'])
        self.assertEqual(len(claim_set.claims), no_claims)
        self.assertEqual(len(claim_set.commitment_hashes), no_claims)
        self.assertEqual(len(claim_set.commitment_salts), no_claims)

    def test_load_foreign_cs(self):
        """Load an attestation of a foreign claim set."""
        case_1 = REFERENCE_DATA['cases']['1']
        step1_output = case_1['steps'][1]['output']
        claim_set_keys = step1_output['claim_set_keys']
        attestation_data = step1_output['attestation']
        ancestors = case_1['data']['attestation_data']['ancestors']
        claim_keys = ClaimSetKeys(data=claim_set_keys)
        my_attestation = Attestation(claim_set_keys=claim_keys,
                                     subject_signing_key=self.sig_harry_pub,
                                     attester_signing_key=self.sig_albus_pub)
        my_attestation.load(attestation_data)

        attester_commitment = my_attestation.commitment_payloads['attester']
        self.assertEqual(attester_commitment['iss'],
                         case_1['data']['attester_data']['attester'])
        self.assertEqual(attester_commitment['role'], 'attester')
        self.assertEqual(attester_commitment['sub'],
                         case_1['data']['harry_did'])
        self.assertEqual(my_attestation.attester_data.iss,
                         case_1['data']['attester_data']['attester'])
        self.assertEqual(my_attestation.attestation_data.iat, 1553836603)
        self.assertEqual(my_attestation.claim_set_keys.encrypted_claim_set,
                         claim_set_keys['claim_set'])
        self.assertEqual(len(my_attestation.provenance), 3064)
        statement1 = {'exp': 1561612603,
                      'metadata': {'kyc_compliance': 'type 3',
                                   'governance': 'NZ, non-banking'}}
        statement2 = {'exp': 1568622642,
                      'metadata': {'kyc_compliance': None,
                                   'governance': 'NZ, non-banking'}}
        self.assertEqual(len(my_attestation.attestation_data.statements), 2)
        self.assertDictEqual(
            deep_sort(my_attestation.attestation_data.statements[0]),
            statement1)
        self.assertDictEqual(
            deep_sort(my_attestation.attestation_data.statements[1]),
            statement2)
        self.assertEqual(len(my_attestation.attestation_data.ancestors), 1)
        ancestor = my_attestation.attestation_data.ancestors[0]
        self.assertEqual(ancestor['id'], ancestors[0]['id'])
        self.assertDictEqual(deep_sort(ancestor['object_key']),
                             ancestors[0]['object_key'])
        self.assertDictEqual(deep_sort(ancestor['trace_key']),
                             ancestors[0]['trace_key'])
        self.assertIsNone(ancestor.get('wrapper_uri'))
        claim_set = my_attestation.claim_set_keys.claim_set
        self.assertEqual(len(claim_set.claims),
                         len(case_1['data']['harry_claims']))
        self.assertEqual(len(claim_set.commitment_hashes),
                         len(case_1['data']['harry_claims']))
        self.assertEqual(len(claim_set.commitment_salts),
                         len(case_1['data']['harry_claims']))

        self.assertIsInstance(my_attestation.content, str)
        self.assertEqual(len(my_attestation.content), 5289)
        self.assertEqual(len(my_attestation.commitments), 1)
        self.assertIsInstance(my_attestation.commitments['attester'], str)
        self.assertEqual(len(my_attestation.commitments['attester']), 1069)

    def test_load_unsecured_device(self):
        """Load an unsecured device attestation."""
        my_attestation = Attestation(subject_signing_key=self.sig_harry_pub,
                                     attester_signing_key=self.sig_albus_pub,
                                     allow_unencrypted=True)
        my_attestation.load(DEVICE_ATTESTATION)

    def test_cosign_attestation(self):
        """Co-sign a foreign claim set attestation."""
        my_attestation = Attestation(claim_set_keys=CLAIM_SET_KEYS,
                                     subject_signing_key=self.sig_harry_pub,
                                     attester_signing_key=self.sig_albus_pub)
        trace_key = sspyjose.jwk.Jwk.get_instance(
            from_dict=ATTESTATION_TRACE_KEY)
        my_attestation.claim_set_keys.trace_key = trace_key
        my_attestation.load(ATTESTATION)
        my_attestation.cosign(self.sig_harry_full)

        self.assertIsNone(my_attestation.claim_set_keys.claim_set.commitment)
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=my_attestation.commitments['subject'],
            jwk=self.sig_harry_pub)
        verifier.verify()
        subject_commitment = verifier.payload
        self.assertEqual(subject_commitment['iss'], HARRY_DID)
        self.assertEqual(subject_commitment['role'], 'subject')
        self.assertEqual(subject_commitment['sub'], HARRY_DID)
        self.assertEqual(subject_commitment['commitment'],
                         CLAIM_SET_COMMITMENT)

        # Check the content against the previous attester commitment.
        attester_commitment = my_attestation.commitment_payloads['attester']
        self.assertEqual(attester_commitment['iss'],
                         'aa:staff/2omXXXbobXXX...WEG')
        self.assertEqual(attester_commitment['role'], 'attester')
        self.assertEqual(attester_commitment['sub'],
                         subject_commitment['sub'])
        self.assertEqual(attester_commitment['commitment'],
                         subject_commitment['commitment'])

    def test_serialise_cosigned_attestation(self):
        """Finalise a co-signed foreign claim set attestation."""
        my_attestation = Attestation(claim_set_keys=FOREIGN_CLAIM_SET_KEYS,
                                     subject_signing_key=self.sig_harry_pub,
                                     attester_signing_key=self.sig_albus_pub)
        my_attestation.load(FOREIGN_ATTESTATION)
        subject_signing_key = sspyjose.jwk.Jwk.get_instance(
            from_json=JWK_HARRY_FULL_ED25519)
        attester_signing_key = sspyjose.jwk.Jwk.get_instance(
            from_json=JWK_ALBUS_FULL_ED25519)
        my_attestation.cosign(subject_signing_key)

        attestation_object = my_attestation.serialise()

        self.assertEqual(
            my_attestation.claim_set_keys.trace_key.to_dict()['k'],
            FOREIGN_CLAIM_SET_KEYS['trace_key']['k'])
        self.assertEqual(
            my_attestation.claim_set_keys.object_key.to_dict()['k'],
            FOREIGN_CLAIM_SET_KEYS['object_key']['k'])

        # Check it.
        decrypter = sspyjose.jwe.Jwe.get_instance(
            from_compact=attestation_object,
            jwk=my_attestation.claim_set_keys.object_key)
        decrypted_attestation_object = decrypter.decrypt()

        # Claim set.
        claim_set = decrypted_attestation_object['claim_set']
        self.assertEqual(claim_set, FOREIGN_CLAIM_SET)
        object_key = sspyjose.jwk.Jwk.get_instance(
            from_dict=FOREIGN_CLAIM_SET_KEYS['object_key'])
        decrypter = sspyjose.jwe.Jwe.get_instance(jwk=object_key,
                                                  from_compact=claim_set)
        claim_set_content = decrypter.decrypt()
        self.assertNotIn('commitment', claim_set_content)

        # Attester commitment.
        attester_commitment = (
            decrypted_attestation_object['commitments']['attester'])
        self.assertEqual(attester_commitment,
                         my_attestation.commitments['attester'])
        attester_commitment_content = utils.json_to_dict(
            utils.string_to_bytes(attester_commitment.split('.')[1]))
        self.assertEqual(attester_commitment_content['role'], 'attester')
        self.assertEqual(attester_commitment_content['sub'], HARRY_DID)
        self.assertEqual(attester_commitment_content['commitment'],
                         FOREIGN_CLAIM_SET_COMMITMENT)
        self.assertEqual(attester_commitment_content['iss'],
                         'aa:staff/2omXXXbobXXX...WEG')

        # Attestation content.
        signed_attestation_content = (
            decrypted_attestation_object['content'])
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=signed_attestation_content, jwk=attester_signing_key)
        verifier.verify()
        content_payload = verifier.payload
        subject_commitment = (
            decrypted_attestation_object['commitments']['subject'])
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=subject_commitment, jwk=subject_signing_key)
        verifier.verify()
        subject_commitment_payload = verifier.payload
        self.assertEqual(subject_commitment_payload['role'], 'subject')
        self.assertEqual(subject_commitment_payload['sub'], HARRY_DID)
        self.assertEqual(subject_commitment_payload['commitment'],
                         FOREIGN_CLAIM_SET_COMMITMENT)
        self.assertEqual(subject_commitment_payload['iss'], HARRY_DID)
        self.assertEqual(content_payload['iss'],
                         my_attestation.attester_data.iss)
        self.assertEqual(content_payload['iat'], 1537086642)
        self.assertEqual(len(content_payload['statements']), 2)
        self.assertDictEqual(
            deep_sort(content_payload['statements'][0]),
            {'exp': 1544862642,
             'metadata': {'kyc_compliance': 'type 3',
                          'governance': 'NZ, non-banking'}})
        self.assertDictEqual(
            deep_sort(content_payload['statements'][1]),
            {'exp': 1568622642,
             'metadata': {'kyc_compliance': None,
                          'governance': 'NZ, non-banking'}})
        self.assert_(len(content_payload['provenance']) > 1000)
        self.assertEqual(len(content_payload['ancestor_keys']), 1)
        ancestor_id = list(content_payload['ancestor_keys'].keys())[0]
        self.assertEqual(ancestor_id,
                         my_attestation.attestation_data.ancestors[0]['id'])
        trace_key = sspyjose.jwk.Jwk.get_instance(
            from_dict=FOREIGN_CLAIM_SET_KEYS['trace_key'])
        decrypter = sspyjose.jwe.Jwe.get_instance(jwk=trace_key)
        ancestor = content_payload['ancestor_keys'][ancestor_id]
        object_key_cipher = ancestor['object_key']
        trace_key_cipher = ancestor['trace_key']
        decrypter.load_compact(object_key_cipher)
        object_key_recovered = decrypter.decrypt()
        self.assertDictEqual(deep_sort(object_key_recovered),
                             ANCESTOR_OBJECT_KEY)
        decrypter.load_compact(trace_key_cipher)
        trace_key_recovered = decrypter.decrypt()
        self.assertDictEqual(deep_sort(trace_key_recovered),
                             ANCESTOR_TRACE_KEY)

    def test_attester_commitment_data(self):
        """Retrieve an attester commitment."""
        my_attestation = Attestation(claim_set_keys=FOREIGN_CLAIM_SET_KEYS,
                                     from_binary=FOREIGN_ATTESTATION,
                                     subject_signing_key=self.sig_harry_pub,
                                     attester_signing_key=self.sig_albus_pub)
        commitment = my_attestation.commitment_payloads['attester']
        self.assertEqual(commitment['commitment'],
                         FOREIGN_CLAIM_SET_COMMITMENT)
        self.assertEqual(commitment['sub'], HARRY_DID)
        self.assertEqual(commitment['role'], 'attester')
        self.assertEqual(commitment['iss'], 'aa:staff/2omXXXbobXXX...WEG')

    def test_subject_commitment_data(self):
        """Retrieve a subject commitment."""
        claim_set_keys = copy.deepcopy(CLAIM_SET_KEYS)
        claim_set_keys['trace_key'] = ATTESTATION_TRACE_KEY
        my_attestation = Attestation(claim_set_keys=claim_set_keys,
                                     from_binary=ATTESTATION,
                                     subject_signing_key=self.sig_harry_pub,
                                     attester_signing_key=self.sig_albus_pub)
        commitment = my_attestation.commitment_payloads['subject']
        self.assertEqual(commitment['commitment'], CLAIM_SET_COMMITMENT)
        self.assertEqual(commitment['sub'], HARRY_DID)
        self.assertEqual(commitment['role'], 'subject')
        self.assertEqual(commitment['iss'], HARRY_DID)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

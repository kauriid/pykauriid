# -*- coding: utf-8 -*-
"""Tests for the attestations module."""

# Created: 2018-09-12 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

from unittest import mock  # @UnusedImport
import unittest

import sspyjose

from pykauriid.messages import (Message,
                                AttestationRequest,
                                AttestationResponse,
                                DisclosureRequest)
from tests.data import (JWK_HARRY_PUB_X25519,
                        JWK_HARRY_PUB_ED25519,
                        JWK_HARRY_FULL_ED25519,
                        JWK_HARRY_FULL_X25519_DICT,
                        JWK_ALBUS_FULL_ED25519,
                        JWK_ALBUS_PUB_ED25519,
                        FOREIGN_CLAIM_SET_KEYS,
                        FOREIGN_ATTESTATION)
from tests.utils import deep_sort


class MessagesTest(unittest.TestCase):
    """Testing the Messages class functionality."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    @mock.patch('sspyjose.Jose.DEFAULT_ENC', 'C20P')
    def test_deserialise_attestation_request(self):
        """Deserialising an attestation request."""
        message = (
            'eyJhbGciOiJFQ0RILUVTIiwiZW5jIjoiQzIwUCIsImVwayI6eyJrdHkiOiJPS1'
            'AiLCJjcnYiOiJYMjU1MTkiLCJ4IjoiMWg4c3FLWWVZemFiZTJOOUQ4QzIzcjE4'
            'WDNyMTlUT21KVUl4Z0ozZ0p4byJ9fQ..o1YzFWk9ll_gT_BR.Qy-uaSK57E6j2'
            '8IQVau3sVBevHb3p3glYMGFNpXEmjBlaK2_IhzAVTB17GsVMsFi7jARijetkP1'
            'YQbIidbBZT67u9x99.5NdvX4X5AueF0oFket1duQ')
        result = Message.deserialise(message, JWK_HARRY_FULL_X25519_DICT)
        self.assertIsInstance(result, AttestationRequest)
        self.assertDictEqual(result.claim_set_keys, {'answer': 42})

    @mock.patch('sspyjose.Jose.DEFAULT_ENC', 'C20P')
    def test_deserialise_attestation_response(self):
        """Deserialising an attestation response."""
        message = (
            'eyJhbGciOiJFQ0RILUVTIiwiZW5jIjoiQzIwUCIsImVwayI6eyJrdHkiOiJPS1'
            'AiLCJjcnYiOiJYMjU1MTkiLCJ4IjoidFhseHRqSEJhQlFLN2F5blFlMDdydVR6'
            'd1JGc0FjUEdKUF84RnJEaF9XNCJ9fQ..cnOoMQRwkVUTPCn7.AN3afNfiVrDrB'
            '_VIOXY8FNTUm-2meUJaAZ_7nxwfgBjvm7O9BaILnelYBDk21vhR4C_O0zSp08u'
            '3Z65Iwg-QIh0kP8QKcw.HIfXwpfdg65xcXqNveAQAg')
        result = Message.deserialise(message, JWK_HARRY_FULL_X25519_DICT)
        self.assertIsInstance(result, AttestationResponse)
        self.assertDictEqual(result.claim_set_keys, {'answer': 42})

    @mock.patch('pykauriid.messages.Jwe', autospec=True)
    def test_deserialise_unknown_msg_type(self, jwe_mock):
        """Deserialising an unknown message type."""
        message = (
            'eyJhbGciOiJFQ0RILUVTIiwiZW5jIjoiQ2hhQ2hhMjAvUG9seTEzMDUiLCJlcGsiO'
            'nsia3R5IjoiT0tQIiwiY3J2IjoiWDI1NTE5IiwieCI6InN0S3gxVm5adWI2Mnk4Nm'
            'g3VDk3a1pKVGRBNGM2MGhsRkdhV2ozcFpyMnMifX0..mfkc5Vsk9SVknsvY.DyFAq'
            'yOnNCQrYrIjD3z1eGcOKwI2cKV73jkQ5-QAlpnFMGCSIL2XzMELKPc7Y0iMlcNbuj'
            'gD7jJ6bhhr_fhR21ZZo2hd.A-hJnMRv3BAq_R7WdG2lxA')
        message_dict = {'message_type': 'frobnication_request',
                        'claim_set_keys': {'answer': 42}}
        jwe_object = mock.create_autospec(sspyjose.jwe.Jwe)
        jwe_object.decrypt.return_value = message_dict
        jwe_mock.get_instance.return_value = jwe_object
        result = Message.deserialise(message, JWK_HARRY_FULL_X25519_DICT)
        self.assertEqual(jwe_mock.get_instance.call_count, 1)
        self.assertIsInstance(result, Message)
        self.assertEqual(result.message_type, 'frobnication_request')
        self.assertDictEqual(result._message, message_dict)


class AttestationRequestTest(unittest.TestCase):
    """Testing the AttestationRequest class functionality."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    def test_constructor(self):
        """Constructor."""
        my_request = AttestationRequest(claim_set_keys={'answer': 42})
        self.assertEqual(my_request.message_type, 'attestation_request')
        self.assertDictEqual(my_request.claim_set_keys, {'answer': 42})

    def test_constructor_with_message(self):
        """Constructor with message."""
        message = {'claim_set_keys': {'answer': 42}}
        my_request = AttestationRequest(message=message)
        self.assertEqual(my_request.message_type, 'attestation_request')
        self.assertDictEqual(my_request.claim_set_keys, {'answer': 42})

    def test_serialise(self):
        """Serialise and encrypt."""
        my_request = AttestationRequest(claim_set_keys={'answer': 42})
        result = my_request.serialise(JWK_HARRY_PUB_X25519)
        decrypter = sspyjose.jwe.Jwe.get_instance(
            alg='ECDH-ES',
            jwk=sspyjose.jwk.Jwk.get_instance(
                from_dict=JWK_HARRY_FULL_X25519_DICT))
        decrypter.load_compact(result)
        recovered = decrypter.decrypt()
        check_message = {'message_type': 'attestation_request',
                         'claim_set_keys': {'answer': 42}}
        self.assertDictEqual(my_request._message, check_message)
        self.assertDictEqual(deep_sort(recovered), check_message)

    def test_serialise_dict_key(self):
        """Serialise and encrypt passing a JWK as dict."""
        my_request = AttestationRequest(claim_set_keys={'answer': 42})
        my_request.serialise(JWK_HARRY_FULL_X25519_DICT)


class AttestationResponseTest(unittest.TestCase):
    """Testing the AttestationResponse class functionality."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    def test_constructor(self):
        """Constructor."""
        my_response = AttestationResponse(claim_set_keys={'answer': 42})
        self.assertEqual(my_response.message_type, 'attestation_response')
        self.assertDictEqual(my_response.claim_set_keys, {'answer': 42})

    def test_constructor_with_message(self):
        """Constructor with message."""
        message = {'claim_set_keys': {'answer': 42}}
        my_response = AttestationResponse(message=message)
        self.assertEqual(my_response.message_type, 'attestation_response')
        self.assertDictEqual(my_response.claim_set_keys, {'answer': 42})

    def test_serialise(self):
        """Serialise and encrypt."""
        my_response = AttestationResponse(
            claim_set_keys=FOREIGN_CLAIM_SET_KEYS,
            attestation=FOREIGN_ATTESTATION)
        result = my_response.serialise(JWK_HARRY_PUB_X25519)
        decrypter = sspyjose.jwe.Jwe.get_instance(
            alg='ECDH-ES',
            jwk=sspyjose.jwk.Jwk.get_instance(
                from_dict=JWK_HARRY_FULL_X25519_DICT))
        decrypter.load_compact(result)
        recovered = decrypter.decrypt()
        check_message = {'message_type': 'attestation_response',
                         'claim_set_keys': FOREIGN_CLAIM_SET_KEYS,
                         'attestation': FOREIGN_ATTESTATION}
        self.assertDictEqual(my_response._message, check_message)
        self.assertDictEqual(deep_sort(recovered), check_message)

    def test_serialise_dict_key(self):
        """Serialise and encrypt passing a JWK as dict."""
        my_response = AttestationResponse(claim_set_keys={'answer': 42})
        my_response.serialise(JWK_HARRY_FULL_X25519_DICT)


class DisclosureRequestTest(unittest.TestCase):
    """Testing the DisclosureRequest class functionality."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    def test_constructor(self):
        """Testing the constructor with all parameters."""
        requested_claims = [
            "http://schema.org/givenName",
            "http://schema.org/familyName",
            "http://schema.org/email"
        ]
        my_request = DisclosureRequest(
            iss="000000",
            issc={'name': 'Steve Stevenson',
                  'url': 'https://somecorp.example'},
            sender_sig_sk=JWK_HARRY_FULL_X25519_DICT,
            sender_dh_pk=JWK_HARRY_PUB_X25519,
            requested_claims=requested_claims,
            callback_url='https://fake.notreal.nope/imaginary')
        self.assertEqual(my_request.message_type, 'shareReq')

    def test_serialise(self):
        """Serialising a disclosure request method."""
        requested_claims = [
            "http://schema.org/givenName",
            "http://schema.org/familyName",
            "http://schema.org/email"
        ]
        my_request = DisclosureRequest(
            iss="000000",
            issc={'name': 'Steve Stevenson',
                  'url': 'https://somecorp.example'},
            sender_sig_sk=JWK_ALBUS_FULL_ED25519,
            sender_dh_pk=JWK_HARRY_PUB_X25519,
            requested_claims=requested_claims,
            callback_url='https://fake.notreal.nope/imaginary')
        result = my_request.serialise()
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=result,
            jwk=sspyjose.jwk.Jwk.get_instance(
                from_json=JWK_ALBUS_PUB_ED25519))
        self.assertTrue(verifier.verify())

        check_payload = ["http://schema.org/givenName",
                         "http://schema.org/familyName",
                         "http://schema.org/email"]

        self.assertEqual(verifier.payload['verified'], check_payload)

    def test_from_json_serialise(self):
        """Serialising with keys from JSON."""
        requested_claims = [
            "http://schema.org/givenName",
            "http://schema.org/familyName",
            "http://schema.org/email"
        ]
        my_request = DisclosureRequest(
            iss="000000",
            issc={'name': 'Steve Stevenson',
                  'url': 'https://somecorp.example'},
            sender_sig_sk=sspyjose.jwk.Jwk.get_instance(
                from_json=JWK_HARRY_FULL_ED25519),
            sender_dh_pk=sspyjose.jwk.Jwk.get_instance(
                from_json=JWK_HARRY_PUB_X25519),
            requested_claims=requested_claims,
            callback_url='https://fake.notreal.nope/imaginary')
        result = my_request.serialise()
        verifier = sspyjose.jws.Jws.get_instance(
            from_compact=result,
            jwk=sspyjose.jwk.Jwk.get_instance(
                from_json=JWK_HARRY_PUB_ED25519))
        self.assertTrue(verifier.verify())

        check_payload = ["http://schema.org/givenName",
                         "http://schema.org/familyName",
                         "http://schema.org/email"]

        self.assertEqual(verifier.payload['verified'], check_payload)


if __name__ == "__main__":
    unittest.main()

# -*- coding: utf-8 -*-
"""Tests for the reference data set."""

# Created: 2019-03-12 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

import json
from unittest import mock  # @UnusedImport
import unittest

from sspyjose.jwk import Jwk

from pykauriid.attestations import (Attestation,
                                    AttesterData,
                                    AttestationStatement,
                                    AttestationData)
from pykauriid.claims import (ClaimSet,
                              ClaimSetKeys)
from tests.reference_data import REFERENCE_DATA


CASE_0 = REFERENCE_DATA['cases']['0']


@mock.patch('sspyjose.Jose.DEFAULT_ENC', 'A256GCM')
class ReferenceTest(unittest.TestCase):
    """Testing against the reference data."""

    def setUp(self):  # noqa: D102
        self.jwk_sig_harry_full = CASE_0['data']['jwks']['harry_sig_full']
        self.jwk_sig_harry_pub = CASE_0['data']['jwks']['harry_sig_pub']
        self.jwk_sig_albus_full = CASE_0['data']['jwks']['dumbledore_sig_full']
        self.jwk_sig_albus_pub = CASE_0['data']['jwks']['dumbledore_sig_pub']
        self.harry_did = CASE_0['data']['harry_did']
        self.harry_claims = CASE_0['data']['harry_claims']
        attester_data_dict = CASE_0['data']['attester_data']
        self.attester_data = AttesterData(
            iss=attester_data_dict['attester'],
            delegation_chain=attester_data_dict['delegation_chain'],
            provenance_namespaces=attester_data_dict['provenance_namespaces'])
        att_data = CASE_0['data']['attestation_data']
        self.statements = [
            AttestationStatement(
                ttl=att_data['statements'][0]['ttl'],
                metadata=att_data['statements'][0]['metadata']),
            AttestationStatement(
                exp=att_data['statements'][1]['exp'],
                metadata=att_data['statements'][1]['metadata'])]
        self.attestation_data = AttestationData(
            provenance_namespaces=att_data['provenance_namespaces'],
            evidence_elements=att_data['evidence_elements'],
            evidence_verification=att_data['evidence_verification'],
            ancestors=att_data['ancestors'],
            statements=self.statements)

    def test_create_self_claim_set(self):
        """Create a new self claim set object (within claim set keys)."""
        signing_key = Jwk.get_instance(from_dict=self.jwk_sig_harry_full)
        claim_set = ClaimSet(self.harry_did, signing_key)
        claims_keys = ClaimSetKeys()
        claims_keys.claim_set = claim_set
        for claim in self.harry_claims:
            claims_keys.add_claim(claim)
        claims_keys.finalise_claim_set()
        serialised_claim_set_keys = claims_keys.serialise(
            claim_type_hints=True)
        claim_set_keys_dict = json.loads(serialised_claim_set_keys)
        claim_set_keys_check = CASE_0['steps'][1]['output']['claim_set_keys']
        # Check whether all claim type hints are present.
        for item in claim_set_keys_dict['claim_type_hints'].values():
            self.assertIn(
                item, claim_set_keys_check['claim_type_hints'].values())
        # Check whether we can read the claim set again.
        ClaimSetKeys(data=serialised_claim_set_keys, signing_key=signing_key)

    def test_attest_self_claim_set(self):
        """Attest a self claim set object (within claim set keys)."""
        attester_sig_key_full = Jwk.get_instance(
            from_dict=self.jwk_sig_albus_full)
        attester_sig_key_pub = Jwk.get_instance(
            from_dict=self.jwk_sig_albus_pub)
        subject_sig_key_pub = Jwk.get_instance(
            from_dict=self.jwk_sig_harry_pub)
        claim_set_keys = (
            CASE_0['steps'][0]['output']['claim_set_keys'])
        my_attestation = Attestation(claim_set_keys=claim_set_keys,
                                     subject_signing_key=subject_sig_key_pub,
                                     attester_signing_key=attester_sig_key_pub)
        # Add attester and attestation data.
        my_attestation.attester_data = self.attester_data
        my_attestation.attestation_data = self.attestation_data
        # Attest the claim set.
        attestation_content = my_attestation.finalise(attester_sig_key_full)
        # Update claim set keys (now contains a trace key).
        updated_claim_set_keys = my_attestation.claim_set_keys.serialise(
            claim_type_hints=True)
        # Compare some of the data.
        output_check = CASE_0['steps'][1]['output']
        attestation_check = Attestation(
            claim_set_keys=output_check['claim_set_keys'],
            subject_signing_key=subject_sig_key_pub,
            attester_signing_key=attester_sig_key_pub)
        attestation_check.load(output_check['attestation'])
        self.assertNotEqual(my_attestation.claim_set_keys.trace_key.k,
                            attestation_check.claim_set_keys.trace_key.k)
        # Check whether we can read the attestation again.
        my_attestation = Attestation(claim_set_keys=updated_claim_set_keys,
                                     subject_signing_key=subject_sig_key_pub,
                                     attester_signing_key=attester_sig_key_pub)
        my_attestation.load(attestation_content)

    def test_accept_attestation(self):
        """Accept (counter-sign) a received attestation."""
        subject_sig_key_full = Jwk.get_instance(
            from_dict=self.jwk_sig_harry_full)
        subject_sig_key_pub = Jwk.get_instance(
            from_dict=self.jwk_sig_harry_pub)
        attester_sig_key_pub = Jwk.get_instance(
            from_dict=self.jwk_sig_albus_pub)
        claim_set_keys_file = (
            CASE_0['steps'][1]['output']['claim_set_keys'])
        attestation_file = (
            CASE_0['steps'][1]['output']['attestation'])
        my_attestation = Attestation(claim_set_keys=claim_set_keys_file,
                                     subject_signing_key=subject_sig_key_pub,
                                     attester_signing_key=attester_sig_key_pub)
        my_attestation.load(attestation_file)
        my_attestation.cosign(subject_sig_key_full)
        accepted_attestation = my_attestation.serialise()
        # Check whether we can load the accepted attestation again.
        check_attestation = Attestation(
            claim_set_keys=claim_set_keys_file,
            subject_signing_key=subject_sig_key_pub,
            attester_signing_key=attester_sig_key_pub)
        check_attestation.load(accepted_attestation)


if __name__ == '__main__':
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

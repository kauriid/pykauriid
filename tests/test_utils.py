# -*- coding: utf-8 -*-
"""Tests for the utils module."""

# Created: 2018-07-25 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

import unittest

from pykauriid.utils import (bytes_to_string,
                             string_to_bytes,
                             dict_to_json,
                             json_to_dict,
                             dict_to_base64,
                             base64_to_dict)


class ModuleTest(unittest.TestCase):
    """Testing the utils module functions."""

    def test_bytes_to_string(self):
        """Base64 decoding various values."""
        tests = [b'Gruffalo', b'pork', b'Schwein',
                 b'dRL"\x86\x00=cC:\xb5\x82u\x9a\xf1\x89']
        expected = ['R3J1ZmZhbG8', 'cG9yaw', 'U2Nod2Vpbg',
                    'ZFJMIoYAPWNDOrWCdZrxiQ']
        for test, check in zip(tests, expected):
            self.assertEqual(bytes_to_string(test), check)

    def test_string_to_bytes(self):
        """Base64 encoding various values."""
        tests = ['R3J1ZmZhbG8', 'cG9yaw', 'U2Nod2Vpbg',
                 'ZFJMIoYAPWNDOrWCdZrxiQ']
        expected = [b'Gruffalo', b'pork', b'Schwein',
                    b'dRL"\x86\x00=cC:\xb5\x82u\x9a\xf1\x89']
        for test, check in zip(tests, expected):
            self.assertEqual(string_to_bytes(test), check)

    def test_dict_to_json_to_remove(self):
        """Dict encoding various samples to JSON, removing attributes."""
        tests = [
            {'sub': {'a': 1, 'b': 2, 'c': 3}},
            {'foo': 'bar', 'baz': 'boo'}
        ]
        expected = [
            '{"sub":{"a":1,"c":3}}',
            '{"foo":"bar"}'
        ]
        for test, check in zip(tests, expected):
            self.assertEqual(dict_to_json(test, to_remove=['b', 'baz']),
                             check)

    def test_dict_to_json(self):
        """Dict encoding various samples to JSON."""
        tests = [
            {'foo': 'bar'},
            {'x': b'\xd7Z\x98\x01\x82\xb1\n\xb7\xd5K\xfe\xd3\xc9d\x07:\x0e'
                  b'\xe1r\xf3\xda\xa6#%\xaf\x02\x1ah\xf7\x07Q\x1a'},
            {'some': [1, 2, 3]},
            {'sub': {'a': 1, 'b': 2, 'c': 3}},
            {'foo': 'bar', '_baz': 'boo'},
            {'sub': [b'Gruffalo', b'pork', b'Schwein']}
        ]
        expected = [
            '{"foo":"bar"}',
            '{"x":"11qYAYKxCrfVS_7TyWQHOg7hcvPapiMlrwIaaPcHURo"}',
            '{"some":[1,2,3]}',
            '{"sub":{"a":1,"b":2,"c":3}}',
            '{"foo":"bar"}',
            '{"sub":["R3J1ZmZhbG8","cG9yaw","U2Nod2Vpbg"]}'
        ]
        for test, check in zip(tests, expected):
            self.assertEqual(dict_to_json(test), check)

    def test_json_to_dict(self):
        """Dict decoding various samples from JSON."""
        tests = [
            ('{"foo": "bar"}', []),
            ('{"x": "11qYAYKxCrfVS_7TyWQHOg7hcvPapiMlrwIaaPcHURo"}', ['x']),
            ('{"some":[1,2,3]}', None),
            ('{"sub": {"a": 1, "b": 2, "c": 3}}', None)
        ]
        expected = [
            {'foo': 'bar'},
            {'x': b'\xd7Z\x98\x01\x82\xb1\n\xb7\xd5K\xfe\xd3\xc9d\x07:\x0e'
                  b'\xe1r\xf3\xda\xa6#%\xaf\x02\x1ah\xf7\x07Q\x1a'},
            {'some': [1, 2, 3]},
            {'sub': {'a': 1, 'b': 2, 'c': 3}},
        ]
        for test, check in zip(tests, expected):
            content, protected = test
            self.assertDictEqual(json_to_dict(content, protected), check)

    def test_dict_json_roundtrip(self):
        """Dict to JSON to dict round trip encoding."""
        test = {
            'foo': 'bar',
            'x': b'\xd7Z\x98\x01\x82\xb1\n\xb7\xd5K\xfe\xd3\xc9d\x07:\x0e'
                 b'\xe1r\xf3\xda\xa6#%\xaf\x02\x1ah\xf7\x07Q\x1a',
            'some': [1, 2, 3],
            'sub': {'a': 1, 'b': 2, 'c': 3}
        }
        json_representation = dict_to_json(test)
        self.assertDictEqual(json_to_dict(json_representation, ['x']), test)

    def test_dict_to_base64(self):
        """Convert a dict to a base64 representation."""
        tests = [
            {'foo': 'bar'},
            {'x': b'\xd7Z\x98\x01\x82\xb1\n\xb7\xd5K\xfe\xd3\xc9d\x07:\x0e'
                  b'\xe1r\xf3\xda\xa6#%\xaf\x02\x1ah\xf7\x07Q\x1a'},
            {'some': [1, 2, 3]},
            {'sub': {'a': 1, 'b': 2, 'c': 3}},
        ]
        expected = [
            'eyJmb28iOiJiYXIifQ',
            ('eyJ4IjoiMTFxWUFZS3hDcmZWU183VHlXUUhPZzdoY3ZQYXBpTWxyd0lhYVBjSFV'
             'SbyJ9'),
            'eyJzb21lIjpbMSwyLDNdfQ',
            'eyJzdWIiOnsiYSI6MSwiYiI6MiwiYyI6M319'
        ]
        for test, check in zip(tests, expected):
            self.assertEqual(dict_to_base64(test), check)

    def test_base64_to_dict(self):
        """Convert a base64 JOSE representation to a dict."""
        tests = [
            'eyJmb28iOiJiYXIifQ',
            ('eyJ4IjoiMTFxWUFZS3hDcmZWU183VHlXUUhPZzdoY3ZQYXBpTWxyd0lhYVBjSFV'
             'SbyJ9'),
            'eyJzb21lIjpbMSwyLDNdfQ',
            'eyJzdWIiOnsiYSI6MSwiYiI6MiwiYyI6M319'
        ]
        expected = [
            {'foo': 'bar'},
            {'x': '11qYAYKxCrfVS_7TyWQHOg7hcvPapiMlrwIaaPcHURo'},
            {'some': [1, 2, 3]},
            {'sub': {'a': 1, 'b': 2, 'c': 3}},
        ]
        for test, check in zip(tests, expected):
            self.assertDictEqual(base64_to_dict(test), check)


if __name__ == '__main__':
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
